/**
 * @file animation/render.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

#include "render.hpp"

Render::Render(const Model &model, int width, int height) :
  _model(model), _width(width), _height(height) {
  _scale = static_cast<float>(std::min(width, height));
  _pixels_in_meter = static_cast<float>(_scale / _model.MetricScale());
}

void Render::display(const Cairo::RefPtr<Cairo::Context> &cr, const std::vector<std::string> &link_names) const {
  cr->save();
  cr->set_source_rgba(1.0, 1.0, 1.0, 1);
  cr->paint();

  draw_land_uses(cr);
  draw_leisure(cr);
  draw_water(cr);
  draw_railways(cr);
  draw_highways(cr, link_names);
  draw_buildings(cr);

  cr->restore();
}

void Render::draw_buildings(const Cairo::RefPtr<Cairo::Context> &cr) const {
  for (const auto &building: _model.Buildings()) {
    cr->set_source_rgb(0.5, 0.5, 0.5);
    draw_surface(cr, path_from_MP(building));
  }
}

void Render::draw_leisure(const Cairo::RefPtr<Cairo::Context> &cr) const {
  for (auto &leisure: _model.Leisures()) {
    cr->set_source_rgb(0.56, 0.93, 0.56);
    draw_surface(cr, path_from_MP(leisure));
  }
}

void Render::draw_surface(const Cairo::RefPtr<Cairo::Context> &cr,
                          const std::vector<std::pair<double, double>> &path) {
  cr->set_line_cap(Cairo::LINE_CAP_ROUND);
  cr->set_line_join(Cairo::LINE_JOIN_ROUND);
  cr->set_line_width(1);
  cr->move_to(path.cbegin()->first, path.cbegin()->second);
  for (auto it = path.cbegin()++; it != path.cend(); ++it) {
    cr->line_to(it->first, it->second);
  }
  cr->fill();
}

void Render::draw_water(const Cairo::RefPtr<Cairo::Context> &cr) const {
  for (auto &water: _model.Waters()) {
    cr->set_source_rgb(0.4, 0.8, 1.);
    draw_surface(cr, path_from_MP(water));
  }
}

void Render::draw_land_uses(const Cairo::RefPtr<Cairo::Context> &cr) const {
  for (auto &land_use: _model.Landuses()) {
    cr->set_source_rgb(0.9, 0.9, 0.9);
    draw_surface(cr, path_from_MP(land_use));
  }
}

void Render::draw_highways(const Cairo::RefPtr<Cairo::Context> &cr, const std::vector<std::string> &link_names) const {
  auto ways = _model.Ways().data();

  cr->set_line_cap(Cairo::LINE_CAP_ROUND);
  cr->set_line_join(Cairo::LINE_JOIN_ROUND);
  for (auto road: _model.Roads()) {
    auto &way = ways[road.way];

    if (std::find_if(link_names.cbegin(), link_names.cend(), [way](const auto &e) {
      return e.find(way.id) != std::string::npos;
    }) != link_names.cend()) {
      cr->set_source_rgb(1., 1., 0.5);
//    } else {
//      cr->set_source_rgb(0.9, 0.9, 0.9);
//    }

      auto path = path_from_way(way);

      cr->set_line_width(16);
      cr->move_to(path.cbegin()->first, path.cbegin()->second);
      for (auto it = path.cbegin()++; it != path.cend(); ++it) {
        cr->line_to(it->first, it->second);
      }
      cr->stroke();
    }
  }
  for (auto road: _model.Roads()) {
    auto &way = ways[road.way];

    if (std::find_if(link_names.cbegin(), link_names.cend(), [way](const auto &e) {
      return e.find(way.id) != std::string::npos;
    }) != link_names.cend()) {
      auto path = path_from_way(way);

      cr->set_source_rgb(0., 0., 0.);
      cr->set_line_width(1);
      cr->move_to(path.cbegin()->first, path.cbegin()->second);
      for (auto it = path.cbegin()++; it != path.cend(); ++it) {
        cr->line_to(it->first, it->second);
      }
      cr->stroke();
    }
  }
}

void Render::draw_railways(const Cairo::RefPtr<Cairo::Context> &/* cr */) const {
//  auto ways = _model.Ways().data();

//  for (auto &railway: _model.Railways()) {
//    auto &way = ways[railway.way];
//    auto path = PathFromWay(way);
//    surface.stroke(m_RailwayStrokeBrush, path, std::nullopt, io2d::stroke_props{_railway_outer_width * _pixels_in_meter});
//    surface.stroke(m_RailwayDashBrush, path, std::nullopt, io2d::stroke_props{_railway_inner_width * _pixels_in_meter},
//                   m_RailwayDashes);
//  }
}

std::vector<std::pair<double, double>> Render::path_from_way(const Model::Way &way) const {
  if (way.nodes.empty())
    return {};

  const auto nodes = _model.Nodes().data();
  std::vector<std::pair<double, double>> lines;

  for (int node: way.nodes) {
    lines.push_back(to_point_2D(nodes[node]));
  }
  return lines;
}

std::vector<std::pair<double, double>> Render::path_from_MP(const Model::Multipolygon &mp) const {
  const auto nodes = _model.Nodes().data();
  const auto ways = _model.Ways().data();

  std::vector<std::pair<double, double>> lines;

  for (auto way_num: mp.outer) {
    const Model::Way &way = ways[way_num];

    for (auto it = way.nodes.begin(); it != std::end(way.nodes); ++it) {
      lines.push_back(to_point_2D(nodes[*it]));
    }
  }
  return lines;
}

std::pair<double, double> Render::to_point_2D(const Model::Node &node) const {
  return {node.x * _scale, _scale - node.y * _scale};
}
