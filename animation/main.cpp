/**
 * @file animation/main.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/TimedIterator.hpp>

#include <artis-traffic/micro/utils/JsonReader.hpp>

#include <artis-traffic/micro/core/vehicle-dynamics/TimeGippsVehicleDynamics.hpp>
#include <artis-traffic/micro/core/vehicle-dynamics/EventGippsVehicleDynamics.hpp>

#include <chrono>
#include <fstream>
#include <iostream>
#include <thread>

#include <gtkmm.h>
#include <cairomm/context.h>
#include <cairomm/surface.h>
#include <condition_variable>

constexpr int DEFAULT_WIDTH = 1024;
constexpr int DEFAULT_HEIGHT = 1024;
constexpr int VEHICLE_LENGTH = 6;

#include "http.hpp"
#include "model.hpp"
#include "render.hpp"

#include "../tests/traffic/micro/JSONNetworkGraphManager.hpp"
#include "simulator.hpp"

//using ConcreteSimulator = Simulator<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>;
using ConcreteSimulator = Simulator<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>;

class MyWindow;

class ViewDrawingArea : public Gtk::DrawingArea {
public:
  ViewDrawingArea(MyWindow *window,
                  const ConcreteSimulator &simulator,
                  const Render &render, int width, int height,
                  std::map<std::string, std::pair<double, double>> stops) :
    _simulator(simulator), _render(render), _window(window), _buffer(nullptr), _isRealized(false), _needRedraw(true),
    _width(width), _height(height), _stops(stops), _is_first(true), _step(0), _delta(1) {
  }

  ~ViewDrawingArea() override = default;

  bool animate() {
    queueRedraw();
    if (_step == 1 and _delta == -1) {
      _step = 0;
      return false;
    } else {
      _step += _delta;
      return _step >= 0 and _step < (_simulator.context().end() - _simulator.context().begin()) * 10;
    }
  }

  void backward() { _delta = -1; }

  void forward() { _delta = 1; }

  unsigned int step() const { return _step; }

private:
  std::pair<double, double> adjust_coordinates(const std::pair<double, double> &p) {
    const auto pi = 3.14159265358979323846264338327950288;
    const auto deg_to_rad = 2. * pi / 360.;
    const auto earth_radius = 6378137.;
    const auto lat2ym = [&](double lat) { return log(tan(lat * deg_to_rad / 2 + pi / 4)) / 2 * earth_radius; };
    const auto lon2xm = [&](double lon) { return lon * deg_to_rad / 2 * earth_radius; };

    return std::make_pair((lon2xm(p.first) - lon2xm(_render.model().MinLon())) / _render.model().MetricScale(),
                          (lat2ym(p.second) - lat2ym(_render.model().MinLat())) / _render.model().MetricScale());
  }

  void adjust_position(const Model::Way &way, int link_index, bool reverse, double &position) {
    const std::map<std::string, artis::traffic::micro::utils::Link<artis::traffic::micro::utils::LinkData>>
      &links = _simulator.network().links;

    if (reverse) {
      int link_number = 20;

      while (link_number >= 0 and
             links.find("way/" + way.id + "_" + std::to_string(link_number) + "_R") == links.cend()) { --link_number; }

      auto it = _simulator.merged_links().find("way/" + way.id + "_" + std::to_string(link_number) + "_R");

      if (it != _simulator.merged_links().cend() and link_number > link_index) {
        int k = 0;

        while (k < it->second.size() and it->second[k].first != way.id) { ++k; }
        position += compute_way_length2(it->second[k].second, _render.model().Nodes().data());
        --link_number;
      }
      for (int k = link_number; k > link_index; --k) {
        auto it_link = links.find("way/" + way.id + "_" + std::to_string(k) + "_R");

        if (it_link != links.cend()) {
          position += it_link->second.data.length;
        }
      }
    } else {
      auto it = _simulator.merged_links().find("way/" + way.id + "_0");

      if (it != (_simulator.merged_links().cend())) {
        unsigned int k = 0;

        while (k < it->second.size() and it->second[k].first != way.id) { ++k; }
        position += compute_way_length2(it->second[k].second, _render.model().Nodes().data());
      }
      for (int k = 0; k < link_index; ++k) {
        auto it_link = links.find("way/" + way.id + "_" + std::to_string(k));

        if (it_link != links.cend()) {
          position += it_link->second.data.length;
        }
      }
    }
  }

  std::pair<double, double>
  compute_coordinates(const std::vector<int> &way_nodes, bool reverse, double position,
                      std::pair<double, double> &direction) {
    const auto pi = 3.14159265358979323846264338327950288;
    const auto deg_to_rad = 2. * pi / 360.;
    const auto earth_radius = 6371000.; // 6378137.;

    const auto nodes = _render.model().Nodes().data();
    int node_index = reverse ? way_nodes.size() - 2 : 1;
    double total_length = 0;
    double length = 0;
    int delta = reverse ? -1 : 1;

    do {
      std::pair<double, double> p1{nodes[way_nodes[node_index - delta]]._x, nodes[way_nodes[node_index - delta]]._y};
      std::pair<double, double> p2{nodes[way_nodes[node_index]]._x, nodes[way_nodes[node_index]]._y};

      total_length += length;

      double u1 = p1.first * deg_to_rad;
      double u2 = p2.first * deg_to_rad;
      double du = (p2.first - p1.first) * deg_to_rad;
      double da = (p2.second - p1.second) * deg_to_rad;
      double a =
        std::sin(du / 2) * std::sin(du / 2) + std::cos(u1) * std::cos(u2) * std::sin(da / 2) * std::sin(da / 2);
      double c = 2 * std::atan2(std::sqrt(a), std::sqrt(1 - a));
      double d = earth_radius * c;

      length = d;
      node_index += delta;
    } while (total_length + length < position and node_index < way_nodes.size() and node_index >= 0);
    node_index -= delta;
    position -= total_length;
    position /= _render.model().MetricScale();
    if (node_index == -1) { // TODO: position > link length
      return {nodes[way_nodes[0]].x, nodes[way_nodes[node_index - delta]].y};
    } else {
      double p1x = nodes[way_nodes[node_index - delta]].x;
      double p1y = nodes[way_nodes[node_index - delta]].y;
      double p2x = nodes[way_nodes[node_index]].x;
      double p2y = nodes[way_nodes[node_index]].y;
      double dx = p2x - p1x;
      double dy = p2y - p1y;
      double d = std::sqrt(dx * dx + dy * dy);

      direction = {(p2x - p1x) / d, (p2y - p1y) / d};
      return {p1x + position * (p2x - p1x) / (length / _render.model().MetricScale()),
              p1y + position * (p2y - p1y) / (length / _render.model().MetricScale())};
    }
  }

  void draw() {
    if (_is_first) {
      _vehicle_context->set_source_rgb(1., 1., 0.5);
      _vehicle_context->paint();
      _vehicle_context->set_source_rgb(0., 0., 0.);
      _vehicle_context->move_to(0, 0);
      _vehicle_context->line_to(VEHICLE_LENGTH, VEHICLE_LENGTH >> 1);
      _vehicle_context->line_to(0, VEHICLE_LENGTH);
      _vehicle_context->line_to(0, 0);
      _vehicle_context->fill();

      _render.display(_map_context, _simulator.all_link_names());
      _is_first = false;
    }
    Gdk::Cairo::set_source_pixmap(_context, _map_buffer, 0, 0);
    _context->paint();
    draw_vehicles();
  }

  void draw_vehicles() {
    const auto pi = 3.14159265358979323846264338327950288;

    _context->save();
    _context->set_source_rgb(0, 0, 0);
    _context->set_line_width(1);
    _context->select_font_face("Times", Cairo::FONT_SLANT_NORMAL, Cairo::FONT_WEIGHT_BOLD);
    _context->set_font_size(12);
    for (const auto &[link_id, data]: _simulator.link_data()) {
      for (const auto &input: data.inputs) {
        double arrived_time = input.first;
        unsigned int vehicle_id = input.second;

        if (std::abs(arrived_time - _step * 0.1) < 1e-6 or arrived_time < _step * 0.1) {
          unsigned int index = std::ceil((_step * 0.1 - arrived_time) * 10);
          const std::vector<double> &positions = data.positions.at(vehicle_id);
          double position = positions[index];

          if (position >= 0) {
            bool reverse = link_id.back() == 'R';
            int link_index;
            std::vector<int> way_nodes;
            auto it_road = find_road(link_id, link_index, reverse, position, way_nodes);

            if (it_road != _render.model().Roads().cend()) {
              auto &way = _render.model().Ways()[it_road->way];
              std::pair<double, double> direction{};

              if (_simulator.merged_links().find(link_id) == _simulator.merged_links().cend()) {
                adjust_position(way, link_index, reverse, position);
              }

              auto p = compute_coordinates(way_nodes, reverse, position, direction);
              double angle = std::acos(direction.first) * (direction.second > 0 ? 1 : -1);
              auto x = (unsigned int) (p.first * _render.scale());
              auto y = (unsigned int) (_render.scale() - p.second * _render.scale());

              _context->save();
              _context->translate(x, y);
              _context->rotate(-angle);
              Gdk::Cairo::set_source_pixmap(_context, _vehicle_buffer, -VEHICLE_LENGTH, 0);
              _context->paint();
              _context->restore();

//              if (direction.first < 0 and direction.second >= 0) {
//                _context->move_to(x + VEHICLE_LENGTH * std::cos(angle - pi / 2),
//                                  y - VEHICLE_LENGTH * std::sin(angle - pi / 2));
//              } else {
//                _context->move_to(x + 1, y + 1);
//              }
//              _context->show_text(std::to_string(vehicle_id));
            }
          }
        }
      }
    }
    for (const auto &[stop_id, data]: _simulator.stop_data()) {
      int vehicle_id = data[_step];

      if (vehicle_id != -1) {
        std::pair<double, double> p = adjust_coordinates(_stops[stop_id]);

        _context->save();
        _context->set_source_rgb(1., 0., 0.);
        _context->rectangle(p.first * _render.scale() - 4, _render.scale() - p.second * _render.scale() - 4, 8, 8);
        _context->fill();
        _context->restore();
      }
    }
    _context->restore();
  }

  static double compute_way_length2(const std::vector<int> &way_nodes, const Model::Node *nodes) {
    const auto pi = 3.14159265358979323846264338327950288;
    const auto deg_to_rad = 2. * pi / 360.;
    const auto earth_radius = 6371000.; // 6378137.;
    double length = 0;

    for (unsigned int i = 0; i < way_nodes.size() - 1; ++i) {
      std::pair<double, double> p1{nodes[way_nodes[i]]._x, nodes[way_nodes[i]]._y};
      std::pair<double, double> p2{nodes[way_nodes[i + 1]]._x, nodes[way_nodes[i + 1]]._y};

      double u1 = p1.first * deg_to_rad;
      double u2 = p2.first * deg_to_rad;
      double du = (p2.first - p1.first) * deg_to_rad;
      double da = (p2.second - p1.second) * deg_to_rad;
      double a =
        std::sin(du / 2) * std::sin(du / 2) + std::cos(u1) * std::cos(u2) * std::sin(da / 2) * std::sin(da / 2);
      double c = 2 * std::atan2(std::sqrt(a), std::sqrt(1 - a));

      length += earth_radius * c;
    }
    return length;
  }

  double compute_way_length(const Model::Way &way) const {
    return compute_way_length2(way.nodes, _render.model().Nodes().data());
  }

  std::vector<Model::Road>::const_iterator
  find_road(const std::string &link_id, int &link_index, bool reverse, double &position,
            std::vector<int> &way_nodes) const {
    auto it = _simulator.merged_links().find(link_id);

    link_index = extract_index(link_id);
    if (it != _simulator.merged_links().cend()) {
      if (reverse) {
        auto itw = it->second.crbegin();

        while (itw != it->second.crend()) {
          const Model::Way &way = *std::find_if(_render.model().Ways().cbegin(), _render.model().Ways().cend(),
                                                [this, itw](const auto &e) {
                                                  return e.id == itw->first;
                                                });
          double length = compute_way_length2(itw->second, _render.model().Nodes().data());

          if (position > length) {
            position -= length;
            ++itw;
          } else {
            break;
          }
        }
        way_nodes = itw->second;
        return std::find_if(_render.model().Roads().cbegin(), _render.model().Roads().cend(),
                            [this, itw](const auto &e) {
                              return _render.model().Ways()[e.way].id == itw->first;
                            });
      } else {
        auto itw = it->second.cbegin();

        while (itw != it->second.cend()) {
          const Model::Way &way = *std::find_if(_render.model().Ways().cbegin(), _render.model().Ways().cend(),
                                                [this, itw](const auto &e) {
                                                  return e.id == itw->first;
                                                });
          double length = compute_way_length2(itw->second, _render.model().Nodes().data());

          if (position > length) {
            position -= length;
            ++itw;
          } else {
            break;
          }
        }
        way_nodes = itw->second;
        return std::find_if(_render.model().Roads().cbegin(), _render.model().Roads().cend(),
                            [this, itw](const auto &e) {
                              return _render.model().Ways()[e.way].id == itw->first;
                            });
      }
    } else {
      auto itr = std::find_if(_render.model().Roads().cbegin(), _render.model().Roads().cend(),
                              [this, link_id](const auto &e) {
                                return link_id.find(_render.model().Ways()[e.way].id) != std::string::npos;
                              });

      way_nodes = _render.model().Ways()[itr->way].nodes;
      return itr;
    }
  }

  static int extract_index(const std::string &link_id) {
    auto pos_first = link_id.find_first_of('_');

    int link_index = 0;
    if (pos_first != std::string::npos) {
      auto pos_second = link_id.find('_', pos_first + 1);

      if (pos_second != std::string::npos) {
        link_index = std::stoi(link_id.substr(pos_first + 1, pos_second - pos_first - 1));
      } else {
        if (link_id[pos_first + 1] != 'R') {
          link_index = std::stoi(link_id.substr(pos_first + 1));
        }
      }
    }
    return link_index;
  }

  void queueRedraw() {
    _needRedraw = true;
    queue_draw();
  }

  bool on_expose_event(GdkEventExpose *) override;

  void on_realize() override;

private:
  const ConcreteSimulator &_simulator;
  const Render &_render;
  std::map<std::string, std::pair<double, double>> _stops;

  MyWindow *_window;
  Glib::RefPtr<Gdk::Window> _gdk_window;
  Glib::RefPtr<Gdk::Pixmap> _buffer;
  Glib::RefPtr<Gdk::Pixmap> _map_buffer;
  Glib::RefPtr<Gdk::Pixmap> _vehicle_buffer;
  Cairo::RefPtr<Cairo::Context> _context;
  Cairo::RefPtr<Cairo::Context> _map_context;
  Cairo::RefPtr<Cairo::Context> _vehicle_context;
  Glib::RefPtr<Gdk::GC> _gc;
  bool _isRealized;
  bool _needRedraw;
  int _width;
  int _height;
  bool _is_first;

  int _step;
  int _delta;
};

class MyWindow : public Gtk::Window {
public:
  MyWindow(
    const ConcreteSimulator &simulator,
    const Render &render, std::map<std::string, std::pair<double, double>> stops)
    : _simulator(simulator), _render(render), _map_view(this, simulator, render, DEFAULT_WIDTH, DEFAULT_HEIGHT, stops),
      _animation_thread([this] {
        bool more = true;

        do {
          std::this_thread::sleep_for(std::chrono::milliseconds(50));
          if (_play) {
//          std::cout << "ANIMATE" << std::endl;
            more = _map_view.animate();
//          std::cout << "ANIMATE - END" << std::endl;
            if (not more and _map_view.step() == 0) {
              {
                std::lock_guard lk(_play_mutex);
//              std::cout << "PLAY = FALSE" << std::endl;
                _play = false;
              }
              {
                std::unique_lock lk(_play_mutex);
//              std::cout << "WAIT PLAY: " << _play << std::endl;
                _play_condition_variable.wait(lk, [this] { return _play; });
                more = true;
              }
            } else {
              std::unique_lock lk(_sleep_mutex);
//            std::cout << "WAIT SLEEP: " << _sleep << std::endl;
              _sleep_condition_variable.wait(lk, [this] { return _sleep; });
              _sleep = false;
            }
          } else {
            std::unique_lock lk(_play_mutex);
//          std::cout << "WAIT PLAY: " << _play << std::endl;
            _play_condition_variable.wait(lk, [this] { return _play; });
          }
        } while (more and not _stop);
      }), _dispatcher(nullptr), _ready(false), _play(false), _sleep(false), _stop(false) {
    set_default_size(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    _time_view.set_text("----");
    _stop_button.set_label("stop");
    _play_backward_button.set_label("<<");
    _pause_button.set_label("pause");
    _play_forward_button.set_label(">>");
    _stop_button.set_label("stop");
    _hbox.pack_start(_stop_button);
    _hbox.pack_start(_play_backward_button);
    _hbox.pack_start(_pause_button);
    _hbox.pack_start(_play_forward_button);
    add(_vbox);
    _vbox.pack_start(_map_view, true, true);
    _vbox.pack_start(_time_view, false, false);
    _vbox.pack_end(_hbox, false, false);

    _stop_button.signal_clicked().connect(sigc::mem_fun(*this, &MyWindow::on_stop_button_clicked));
    _play_backward_button.signal_clicked().connect(sigc::mem_fun(*this, &MyWindow::on_backward_play_button_clicked));
    _pause_button.signal_clicked().connect(sigc::mem_fun(*this, &MyWindow::on_pause_button_clicked));
    _play_forward_button.signal_clicked().connect(sigc::mem_fun(*this, &MyWindow::on_forward_play_button_clicked));

    signal_delete_event().connect(sigc::mem_fun(*this, &MyWindow::on_quit));

    show_all();
  }

  ~MyWindow() override = default;

  void notify() {
    if (_play) {
//      std::cout << "EMIT" << std::endl;
      _dispatcher->emit();
      {
        std::lock_guard lk(_mutex);
//        std::cout << "READY = TRUE" << std::endl;
        _ready = true;
      }
      _condition_variable.notify_one();
    }
  }

  void set_dispatcher(Glib::Dispatcher *dispatcher) {
    _dispatcher = dispatcher;
    _dispatcher->connect([this]() { dispatcher_task(); });
  }

private:
  void dispatcher_task() {
    {
      std::unique_lock lk(_mutex);
//      std::cout << "WAIT READY: " << _ready << std::endl;
      _condition_variable.wait(lk, [this] { return _ready; });
//      std::cout << "READY = FALSE" << std::endl;
      _ready = false;
      if (_map_view.step() > 0) {
        _time_view.set_text(std::to_string(_map_view.step()));
      } else {
        _time_view.set_text("----");
      }
    }
    {
      std::lock_guard lk(_sleep_mutex);
//      std::cout << "SLEEP = TRUE" << std::endl;
      _sleep = true;
    }
    _sleep_condition_variable.notify_one();
  }

  bool on_quit(GdkEventAny * /* event */) {
    _stop = true;
    if (not _play) {
      {
        std::lock_guard lk(_mutex);
//        std::cout << "PLAY = TRUE" << std::endl;
        _play = true;
      }
      _play_condition_variable.notify_one();
    }
    _animation_thread.join();
    hide();
    return true;
  }

  void on_stop_button_clicked() {
    _stop = true;
  }

  void on_backward_play_button_clicked() {
    _map_view.backward();
    if (not _play) {
      {
        std::lock_guard lk(_play_mutex);
//        std::cout << "PLAY = TRUE" << std::endl;
        _play = true;
      }
      _play_condition_variable.notify_one();
    }
  }

  void on_forward_play_button_clicked() {
    _map_view.forward();
    if (not _play) {
      {
        std::lock_guard lk(_play_mutex);
//        std::cout << "PLAY = TRUE" << std::endl;
        _play = true;
      }
      _play_condition_variable.notify_one();
    }
  }

  void on_pause_button_clicked() {
    if (_play) {
      std::unique_lock lk(_play_mutex);
//      std::cout << "PLAY = FALSE" << std::endl;
      _play = false;
    }
  }

  const ConcreteSimulator &_simulator;
  const Render &_render;
  Gtk::VBox _vbox;
  Gtk::HBox _hbox;
  ViewDrawingArea _map_view;
  Gtk::Label _time_view;
  Gtk::Button _play_forward_button;
  Gtk::Button _play_backward_button;
  Gtk::Button _pause_button;
  Gtk::Button _stop_button;
  mutable std::mutex _mutex;
  mutable std::mutex _play_mutex;
  mutable std::mutex _sleep_mutex;
  std::condition_variable _condition_variable;
  std::condition_variable _play_condition_variable;
  std::condition_variable _sleep_condition_variable;
  std::thread _animation_thread;
  Glib::Dispatcher *_dispatcher;
  bool _ready;
  bool _play;
  bool _sleep;
  bool _stop;
};

bool ViewDrawingArea::on_expose_event(GdkEventExpose *) {

//  std::cout << "on_expose_event" << std::endl;

  if (_isRealized) {
    if (!_buffer) {
      _buffer = Gdk::Pixmap::create(_gdk_window, _width, _height, -1);
      _map_buffer = Gdk::Pixmap::create(_gdk_window, _width, _height, -1);
      _vehicle_buffer = Gdk::Pixmap::create(_gdk_window, 6, 6, -1);
    }
    if (_buffer) {
      if (_needRedraw) {
        _context = _buffer->create_cairo_context();
        _map_context = _map_buffer->create_cairo_context();
        _vehicle_context = _vehicle_buffer->create_cairo_context();

//        std::cout << "DRAW" << std::endl;
        draw();
//        std::cout << "DRAW - END" << std::endl;

        _needRedraw = false;
      }
      _gdk_window->draw_drawable(_gc, _buffer, 0, 0, 0, 0, -1, -1);
//      std::cout << "NOTIFY" << std::endl;
      _window->notify();
    }
  }
  return true;
}

void ViewDrawingArea::on_realize() {
  Gtk::DrawingArea::on_realize();
  _gdk_window = get_window();
  _gc = Gdk::GC::create(_gdk_window);
  _isRealized = true;
  queueRedraw();
}

std::vector<std::byte> load_osm_data() {
  std::vector<std::byte> osm_data;
//  std::string osm_bounding_box = "1.868,50.952,1.88,50.968";
// mollien
//  std::string osm_bounding_box = "1.87,50.955,1.88,50.965";
// audruicq 2stop
//  std::string osm_bounding_box = "2.066,50.878,2.081,50.889";
// audruicq coq
//  std::string osm_bounding_box = "2.06843,50.87329,2.07261,50.87577";
// audruicq sym
//  std::string osm_bounding_box = "2.066,50.878,2.081,50.889";
// audruicq full
//  std::string osm_bounding_box = "2.026,50.841,2.118,50.871";
// audruicq centre
  std::string osm_bounding_box = "2.0752,50.8749,2.0872,50.882";
//  std::string osm_bounding_box = "2.079,50.877,2.083,50.881";
// audruicq centre 2
//  std::string osm_bounding_box = "2.062,50.866,2.093,50.889";
  std::cout << "Downloading OpenStreetMap data for the following bounding box: " << osm_bounding_box << " ... ";
  osm_data = FetchOpenStreetMapData(osm_bounding_box);
  if (osm_data.empty()) {
    std::cout << "Failed to download." << std::endl;
  } else {
    std::cout << "OK" << std::endl;
  }
  return osm_data;
}

std::string extract_name(const std::string &link_id) {
  auto pos_first = link_id.find_first_of('/');
  auto pos_second = link_id.find_first_of('_');

  if (pos_second == std::string::npos) {
    return link_id.substr(pos_first + 1);
  } else {
    return link_id.substr(pos_first + 1, pos_second - pos_first - 1);
  }
}

std::vector<std::pair<std::string, std::vector<int>>>
build_ordered_merged_link_list(const Model &model, const std::vector<std::string> &links,
                               const std::vector<std::vector<std::vector<double>>> &coords) {
  std::vector<std::pair<std::string, std::vector<int>>> ordered_merged_link_list;
  std::vector<const Model::Way *> ways;
  std::vector<std::pair<int, int >> node_ids;
  int first_node = -1;
  int last_node = -1;
  int next_node = -1;
  unsigned int link_index = 0;

  // search the first and last node of way/link
  for (const auto &l: links) {

//    std::cout << "  " << l << std::endl;

    std::string link_name = extract_name(l);
    auto it = std::find_if(model.Ways().cbegin(), model.Ways().cend(),
                           [link_name](const auto &w) { return w.id == link_name; });

    ways.push_back(&(*it));

    size_t first_index = 0;
    while (first_index < it->nodes.size() and
           not(std::abs(model.Nodes()[it->nodes[first_index]]._x - coords[link_index][0][0]) < 1e-6 and
               std::abs(model.Nodes()[it->nodes[first_index]]._y - coords[link_index][0][1]) < 1e-6)) {
      ++first_index;
    }

    size_t last_index = 0;
    while (last_index < it->nodes.size() and
           not(std::abs(model.Nodes()[it->nodes[last_index]]._x - coords[link_index][1][0]) < 1e-6 and
               std::abs(model.Nodes()[it->nodes[last_index]]._y - coords[link_index][1][1]) < 1e-6)) {
      ++last_index;
    }

//    std::cout << first_index << " -> " << last_index << " / " << it->nodes.size() << std::endl;

    node_ids.emplace_back(it->nodes[first_index], it->nodes[last_index]);
    ++link_index;
  }
  // search the first node and first way/link
  link_index = 0;
  for (const auto &node: node_ids) {
    if (std::find_if(node_ids.cbegin(), node_ids.cend(), [node](const auto &n) { return n.second == node.first; }) ==
        node_ids.cend()) {
      unsigned int index = 0;

      first_node = node.first;
      next_node = node.second;

      auto it = std::find_if(model.Ways().cbegin(), model.Ways().cend(),
                             [links, link_index, first_node, &index](const auto &w) {
                               index = 0;
                               return std::find_if(w.nodes.cbegin(), w.nodes.cend(),
                                                   [links, link_index, w, first_node, & index](const auto &n) {
                                                     ++index;
                                                     return n == first_node and
                                                            links[link_index].find(w.id) != std::string::npos;
                                                   }) != w.nodes.cend();
                             });
      ordered_merged_link_list.emplace_back(it->id, std::vector<int>());
      for (unsigned int k = index - 1; k < it->nodes.size(); ++k) {
        ordered_merged_link_list.back().second.push_back(it->nodes[k]);
      }
      break;
    }
    ++link_index;
  }
  // search the last node
  for (const auto &node: node_ids) {
    if (std::find_if(node_ids.cbegin(), node_ids.cend(), [node](const auto &n) { return n.first == node.second; }) ==
        node_ids.cend()) {
      last_node = node.second;
      break;
    }
  }

  bool stop = false;

  while (not stop) {
    auto it = std::find_if(ways.cbegin(), ways.cend(), [next_node](const auto &w) {
      return w->nodes[0] == next_node;
    });

    ordered_merged_link_list.emplace_back((*it)->id, std::vector<int>());
    for (unsigned int k = 0; k < (*it)->nodes.size() and not stop; ++k) {
      ordered_merged_link_list.back().second.push_back((*it)->nodes[k]);
      stop = (*it)->nodes[k] == last_node;
    }
    if (not stop) {
      next_node = (*it)->nodes[(*it)->nodes.size() - 1];
      stop = next_node == last_node;
    }
  }
  return ordered_merged_link_list;
}

int main(int argc, char *argv[]) {
  std::vector<std::byte> osm_data = load_osm_data();

  Glib::thread_init();
  if (not osm_data.empty()) {
    Model model{osm_data};
    Render render{model, DEFAULT_WIDTH, DEFAULT_HEIGHT};
    std::ifstream input("../../scripts/test_audruicq_centre.json");
//    std::ifstream input("../../scripts/test_audruicq_centre_2.json");

    if (input) {
      std::string str((std::istreambuf_iterator<char>(input)), std::istreambuf_iterator<char>());
      artis::traffic::micro::utils::JsonReader reader;
      std::vector<std::string> link_names;
      std::map<std::string, std::vector<std::pair<std::string, std::vector<int>>>> merged_links;
      std::map<std::string, std::pair<double, double>> stops;
      std::vector<std::string> stop_names;

      reader.parse_network(str);
      for (const auto &[key, value]: reader.network().links) {
        link_names.push_back(key);
        if (not value.data.merged_links_id.empty()) {
          merged_links[key] = build_ordered_merged_link_list(model, value.data.merged_links_id,
                                                             value.data.merged_links_coords);
        }
      }
      for (const auto &[key, value]: reader.network().stops) {
        stops[key] = value.data.coordinates;
        stop_names.push_back(key);
      }

      artis::common::context::Context<artis::common::DoubleTime> context(0, 1000);
      ConcreteSimulator simulator(context, reader.network(), link_names, merged_links, stop_names);

      std::cout << "Simulating ...";
      std::cout.flush();

      simulator.run(context);

      std::cout << " OK" << std::endl;

      Gtk::Main app(&argc, &argv);
      MyWindow window(simulator, render, stops);
      std::thread w([&app, &window]() {
        Glib::Dispatcher dp;

        window.set_dispatcher(&dp);
        app.run(window);
      });

      w.join();
    }
  }
  return EXIT_SUCCESS;
}
