/**
 * @file animation/http.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

#pragma once

#include <vector>
#include <cstddef>
#include <string>

std::vector<std::byte> FetchOpenStreetMapData(const std::string &bounding_box);

