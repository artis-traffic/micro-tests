/**
 * @file animation/simulator.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_TESTS_SIMULATOR_HPP
#define ARTIS_TRAFFIC_MICRO_TESTS_SIMULATOR_HPP

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/TimedIterator.hpp>

#include <artis-traffic/micro/utils/JsonReader.hpp>

#include <artis-traffic/micro/core/vehicle-dynamics/TimeGippsVehicleDynamics.hpp>
#include <artis-traffic/micro/core/vehicle-dynamics/EventGippsVehicleDynamics.hpp>

#include "../tests/traffic/micro/JSONNetworkGraphManager.hpp"

template<class Vehicle, class VehicleEntry, class VehicleState>
class Simulator {
public:
  struct LinkData {
    std::vector<std::pair<double, unsigned int>> inputs;
    std::map<unsigned int, std::vector<double>> positions;
  };

  typedef std::vector<int> StopData;



  Simulator(const artis::common::context::Context<artis::common::DoubleTime> &context,
            const artis::traffic::micro::utils::Network<artis::traffic::micro::utils::InputData,
                    artis::traffic::micro::utils::OutputData, artis::traffic::micro::utils::NodeData,
                    artis::traffic::micro::utils::JunctionData, artis::traffic::micro::utils::StopData,
                    artis::traffic::micro::utils::LinkData> &network, const std::vector<std::string> &links,
            const std::map<std::string, std::vector<std::pair<std::string, std::vector<int>>>> &merged_links,
            const std::vector<std::string> &stops) :
    _context(context), _network(network), _rc(context, "root", artis::common::NoParameters(), network),
    _link_names(links), _merged_links(merged_links) {
    for (const auto &link: links) {
      _link_data.insert(std::make_pair(link, LinkData{}));
    }
    for (const auto &stop: stops) {
      _stop_data.insert(std::make_pair(stop, StopData()));
    }
  }

  std::vector<std::string> all_link_names() const {
    std::vector<std::string> names = _link_names;

    for (const auto &e: _merged_links) {
      for (const auto &l: e.second) {
        if (std::find_if(names.begin(), names.end(),
                         [l](const auto &p) { return p.find("way/" + l.first) != std::string::npos; }) ==
            names.cend()) {
          names.push_back(l.first);
        }
      }
    }
    return names;
  }

  const artis::common::context::Context<artis::common::DoubleTime> &context() const { return _context; }

  const std::map<std::string, LinkData> &link_data() const { return _link_data; }

  const std::vector<std::string> &link_names() const { return _link_names; }

  const std::map<std::string, std::vector<std::pair<std::string, std::vector<int>>>> &
  merged_links() const { return _merged_links; }

  const artis::traffic::micro::utils::Network<artis::traffic::micro::utils::InputData,
          artis::traffic::micro::utils::OutputData, artis::traffic::micro::utils::NodeData,
          artis::traffic::micro::utils::JunctionData, artis::traffic::micro::utils::StopData,
          artis::traffic::micro::utils::LinkData> &network() const { return _network; }

  const artis::common::observer::Observer<artis::common::DoubleTime> &observer() const { return _rc.observer(); }

  const std::map<std::string, StopData> &stop_data() const { return _stop_data; }

  void run(artis::common::context::Context<artis::common::DoubleTime> &context) {
    _rc.attachView("Links", new JSONLinkView<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::utils::Network<artis::traffic::micro::utils::InputData,
            artis::traffic::micro::utils::OutputData, artis::traffic::micro::utils::NodeData,
            artis::traffic::micro::utils::JunctionData, artis::traffic::micro::utils::StopData,
            artis::traffic::micro::utils::LinkData>, artis::common::DoubleTime>(_network));
    _rc.attachView("Stops", new JSONStopView<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::utils::Network<artis::traffic::micro::utils::InputData,
            artis::traffic::micro::utils::OutputData, artis::traffic::micro::utils::NodeData,
            artis::traffic::micro::utils::JunctionData, artis::traffic::micro::utils::StopData,
            artis::traffic::micro::utils::LinkData>, artis::common::DoubleTime>(_network));
    _rc.switch_to_timed_observer(0.1);
    _rc.run(context);
    load_data();
  }

private:
  void load_data() {
    auto &link_view = this->link_view();

    for (auto &[key, value]: _link_data) {
      const typename JSONLinkView<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::utils::Network<artis::traffic::micro::utils::InputData,
              artis::traffic::micro::utils::OutputData, artis::traffic::micro::utils::NodeData,
              artis::traffic::micro::utils::JunctionData, artis::traffic::micro::utils::StopData,
              artis::traffic::micro::utils::LinkData>, artis::common::DoubleTime>::Values &indexes_values = link_view.get(
        "Link_" + key + ":vehicle_indexes");
      const typename JSONLinkView<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::utils::Network<artis::traffic::micro::utils::InputData,
              artis::traffic::micro::utils::OutputData, artis::traffic::micro::utils::NodeData,
              artis::traffic::micro::utils::JunctionData, artis::traffic::micro::utils::StopData,
              artis::traffic::micro::utils::LinkData>, artis::common::DoubleTime>::Values &positions_values = link_view.get(
        "Link_" + key + ":vehicle_positions");
      auto it_indexes = indexes_values.cbegin();
      auto it_positions = positions_values.cbegin();

      while (it_indexes != indexes_values.cend()) {
        std::vector<unsigned int> indexes;
        std::vector<double> positions;

        it_indexes->second(indexes);
        it_positions->second(positions);
        if (not indexes.empty()) {
          auto iti = indexes.cbegin();
          auto itp = positions.cbegin();

          while (iti != indexes.cend()) {
            if (value.positions.find(*iti) == value.positions.cend()) {
              value.inputs.emplace_back(it_indexes->first, *iti);
              value.positions.insert(std::make_pair(*iti, std::vector<double>()));
            }
            value.positions[*iti].push_back(*itp);
            ++iti;
            ++itp;
          }
        }
        ++it_indexes;
        ++it_positions;
      }
    }

    auto &stop_view = this->stop_view();

    for (auto &[key, value]: _stop_data) {
      const typename JSONStopView<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::utils::Network<artis::traffic::micro::utils::InputData,
              artis::traffic::micro::utils::OutputData, artis::traffic::micro::utils::NodeData,
              artis::traffic::micro::utils::JunctionData, artis::traffic::micro::utils::StopData,
              artis::traffic::micro::utils::LinkData>, artis::common::DoubleTime>::Values &index_values = stop_view.get(
        "Stop_" + key + ":vehicle_index");
      auto it_index = index_values.cbegin();

      while (it_index != index_values.cend()) {
        int vehicle_index;

        it_index->second(vehicle_index);
        value.push_back(vehicle_index);
        ++it_index;
      }
    }
  }

  const artis::common::observer::View<artis::common::DoubleTime> &link_view() const {
    return _rc.observer().view("Links");
  }

  const artis::common::observer::View<artis::common::DoubleTime> &stop_view() const {
    return _rc.observer().view("Stops");
  }

  const artis::common::context::Context<artis::common::DoubleTime> &_context;
  const artis::traffic::micro::utils::Network<artis::traffic::micro::utils::InputData,
          artis::traffic::micro::utils::OutputData, artis::traffic::micro::utils::NodeData,
          artis::traffic::micro::utils::JunctionData, artis::traffic::micro::utils::StopData,
          artis::traffic::micro::utils::LinkData> &_network;
    artis::common::RootCoordinator<
            artis::common::DoubleTime, artis::pdevs::Coordinator<
                    artis::common::DoubleTime,
                    JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime>,
                    artis::common::NoParameters, artis::traffic::micro::utils::Network<artis::traffic::micro::utils::InputData,
                            artis::traffic::micro::utils::OutputData, artis::traffic::micro::utils::NodeData,
                            artis::traffic::micro::utils::JunctionData, artis::traffic::micro::utils::StopData,
                            artis::traffic::micro::utils::LinkData>>
  > _rc;
  std::map<std::string, LinkData> _link_data;
  std::map<std::string, StopData> _stop_data;
  std::vector<std::string> _link_names;
  std::map<std::string, std::vector<std::pair<std::string, std::vector<int>>>> _merged_links;
};

#endif //ARTIS_TRAFFIC_MICRO_TESTS_SIMULATOR_HPP
