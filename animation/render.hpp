/**
 * @file animation/render.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

#pragma once

#include "model.hpp"

#include <cairomm/context.h>
#include <map>
#include <unordered_map>

class Render {
public:
  Render(const Model &model, int width, int height);

  void display(const Cairo::RefPtr<Cairo::Context> &cr, const std::vector<std::string> &link_names) const;

  const Model &model() const { return _model; }

  double scale() const { return _scale; }

private:
  void draw_buildings(const Cairo::RefPtr<Cairo::Context> &cr) const;

  void draw_highways(const Cairo::RefPtr<Cairo::Context> &cr, const std::vector<std::string> &link_names) const;

  void draw_railways(const Cairo::RefPtr<Cairo::Context> &cr) const;

  void draw_leisure(const Cairo::RefPtr<Cairo::Context> &cr) const;

  void draw_water(const Cairo::RefPtr<Cairo::Context> &cr) const;

  void draw_land_uses(const Cairo::RefPtr<Cairo::Context> &cr) const;

  static void draw_surface(const Cairo::RefPtr<Cairo::Context> &cr,
                           const std::vector<std::pair<double, double>> &path);

  std::vector<std::pair<double, double>> path_from_way(const Model::Way &way) const;

  std::vector<std::pair<double, double>> path_from_MP(const Model::Multipolygon &mp) const;

  std::pair<double, double> to_point_2D(const Model::Node &node) const;

  const Model &_model;

  const int _width;
  const int _height;

  float _scale = 1.f;
  float _pixels_in_meter = 1.f;

  float _railway_outer_width = 3.f;
  float _railway_inner_width = 2.f;
};
