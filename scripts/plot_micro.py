from math import sqrt

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from itertools import cycle


def split_columns(column, max_col):
    pos = column.str.split(" ")
    pos = pos.dropna()
    VN = []
    for num_col in range(0, max_col):
        V_i = pos.str[num_col]
        V_i.replace('', np.nan, inplace=True)
        V_i = V_i.dropna().astype('float')
        V_i.replace(-1, np.nan, inplace=True)
        VN.append(V_i)

    return VN


def plot_observable(df, link, vehicle_indexes):
    data = []
    if df[":root:" + link + ":vehicle_indexes"].dtype == 'object':
        indexes = df[":root:" + link + ":vehicle_indexes"].str.split(" ")
        max_col = len(indexes[indexes.last_valid_index()]) - 1
        # print(max_col)
    else:
        max_col = 1

    lines = ["-", "--", "-.", ":"]
    linecycler = cycle(lines)
    name = ''
    columns = ['speed', 'position']

    for column in columns:
        if column == 'speed':
            data = df[":root:" + link + ":vehicle_speeds"]
            name = 'Vitesse'
            # plt.xlim(263, 280)
            # plt.axvline(x=266.82286547039018, color='r', linestyle='--', label='t')
            # plt.axvline(x=274.663851012, color='black', linestyle='--', label='t\'')
            # plt.ylim(0, 1)
        if column == 'position':
            data = df[":root:" + link + ":vehicle_positions"]
            name = 'Position'
            # plt.ylim(470, 501)
            # plt.xlim(25, 170)
        if max_col > 1:
            VN = split_columns(data, max_col)
        else:
            VN = data

        vehicle_indexes = [i for i in vehicle_indexes if i < len(VN)]
        if max_col > 1:
            for i in range(0, len(vehicle_indexes)):
                V = VN[vehicle_indexes[i]]
                V = V.dropna()
                times = df['time']
                times = times[V.first_valid_index():V.last_valid_index() + 1]
                # plt.xlim(600, 617)
                plt.plot(times, V, linestyle=next(linecycler), label='Vehicule ' + str(vehicle_indexes[i]))
        else:
            data = data.astype('str')
            pos = data.str.split(" ")
            pos = pos.dropna()
            V_i = pos.str[0]
            V_i.replace('', np.nan, inplace=True)
            V_i = V_i.dropna().astype('float')
            V_i.replace(-1, np.nan, inplace=True)
            times = df['time']
            times = times[V_i.first_valid_index():len(V_i) + 1]
            plt.plot(times, V_i, linestyle=next(linecycler), label='Vehicule')
        plt.title(name + ' des véhicules')
        plt.xlabel('Temps')
        plt.ylabel(name)
        plt.legend()
        plt.savefig(column + '.png')
        plt.close()


def plot_observable_multi_links(df, links, full_vehicle_indexes):
    speeds = [[], [], []]
    positions = [[], [], []]
    full_times = [[], [], []]
    for idx, link in enumerate(links):
        data = []
        vehicle_indexes = []
        if df[":root:" + link + ":vehicle_indexes"].dtype == 'object':
            indexes = df[":root:" + link + ":vehicle_indexes"].str.split(" ")
            max_col = len(indexes[indexes.last_valid_index()]) - 1
            split_indexes = []
            for num_col in range(0, max_col):
                temp_i = indexes.str[num_col]
                temp_i.replace('', np.nan, inplace=True)
                temp_i = temp_i.dropna().astype('float')
                temp_i.replace(-1, np.nan, inplace=True)
                split_indexes.append(temp_i)
            for vehicle_index in full_vehicle_indexes:
                for split in split_indexes:
                    if vehicle_index in split.values:
                        vehicle_indexes.append(vehicle_index)
            # print(max_col)
        else:
            max_col = 1
            indexes = df[":root:" + link + ":vehicle_indexes"]
            for vehicle_index in full_vehicle_indexes:
                if vehicle_index in indexes:
                    vehicle_indexes.append(vehicle_index)

        lines = ["-", "--", "-.", ":"]
        linecycler = cycle(lines)
        name = ''
        columns = ['speed', 'position']

        for column in columns:
            if column == 'speed':
                data = df[":root:" + link + ":vehicle_speeds"]
                name = 'Speed(m/s)'
                # plt.xlim(263, 280)
                # plt.axvline(x=266.82286547039018, color='r', linestyle='--', label='t')
                # plt.axvline(x=274.663851012, color='black', linestyle='--', label='t\'')
                # plt.ylim(0, 1)
            if column == 'position':
                data = df[":root:" + link + ":vehicle_positions"]
                name = 'Position(m)'
                # plt.ylim(470, 501)
                # plt.xlim(25, 170)
            if max_col > 1:
                VN = split_columns(data, max_col)
            else:
                VN = data
                index = df[":root:" + link + ":vehicle_indexes"][0]

            #vehicle_indexes = [i for i in vehicle_indexes if i < len(VN)]
            if max_col > 1:
                for i in range(0, len(vehicle_indexes)):
                    V = VN[i]
                    V = V.dropna()
                    V = V[0:V.last_valid_index()]
                    times = df['time']
                    times = times[V.first_valid_index():V.last_valid_index() + 1]
                    full_times[idx].append(times)
                    if column == 'speed':
                        speeds[idx].append(V)
                    else:
                        positions[idx].append(V)
                    #plt.plot(times, V, linestyle=next(linecycler), label='Vehicle ' + str(vehicle_indexes[i]))
            else:
                data = data.astype('str')
                data = data[data.first_valid_index():data.last_valid_index()]
                pos = data.str.split(" ")
                pos = pos.dropna()
                V_i = pos.str[0]
                V_i.replace('', np.nan, inplace=True)
                V_i = V_i.dropna().astype('float')
                V_i.replace(-1, np.nan, inplace=True)
                V_i = V_i[V_i.first_valid_index():V_i.last_valid_index()]
                times = df['time']
                times = times[V_i.first_valid_index():V_i.last_valid_index() + 1]
                full_times[idx].append(times)

                if column == 'speed':
                    speeds[idx].append(V_i)
                if column == 'position':
                    positions[idx].append(V_i)
                #plt.plot(times, V_i, linestyle=next(linecycler), label='Vehicle_' + str(int(index)))

    lines = ["-", "--", "-.", ":"]
    linecycler = cycle(lines)
    v_idx = 0
    for i in range(0, len(speeds)):
        for j in range(0, len(speeds[i])):
            t_ij = full_times[i][j]
            s_ij = speeds[i][j]
            plt.plot(t_ij, s_ij, linestyle=next(linecycler),
                     label='Vehicle_' + str(full_vehicle_indexes[v_idx]) + ' (L' + str(i + 1) + ')')
            v_idx += 1
    plt.title('Vehicle ' + 'speed')
    plt.xlabel('Time(s)')
    plt.ylabel('Speed(m/s)')
    plt.legend()
    plt.savefig('multi_' + 'speed' + '.png')
    plt.close()

    v_idx = 0
    for i in range(0, len(positions)):
        for j in range(0, len(positions[i])):
            t_ij = full_times[i][j]
            s_ij = positions[i][j]
            plt.plot(t_ij, s_ij, linestyle=next(linecycler),
                     label='Vehicle_' + str(full_vehicle_indexes[v_idx]) + ' (L' + str(i + 1) + ')')
            v_idx += 1
    plt.title('Vehicle ' + 'position')
    plt.xlabel('Time(s)')
    plt.ylabel('Position(m)')
    plt.legend()
    plt.savefig('multi_' + 'position' + '.png')
    plt.close()


def plot_double_observable(df, link, vehicle_indexes, df_e):
    data = []
    data_e = []
    if df[":root:" + link + ":vehicle_indexes"].dtype == 'object':
        indexes = df[":root:" + link + ":vehicle_indexes"].str.split(" ")
        max_col = len(indexes[indexes.last_valid_index()]) - 1
        # print(max_col)
    else:
        max_col = 1
    if df_e[":root:" + link + ":vehicle_indexes"].dtype == 'object':
        indexes_e = df_e[":root:" + link + ":vehicle_indexes"].str.split(" ")
        max_col_e = len(indexes_e[indexes_e.last_valid_index()]) - 1
        # print(max_col)
    else:
        max_col_e = 1

    lines = ["-", "--", "-.", ":"]
    linecycler = cycle(lines)
    name = ''
    columns = ['speed', 'position', 'delta_speed', 'delta_position']

    for column in columns:
        if column == 'speed' or column == 'delta_speed':
            data = df[":root:" + link + ":vehicle_speeds"]
            data_e = df_e[":root:" + link + ":vehicle_speeds"]
            name = 'Speed (m/s)'
            # plt.xlim(263, 280)
            # plt.axvline(x=266.82286547039018, color='r', linestyle='--', label='t')
            # plt.axvline(x=274.663851012, color='black', linestyle='--', label='t\'')
            # plt.ylim(0, 1)
        if column == 'position' or column == 'delta_position':
            data = df[":root:" + link + ":vehicle_positions"]
            data_e = df_e[":root:" + link + ":vehicle_positions"]
            name = 'Position (m)'
            # plt.ylim(470, 501)
            # plt.xlim(25, 170)
        if max_col > 1:
            VN = split_columns(data, max_col)
            VN_e = split_columns(data_e, max_col)
        else:
            VN = data
            VN_e = data_e

        vehicle_indexes = [i for i in vehicle_indexes if i < len(VN)]
        if max_col > 1:
            for i in range(0, len(vehicle_indexes)):
                V = VN[vehicle_indexes[i]]
                V_e = VN_e[vehicle_indexes[i]]
                V = V.dropna()
                V_e = V_e.dropna()
                times = df['time']
                times_e = df_e['time']
                times = times[V.first_valid_index():V.last_valid_index() + 1]
                times_e = times_e[V_e.first_valid_index():V_e.last_valid_index() + 1]
                # plt.xlim(600, 617)
                if column == 'speed' or column == 'position':
                    plt.plot(times, V, linestyle=next(linecycler), label='Time Vehicle ' + str(vehicle_indexes[i]))
                    plt.plot(times_e, V_e, linestyle=next(linecycler),
                             label='Event Vehicle ' + str(vehicle_indexes[i]))
                else:
                    V_rows = len(V)
                    V_e_rows = len(V_e)
                    if V_rows < V_e_rows:
                        V_e = V_e[V_e.first_valid_index():V_rows]
                    if V_rows > V_e_rows:
                        V = V[V.first_valid_index():V_e_rows]
                        times = times_e
                    plt.plot(times, V - V_e, linestyle=next(linecycler),
                             label='Delta Vehicle ' + str(vehicle_indexes[i]))
        else:
            data = data.astype('str')
            pos = data.str.split(" ")
            pos = pos.dropna()
            V_i = pos.str[0]
            V_i.replace('', np.nan, inplace=True)
            V_i = V_i.dropna().astype('float')
            V_i.replace(-1, np.nan, inplace=True)
            times = df['time']
            times = times[V_i.first_valid_index():len(V_i) + 1]
            plt.plot(times, V_i, linestyle=next(linecycler), label='Vehicule')

        if name == 'Speed (m/s)':
            title_name = 'speeds'
        if name == 'Position (m)':
            title_name = 'positions'
        plt.title('Vehicles ' + title_name)
        plt.xlabel('Time (s)')
        plt.ylabel(name)
        plt.legend()
        plt.savefig(column + '.png')
        plt.close()


def plot_double_observable_timestep(l_df, link, vehicle_indexes, l_df_e, timesteps):
    speeds_df = []
    positions_df = []
    speeds_diff = []
    positions_diff = []
    for timestep in timesteps:
        speeds_df.append([])
        positions_df.append([])
    # for vehicle_idx in vehicle_indexes:
    #     speeds_diff.append([])
    #     positions_diff.append([])
    for idx, df in enumerate(l_df):
        df_e = l_df_e[idx]
        data = []
        data_e = []
        if df[":root:" + link + ":vehicle_indexes"].dtype == 'object':
            indexes = df[":root:" + link + ":vehicle_indexes"].str.split(" ")
            max_col = len(indexes[indexes.last_valid_index()]) - 1
            # print(max_col)
        else:
            max_col = 1
        if df_e[":root:" + link + ":vehicle_indexes"].dtype == 'object':
            indexes_e = df_e[":root:" + link + ":vehicle_indexes"].str.split(" ")
            max_col_e = len(indexes_e[indexes_e.last_valid_index()]) - 1
            # print(max_col)
        else:
            max_col_e = 1

        lines = ["-", "--", "-.", ":"]
        linecycler = cycle(lines)
        name = ''
        columns = ['speed', 'position']

        for column in columns:
            if column == 'speed' or column == 'delta_speed':
                data = df[":root:" + link + ":vehicle_speeds"]
                data_e = df_e[":root:" + link + ":vehicle_speeds"]
                name = 'Speed (m/s)'
                # plt.xlim(263, 280)
                # plt.axvline(x=266.82286547039018, color='r', linestyle='--', label='t')
                # plt.axvline(x=274.663851012, color='black', linestyle='--', label='t\'')
                # plt.ylim(0, 1)
            if column == 'position' or column == 'delta_position':
                data = df[":root:" + link + ":vehicle_positions"]
                data_e = df_e[":root:" + link + ":vehicle_positions"]
                name = 'Position (m)'
                # plt.ylim(470, 501)
                # plt.xlim(25, 170)
            if max_col > 1:
                VN = split_columns(data, max_col)
                VN_e = split_columns(data_e, max_col)
            else:
                VN = data
                VN_e = data_e

            vehicle_indexes = [i for i in vehicle_indexes if i < len(VN)]
            if max_col > 1:
                for i in range(0, len(vehicle_indexes)):
                    V = VN[vehicle_indexes[i]]
                    V_e = VN_e[vehicle_indexes[i]]
                    V = V.dropna()
                    V_e = V_e.dropna()
                    times = df['time']
                    times_e = df_e['time']
                    times = times[V.first_valid_index():V.last_valid_index() + 1]
                    times_e = times_e[V_e.first_valid_index():V_e.last_valid_index() + 1]

                    V_rows = len(V)
                    V_e_rows = len(V_e)
                    if V_rows < V_e_rows:
                        V_e = V_e[V_e.first_valid_index():V_rows]
                    if V_rows > V_e_rows:
                        V = V[V.first_valid_index():V_e_rows]
                        times = times_e
                    if column == 'speed':
                        speeds_df[idx].append(abs(V_e - V))
                    if column == 'position':
                        positions_df[idx].append(abs(V_e - V))
                     #plt.plot(times, V - V_e, linestyle=next(linecycler),
                     #           label='Delta Vehicle ' + str(vehicle_indexes[i]))
            else:
                data = data.astype('str')
                pos = data.str.split(" ")
                pos = pos.dropna()
                V_i = pos.str[0]
                V_i.replace('', np.nan, inplace=True)
                V_i = V_i.dropna().astype('float')
                V_i.replace(-1, np.nan, inplace=True)
                times = df['time']
                times = times[V_i.first_valid_index():len(V_i) + 1]
                #plt.plot(times, V_i, linestyle=next(linecycler), label='Vehicule')

    for column in ['speed', 'position']:
        for idx, df in enumerate(l_df):
            spd_d = 0
            pos_d = 0
            for vehicle_idx in range(0, len(vehicle_indexes)):
                if column == 'speed':
                    v_df = pd.DataFrame(speeds_df[idx][vehicle_idx])
                    # speeds_diff[vehicle_idx].append(v_df.mean())
                    spd_d += v_df.mean()
                if column == 'position':
                    v_df = pd.DataFrame(positions_df[idx][vehicle_idx])
                    # positions_diff[vehicle_idx].append(v_df.mean())
                    pos_d += v_df.mean()
            if column == 'speed':
                speeds_diff.append(spd_d)
            if column == 'position':
                positions_diff.append(pos_d)

        # for idx, vehicle_idx in enumerate(vehicle_indexes):
        #     if column == 'speed':
        #         plt.title('Mean delta speed by timestep')
        #         plt.xlabel('Timestep(s)')
        #         plt.ylabel('Mean delta speed')
        #         plt.plot(timesteps, speeds_diff[idx], label='Vehicle ' + str(vehicle_idx))
        #     if column == 'position':
        #         plt.title('Mean delta position by timestep')
        #         plt.xlabel('Timestep(s)')
        #         plt.ylabel('Mean delta position')
        #         plt.plot(timesteps, speeds_diff[idx], label='Vehicle ' + str(vehicle_idx))

        if column == 'speed':
            # plt.title('Sum of mean delta speed by timestep')
            # plt.xlabel('Timestep(s)')
            # plt.ylabel('Sum of mean delta speed')
            plt.plot(timesteps, speeds_diff, label='Speed')
        if column == 'position':
            # plt.title('Sum of mean delta position by timestep')
            # plt.xlabel('Timestep(s)')
            # plt.ylabel('Sum of mean delta position')
            plt.plot(timesteps, positions_diff, label='Position')
    plt.title('Sum of mean deltas by timestep')
    plt.xlabel('Timestep(s)')
    plt.ylabel('Sum of mean deltas')
    plt.legend()
    plt.savefig('timestep_delta.png')
    plt.close()


def gm_speed(df, max_col):
    # data_time = df["time"]
    # data_pos = df[":root:link:vehicle_positions"]
    VN_pos = df[2]
    # VN_pos = split_columns(data_pos, max_col)
    # data_speed = df[":root:link:vehicle_speed"]
    # VN_speed = split_columns(data_speed, max_col)
    VN_speed = df[1]
    accelerations = [[]]
    speeds = [[]]
    positions = [[]]
    alpha = 1
    l = 1
    m = 1
    max_acceleration = 1
    min_follow_position = 140
    reaction_time = 1
    time_step = 0.1
    previous_time = int(reaction_time / time_step)
    for num_v in range(0, max_col):
        if num_v == 0:
            # for num_row, value in VN_pos[num_v].items():
            for num_row, value in VN_pos.items():
                accelerations[0].append(max_acceleration)
                # speeds[0].append(VN_speed[num_v][num_row])
                speeds[0].append(VN_speed[num_row])
                positions[0].append(VN_pos[num_row])
                # positions[0].append(VN_pos[num_v][num_row])
        else:
            accelerations.append([])
            speeds.append([])
            positions.append([])
            i = 0
            while i < df[2 + num_v].first_valid_index():
                # while i < VN_pos[num_v].first_valid_index():
                accelerations[num_v].append(-100)
                speeds[num_v].append(-1)
                positions[num_v].append(-1)
                i += 1
            simulated_time = 0
            for num_row, value in df[2 + num_v].items():
                # for num_row, value in VN_pos[num_v].items():
                if simulated_time < previous_time:
                    accelerations[num_v].append(0)
                    # speeds[num_v].append(VN_speed[num_v][num_row])
                    speeds[num_v].append(df[2 + num_v][num_row])
                    positions[num_v].append(min_follow_position)
                    simulated_time += 1
                else:
                    delta_x = positions[num_v - 1][num_row - previous_time] - positions[num_v][num_row - previous_time]
                    delta_v = speeds[num_v - 1][num_row - previous_time] - speeds[num_v][num_row - previous_time]
                    acceleration = (alpha * pow(speeds[num_v][num_row - 1], m)) / (pow(delta_x, l)) * delta_v
                    speed = speeds[num_v][num_row - 1] + accelerations[num_v][num_row - 1] * time_step
                    position = positions[num_v][num_row - 1] + speeds[num_v][num_row - 1] * time_step \
                               + 0.5 * (accelerations[num_v][num_row - 1] * pow(time_step, 2))
                    accelerations[num_v].append(acceleration)
                    speeds[num_v].append(speed)
                    positions[num_v].append(position)

    new_speeds = []
    new_pos = []
    new_accel = []
    for i in range(0, max_col):
        speeds_1 = [speed for speed in speeds[i]]
        speeds_1 = pd.Series(speeds_1)
        speeds_1.replace(-1, np.nan, inplace=True)
        new_speeds.append(speeds_1)
        pos_1 = [pos for pos in positions[i]]
        pos_1 = pd.Series(pos_1)
        pos_1.replace(-1, np.nan, inplace=True)
        new_pos.append(pos_1)
        accel_1 = [a for a in accelerations[i]]
        accel_1 = pd.Series(accel_1)
        accel_1.replace(-100, np.nan, inplace=True)
        new_accel.append(accel_1)

    title = 'accel'
    print(new_accel[1].to_string())
    # plt.plot(VN_speed[1], label='DEVS')
    # plt.plot(data_time[0:len(new_pos[1])], new_accel[1], label='GM')
    # plt.plot(df[0], new_accel[1], label='GM')
    plt.plot(df[0], new_pos[1])
    # plt.plot(new_pos[0], label='V1')
    # plt.plot(new_pos[1], label='V2')
    plt.xlabel('time')
    plt.title(title)
    plt.legend()
    plt.show()


def compute_vn(t, t0, a, vn_t0):
    vn = vn_t0 + a * (t - t0)
    return vn if vn > 0 else 0


def compute_xn(t, t0, a, xn_t0, vn_t0):
    if compute_vn(t, t0, a, vn_t0) > 0:
        return xn_t0 + vn_t0 * (t - t0) + 0.5 * a * (t - t0) * (t - t0)
    else:
        d = vn_t0 / -a

        return xn_t0 + vn_t0 * d + 0.5 * a * d * d


def gipps_speed(df, link, lim_col, V_n, dt, tau, gap, column, vehicle_indexes, ignore_leader):
    data_time = df["time"]

    if df[":root:" + link + ":vehicle_indexes"].dtype == 'object':
        indexes = df[":root:" + link + ":vehicle_indexes"].str.split(" ")
        max_col = min(len(indexes[indexes.last_valid_index()]) - 1, lim_col)
    else:
        max_col = 1

    data_pos = df[":root:" + link + ":vehicle_positions"]
    data_pos = split_columns(data_pos, lim_col)
    leader_pos = data_pos[0]

    data_speed = df[":root:" + link + ":vehicle_speeds"]
    data_speed = split_columns(data_speed, lim_col)
    leader_speed = data_speed[0]

    followers_pos = []
    followers_speed = []

    previous_time = int(tau / dt)

    if 0 in vehicle_indexes:
        vehicle_indexes = [i - 1 for i in vehicle_indexes]
        vehicle_indexes.remove(-1)

    start_points = []
    start_speeds = []
    for i in range(1, max_col):
        start_points.append(int(data_time[data_pos[i].first_valid_index()]))
        start_speeds.append(data_speed[i][data_speed[i].first_valid_index()])
        followers_pos.append(data_pos[i])
        followers_speed.append(data_speed[i])

    link_length = data_pos[0].max()

    times = []
    v_lead_gipps = []
    x_lead_gipps = []
    v_follows_gipps = []
    x_follows_gipps = []
    devs_follows_x = []
    devs_follows_v = []
    devs_times = []
    devs_times_x = []
    for i in range(1, max_col):
        v_follows_gipps.append([])
        x_follows_gipps.append([])

    a = 1
    b = -1
    b_hat = -1

    leader_pos = [x for x in leader_pos]
    leader_speed = leader_speed[0:len(leader_pos)]
    VN_1_pos = [x for x in followers_pos[0]]
    leader_times = data_time[0:len(leader_speed)]

    done = []
    for k in range(0, max_col - 1):
        done.append(False)

    for i in range(0, len(start_points)):
        start_points[i] = int(start_points[i] * tau / dt)
    for i in range(0, start_points[0]):
        time = data_time[i]
        if time % tau == 0 and i >= leader_speed.first_valid_index():
            times.append(time)
            v_lead_gipps.append(leader_speed[i])
            x_lead_gipps.append(leader_pos[i])
            for k in range(0, max_col - 1):
                v_follows_gipps[k].append(-1)
                x_follows_gipps[k].append(-1)
    for i in range(start_points[0], len(leader_pos)):
        time = data_time[i]
        if time % tau == 0:
            times.append(time)
            v_lead_gipps.append(leader_speed[i])
            x_lead_gipps.append(leader_pos[i])
        for k in range(0, max_col - 1):
            if time < start_points[k] / previous_time:
                if time % tau == 0:
                    v_follows_gipps[k].append(-1)
                    x_follows_gipps[k].append(-1)
            else:
                if time == start_points[k] / previous_time:
                    v_follows_gipps[k].append(start_speeds[k])
                    x_follows_gipps[k].append(VN_1_pos[0])
                # elif time <= start_points[k] / previous_time + tau:
                #    if time % tau == 0:
                #        v_follows_gipps[k].append(v_follows_gipps[k][-1])
                #        x_follows_gipps[k].append(x_follows_gipps[k][-1] + v_follows_gipps[k][-1] * tau)
                else:
                    if time % tau == 0:
                        if k == 0:
                            v_first_gipps = v_lead_gipps
                            x_first_gipps = x_lead_gipps
                        else:
                            v_first_gipps = v_follows_gipps[k - 1]
                            x_first_gipps = x_follows_gipps[k - 1]
                        if x_follows_gipps[k][-1] >= link_length or done[k]:
                            v_follows_gipps[k].append(-1)
                            x_follows_gipps[k].append(-1)
                            done[k] = True
                        else:
                            if x_first_gipps[-1] == -1:
                                v_follows_gipps[k].append(v_follows_gipps[k][-1] + a * tau)
                                x_follows_gipps[k].append((x_follows_gipps[k][-1])
                                                          + v_follows_gipps[k][-1] * tau + 1 / 2
                                                          * a * tau * tau)
                            else:
                                acceleration_limit = compute_acceleration_limit(tau, V_n, a, v_follows_gipps[k])
                                deceleration_limit = compute_deceleration_limit(tau, b, b_hat, gap, v_first_gipps,
                                                                                v_follows_gipps[k],
                                                                                x_first_gipps, x_follows_gipps[k])
                                print(time, acceleration_limit, deceleration_limit, x_first_gipps[-2])
                                v_follows_gipps[k].append(max(min(acceleration_limit, deceleration_limit), 0))
                                x_follows_gipps[k].append(x_follows_gipps[k][-1] + v_follows_gipps[k][-1] * tau)

    v_follows_gipps, x_follows_gipps = clean_gipps_dataframe(v_follows_gipps, x_follows_gipps)

    reindex_devs_dataframe(data_pos, data_speed, data_time, devs_follows_v, devs_follows_x, devs_times, devs_times_x,
                           max_col)

    times = pd.Series(times)

    lines = ["-", "--", "-.", ":"]
    linecycler = cycle(lines)
    if column == 'speed':
        if not ignore_leader:
            plt.plot(leader_times, leader_speed, label='Leader')
        for i in range(0, len(vehicle_indexes)):
            k = vehicle_indexes[i]
            plt.plot(devs_times[k], devs_follows_v[k], label='Véhicule DEVS_' + str(k), linestyle=next(linecycler))
            plt.plot(times, v_follows_gipps[k], label='Véhicule Gipps_' + str(k), linestyle=next(linecycler))
        plt.title('Vitesse des véhicules')
        plt.xlabel('Temps (s)')
        plt.ylabel('Vitesse (m/s)')
    if column == 'position':
        if not ignore_leader:
            plt.plot(leader_times, leader_pos, label='Leader')
        for i in range(0, len(vehicle_indexes)):
            k = vehicle_indexes[i]
            plt.plot(devs_times_x[k], devs_follows_x[k], label='Véhicule DEVS_' + str(k), linestyle=next(linecycler))
            plt.plot(times, x_follows_gipps[k], label='Véhicule Gipps_' + str(k), linestyle=next(linecycler))
        plt.title('Position des véhicules')
        plt.xlabel('Temps')
        plt.ylabel('Position')
        plt.ylim(0, link_length)
    if column == 'delta':
        for i in range(0, len(vehicle_indexes)):
            k = vehicle_indexes[i]
            print("k", k)
            sp = int(start_points[k] / previous_time)
            print("sp", sp)
            print(len(times))
            times_k = times[0:times.last_valid_index() + 1]
            print("time", times)
            adapt_devs_dataframe_delta(devs_follows_v, devs_follows_x, k, previous_time)
            plt.plot(times_k, v_follows_gipps[k] - devs_follows_v[k], label='Delta vitesse_' + str(k + 1))
            plt.plot(times_k, x_follows_gipps[k] - devs_follows_x[k], label='Delta position_' + str(k + 1))
        plt.title('Deltas de vitesse et position entre véhicules DEVS et Gipps')
        plt.xlabel('temps')
        plt.ylabel('delta')
    plt.legend()
    plt.show()


def reindex_devs_dataframe(data_pos, data_speed, data_time, devs_follows_v, devs_follows_x, devs_times, devs_times_x,
                           max_col):
    for k in range(1, max_col):
        devs_follows_x_k = data_pos[k]
        devs_follows_x_k = devs_follows_x_k.reindex(list(range(0, devs_follows_x_k.last_valid_index()))).reset_index(
            drop=True)
        devs_follows_x.append(devs_follows_x_k)
        devs_follows_v_k = data_speed[k]
        devs_follows_v_k = devs_follows_v_k.reindex(list(range(0, devs_follows_v_k.last_valid_index()))).reset_index(
            drop=True)
        devs_follows_v.append(devs_follows_v_k)
        devs_times.append(data_time[0:devs_follows_v_k.last_valid_index() + 1])
        devs_times_x.append(data_time[0:devs_follows_x_k.last_valid_index() + 1])


def clean_gipps_dataframe(v_follows_gipps, x_follows_gipps):
    x_follows_gipps = pd.DataFrame(x_follows_gipps)
    x_follows_gipps.replace(-1, np.nan, inplace=True)
    x_follows_gipps = x_follows_gipps.T
    v_follows_gipps = pd.DataFrame(v_follows_gipps)
    v_follows_gipps.replace(-1, np.nan, inplace=True)
    v_follows_gipps = v_follows_gipps.T
    return v_follows_gipps, x_follows_gipps


def adapt_devs_dataframe_delta(devs_follows_v, devs_follows_x, k, previous_time):
    rows_to_delete_v = []
    rows_to_delete_x = []
    for i in range(0, len(devs_follows_v[k])):
        if i % previous_time != 0:
            rows_to_delete_v.append(i)
    for i in range(0, len(devs_follows_x[k])):
        if i % previous_time != 0 or i > len(devs_follows_v[k]):
            rows_to_delete_x.append(i)
    devs_follows_v[k] = devs_follows_v[k].drop(rows_to_delete_v).reset_index(drop=True)
    devs_follows_x[k] = devs_follows_x[k].drop(rows_to_delete_x).reset_index(drop=True)


def compute_deceleration_limit(tau, b, b_hat, gap, vn, vn_1, xn, xn_1):
    return b * tau + sqrt(
        pow(b, 2) * pow(tau, 2) - b * (2 * (xn[-2] - gap - xn_1[-1]) - vn_1[-1] *
                                       tau - pow(vn[-2], 2) / b_hat))


def compute_acceleration_limit(tau, Vn_1, a, vn_1):
    return vn_1[-1] + 2.5 * a * tau * (1 - vn_1[-1] / Vn_1) \
           * pow(0.025 + vn_1[-1] / Vn_1, 0.5)


def calculate_gipps_event_count(df, vehicle_numbers, tau, dt):
    data_time = df["time"]

    vehicle_frequency = 0
    for i in range(0, len(data_time)):
        time = data_time[i]
        if time % tau == 0:
            if not np.isnan(vehicle_numbers[int(time * tau / dt)]):
                vehicle_frequency += vehicle_numbers[int(time * tau / dt)]

    return vehicle_frequency


def plot_event_frequency_by_mean(link, lengths, means, seeds, tau, dt):
    for length in lengths:
        devs_count = []
        gipps_count = []
        max_time = 600
        vehicle_sec = []
        vehicle_sec_e = []
        vehicle_avg = []
        vehicle_avg_e = []
        for mean in means:
            devs_count_seed = 0
            gipps_count_seed = 0
            vehicle_sec_seed = 0
            vehicle_sec_seed_e = 0
            vehicle_avg_seed = 0
            vehicle_avg_seed_e = 0
            for seed in seeds:
                filename = '../cmake-build-debug/tests/traffic/micro/Link_stop_length_' + str(length) + '_mean_' + str(
                    mean) + '_seed_' + str(seed) + '.csv'
                filename_e = '../cmake-build-debug/tests/traffic/micro/Link_stop_event_length_' + str(
                    length) + '_mean_' + str(mean) + '_seed_' + str(seed) + '.csv'
                df = pd.read_csv(filename, sep=";")
                df_e = pd.read_csv(filename_e, sep=";")
                vehicle_count = 0
                vehicle_count_e = 0
                if df[":root:" + link + ":vehicle_indexes"].dtype == 'object':
                    indexes = df[":root:" + link + ":vehicle_indexes"].str.split(" ")
                    vehicle_count = len(indexes[indexes.last_valid_index()]) - 1
                if df_e[":root:" + link + ":vehicle_indexes"].dtype == 'object':
                    indexes_e = df_e[":root:" + link + ":vehicle_indexes"].str.split(" ")
                    vehicle_count_e = len(indexes_e[indexes_e.last_valid_index()]) - 1

                vehicle_numbers = df[":root:" + link + ":vehicle_number"]
                vehicle_numbers_e = df_e[":root:" + link + ":vehicle_number"]
                vehicle_event_count = df[":root:" + link + ":event_count"]
                vehicle_event_count_e = df_e[":root:" + link + ":event_count"]
                devs_count_seed += vehicle_event_count_e[vehicle_event_count_e.last_valid_index() - 1] / vehicle_count_e
                gipps_count_seed += vehicle_event_count[vehicle_event_count.last_valid_index() - 1] / vehicle_count

                vehicle_avg_seed += vehicle_numbers.mean()
                vehicle_avg_seed_e += vehicle_numbers_e.mean()
                vehicle_sec_seed += vehicle_count / max_time
                vehicle_sec_seed_e += vehicle_count_e / max_time

            devs_count.append(devs_count_seed / len(seeds))
            gipps_count.append(gipps_count_seed / len(seeds))
            vehicle_sec.append(vehicle_sec_seed / len(seeds))
            vehicle_sec_e.append(vehicle_sec_seed_e / len(seeds))
            vehicle_avg.append(vehicle_avg_seed / len(seeds))
            vehicle_avg_e.append(vehicle_avg_seed_e / len(seeds))

        # plt.plot(vehicle_sec_e, devs_count, label='DEVS')
        # plt.plot(vehicle_sec, gipps_count, label='Gipps')
        # plt.plot(vehicle_avg_e, devs_count, label='DEVS')
        # plt.plot(vehicle_avg, gipps_count, label='Gipps')

        gipps_count = pd.Series(gipps_count)
        devs_count = pd.Series(devs_count)
        mean_vehicle_avg = (pd.Series(vehicle_avg) + pd.Series(vehicle_avg_e)) / 2
        #plt.plot(mean_vehicle_avg, (gipps_count - devs_count) / gipps_count * 100, label='Gipps - DEVS ' + str(length))
        # plt.plot(mean_vehicle_avg, (gipps_count - devs_count), label=str(length) + 'm Link')
        plt.plot(mean_vehicle_avg, (gipps_count), label=str(length) + 'm Link')
    # plt.xlabel('Véhicule par seconde')
    plt.xlabel('Average vehicles number')
    plt.ylabel('Average events number')
    plt.legend()
    plt.title('Event: Average events number', wrap=True)
    # plt.show()
    plt.savefig('eventfreq_time.png')


def main_single():
    link = 'link_1'
    #    link = 'L1'
    file = '../cmake-build-debug/tests/traffic/micro/Link_1_t.csv'
    file_e = '../cmake-build-debug/tests/traffic/micro/Link_1_e.csv'
    # file = '../cmake-build-debug/tests/traffic/micro/Link_node_1.csv'
    df = pd.read_csv(file, sep=";")
    df_e = pd.read_csv(file_e, sep=";")
    vehicles_indexes = [0,1,2,3]
    plot_observable(df, link, vehicles_indexes)
    plot_double_observable(df, link, vehicles_indexes, df_e)
    V_n = 10
    dt = 1
    tau = 1
    gap = 7.5
    column = 'speed'
    max_col = 300
    ignore_leader = False
    # gipps_speed(df, link, max_col, V_n, dt, tau, gap, column, vehicles_indexes, ignore_leader)

    min_length = 70
    max_length = 1070
    step_length = 200
    lengths = []
    for i in range(min_length, max_length, step_length):
        lengths.append(i)
    min_mean = 5
    max_mean = 30
    step_mean = 5
    means = []
    for i in range(min_mean, max_mean, step_mean):
        means.append(i)
    min_seed = 100
    max_seed = 1000
    step_seed = 100
    seeds = []
    for i in range(min_seed, max_seed, step_seed):
        seeds.append(i)
    # plot_event_frequency_by_mean(link, lengths, means, seeds, tau, dt)


def main_single_timesteps():
    link = 'link_1'
    file_e = '../cmake-build-debug/tests/traffic/micro/Link_1_e.csv'
    df_e = pd.read_csv(file_e, sep=";")
    vehicles_indexes = [0, 1, 2, 3]
    l_df = []
    l_df_e = []
    timesteps = [1, 0.5, 0.25, 0.2, 0.1, 0.0625, 0.05]
    for timestep in timesteps:
        file = '../cmake-build-debug/tests/traffic/micro/Link_1_t_' + str(timestep) + '.csv'
        df = pd.read_csv(file, sep=";")
        l_df.append(df)
        if timestep >= 0.1:
            l_df_e.append(df_e)
        else:
            file_ts_e = '../cmake-build-debug/tests/traffic/micro/Link_1_e_' + str(timestep) + '.csv'
            ts_df_e = pd.read_csv(file_ts_e, sep=";")
            l_df_e.append(ts_df_e)

    plot_double_observable_timestep(l_df, link, vehicles_indexes, l_df_e, timesteps)


def main_multi():
    dynamic = 'time'
    #time_durations = [4.5121, 4.60551, 3.44581, 2.77972, 2.75545, 2.44188, 2.28602, 2.14346, 2.08346]
    time_durations = [22.1083, 16.6024, 13.9554, 12.8089, 11.7748, 11.1549, 10.688, 10.3312, 10.0484]
    #event_durations = [2.74723, 2.63384, 2.64117, 2.193, 2.18327, 2.02646, 2.05079, 2.04783, 1.96952]
    event_durations = [8.54567, 7.87676, 7.61033, 7.50689, 7.8133, 7.98445, 7.61373, 7.52762, 7.60755]
    for dynamic in ['time', 'event']:
        vehicle_counts = []
        event_counts = []
        durations = []
        for i in range(1, 10):
            file_generator = '../cmake-build-release/tests/traffic/micro/Generator_' + dynamic + '_set' + str(i) + '.csv'
            df_generator = pd.read_csv(file_generator, sep=";")
            cols_generator = df_generator.filter(like='counter')
            vehicle_count = int(cols_generator.sum(axis=1).iloc[-2])
            vehicle_counts.append(vehicle_count)
            print('Set ' + str(i))
            print(' Vehicle count: ' + str(vehicle_count))

            file_link = '../cmake-build-release/tests/traffic/micro/Link_' + dynamic + '_set' + str(i) + '.csv'
            df_link = pd.read_csv(file_link, sep=";", low_memory=False)
            cols_link = df_link.filter(like='event_count')
            event_count = int(cols_link.sum(axis=1).iloc[-2])
            event_counts.append(event_count)
            print('Event count: ' + str(event_count))
        #plt.plot(vehicle_counts, event_counts, label=dynamic)
        if dynamic == 'time':
            durations = time_durations
        else:
            durations = event_durations
        plt.plot(vehicle_counts, durations, label=dynamic)
    plt.xlabel('Vehicles number')
    #plt.ylabel('Events number')
    plt.ylabel('Duration(s)')
    plt.legend()
    plt.title('Duration', wrap=True)
    #plt.title('Events number', wrap=True)
    # plt.show()
    plt.savefig('duration' + 'time_event' + '_multi.png')
    #plt.savefig('eventcount' + 'time_event' + '_multi.png')


def main_multi_links_dynamic():
    links = ['L1', 'L2', 'L3']
    #    link = 'L1'
    file = '../cmake-build-debug/tests/traffic/micro/Link_global_stop.csv'
    df = pd.read_csv(file, sep=";")
    vehicles_indexes = [0, 100, 101, 200]
    plot_observable_multi_links(df, links, vehicles_indexes)


if __name__ == "__main__":
    main_multi()
