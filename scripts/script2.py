import sys
import numpy as np
import pandas
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error


def read_data(filepath, filepath2, sensor):
    csv = pandas.read_csv(filepath, sep=";", header=0)
    csv2 = pandas.read_csv(filepath2, sep=";", header=0)

    vl_real = []
    pl_real = []
    t_real = []
    vl_calc = []
    pl_calc = []
    t_calc = []

    line = 0
    for _, row in csv.iterrows():
        print("Reading real line ", line)
        if 18000 <= row["time_[s]"] <= 43200:
            if row["sensor"] == sensor and row["day"] == "2019-11-13":
                vl_real.append(row["VL/h"])
                pl_real.append(row["PL/h"])
                t_real.append(row["time_[s]"])
        line += 1
    line = 0
    for _, row in csv2.iterrows():
        print("Reading calc line ", line)
        if 18000 <= row[":root:sensor_" + sensor + ":TimeObs"] <= 43200:
            vl_calc.append(row[":root:sensor_" + sensor + ":VL/h"])
            pl_calc.append(row[":root:sensor_" + sensor + ":PL/h"])
            t_calc.append(row[":root:sensor_" + sensor + ":TimeObs"])
        line += 1

    return vl_real, pl_real, t_real, vl_calc, pl_calc, t_calc


def print_plot(vl_real, pl_real, t_real, vl_calc, pl_calc, t_calc, sensor):
    colors = ["b", "g", "r", "c"]

    fig, ax = plt.subplots()
    ax.plot(t_real, vl_real, color='blue', label='vl_real')
    ax.plot(t_real, pl_real, color='red', label='pl_real')
    ax.plot(t_calc, vl_calc, color='cyan', label='vl_calc')
    ax.plot(t_calc, pl_calc, color='green', label='pl_calc')
    ax.legend()
    plt.ylabel("Vehicle number by hour")
    plt.xlabel("Time")
    plt.title("Sensor " + str(sensor))
    plt.grid()
    plt.show()


def main(argv):
    filepath = "./../data/A15-A115/loop_data.csv"
    filepath2 = argv[0]
    sensor = argv[1]
    vl_real, pl_real, t_real, vl_calc, pl_calc, t_calc = read_data(filepath, filepath2, sensor)
    diff = len(vl_real) - len(vl_calc)
    del vl_real[-diff:]
    del pl_real[-diff:]
    del t_real[-diff:]
    print("MSE VL/h => ", mean_squared_error(vl_real, vl_calc))
    print("MSE PL/h => ", mean_squared_error(pl_real, pl_calc))
    print_plot(vl_real, pl_real, t_real, vl_calc, pl_calc, t_calc, sensor)


if __name__ == '__main__':
    main(sys.argv[1:])
