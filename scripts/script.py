import sys
import numpy as np
import pandas
import matplotlib.pyplot as plt
import json
from re import match


def is_json(myjson):
    try:
        json.loads(myjson)
    except ValueError as e:
        return False
    except TypeError as e:
        return False
    return True


def read_data(filepath):
    csv = pandas.read_csv(filepath, sep=";", header=0)
    vehicles_ids = []
    vehicles_history = []
    links_ids = []
    links_history = []
    ligne = 0
    for _, row in csv.iterrows():
        print("Reading ligne ", ligne)
        for i in range(0, len(row)):
            if is_json(row[i]):
                history = json.loads(row[i])
                if not history['id'] in vehicles_ids:
                    vehicles_ids.append(history['id'])
                    vehicles_history.append([])
                vehicles_history[vehicles_ids.index(history['id'])].append(history)
                if not history['link'] in links_ids:
                    links_ids.append(history['link'])
                    links_history.append([])
                links_history[links_ids.index(history['link'])].append(history)
        ligne += 1

    return vehicles_ids, vehicles_history, links_ids, links_history


def get_index_of_link_id(links_ids, link_id):
    try:
        return links_ids.index(link_id)
    except ValueError as e:
        return -1


def get_index_of_vehicle_id(vehicles_ids, vehicle_id):
    try:
        return vehicles_ids.index(vehicle_id)
    except ValueError as e:
        return -1


def print_links_ids(links_ids):
    sorted_links_ids = sorted(links_ids)
    for link_id in sorted_links_ids:
        print(link_id)


def print_vehicles_ids(vehicles_ids):
    sorted_vehicles_ids = sorted(vehicles_ids)
    for vehicle_id in sorted_vehicles_ids:
        print(vehicle_id)


def print_link(link_history, binf=0, bsup=20):
    binf = min(max(binf, 0), len(link_history))
    bsup = min(max(bsup, binf + 1), len(link_history))
    x = [data["t_entry"] for data in link_history]
    y = [data["t_estimated_end"] for data in link_history]
    z = [data["t_real_end"] for data in link_history]

    colors = ["b", "g", "r", "c", "m", "y", "k"]

    fig, ax = plt.subplots()
    for i in range(binf, bsup):
        queue_rank = 0
        for j in range(binf, i):
            if (y[i] < z[j]):
                queue_rank += 1
        ax.plot([x[i], y[i]], [0, 1 - 0.02 * queue_rank], colors[i % len(colors)])
        ax.plot([y[i], z[i]], [1 - 0.02 * queue_rank, 1], colors[i % len(colors)])
    ax.set_yticks([0, 1])
    ax.set_yticklabels(["Amont", "Aval"])
    plt.ylabel("Trajet")
    plt.xlabel("t")
    plt.title("Link " + str(link_history[0]["link"]) + " : Vehicles " + str(binf + 1) + " at " + str(bsup))
    plt.grid()
    plt.show()


def print_vehicle(vehicle_history):
    x = [data["t_entry"] for data in vehicle_history]
    y = [data["t_estimated_end"] for data in vehicle_history]
    z = [data["t_real_end"] for data in vehicle_history]
    links = [data["link"] for data in vehicle_history]

    colors = ["b", "g", "r", "c", "m", "y", "k"]

    fig, ax = plt.subplots()
    ticks = []
    ticks_labels = []
    for i in range(0, len(vehicle_history)):
        ax.plot([x[i], y[i]], [i, i + 1], colors[i % len(colors)])
        ax.plot([y[i], z[i]], [i + 1, i + 1], colors[i % len(colors)])
        if not i in ticks:
            ticks.append(i)
        if not (i + 1) in ticks:
            ticks.append(i + 1)
        if i == 0:
            ticks_labels.append("Amont lien " + str(links[i]))
            ticks_labels.append("Aval lien " + str(links[i]))
        else:
            ticks_labels[i] = "Amont lien " + str(links[i]) + "\n" + ticks_labels[i]
            ticks_labels.append("Aval lien " + str(links[i]))
    ax.set_yticks(ticks)
    ax.set_yticklabels(ticks_labels)
    plt.ylabel("Trajet")
    plt.xlabel("t")
    plt.title('Vehicle history' + str(vehicle_history[0]["id"]))
    plt.grid()
    plt.show()


def menu(vehicles_ids, vehicles_history, links_ids, links_history):
    print("--- MENU ---")
    print("1 -> print links_ids")
    print("2 -> print vehicles_ids")
    print("3 link_id [binf] [bsup] -> print link history")
    print("4 vehicle_id -> print vehicle history")
    print("5 -> quit")

    choice = input()

    if len(choice) > 0:
        if choice[0] == "1" and len(choice) == 1:
            print_links_ids(links_ids)
            return True
        elif choice[0] == "2" and len(choice) == 1:
            print_vehicles_ids(vehicles_ids)
            return True
        elif choice[0] == "3":
            if len(choice) > 1 and choice[1] == " ":
                params = choice.split(" ")
                if len(params) == 2:
                    link_index = get_index_of_link_id(links_ids, int(params[1]))
                    if link_index != -1:
                        print_link(links_history[link_index])
                    else:
                        print("Invalid link_id")
                elif len(params) == 4:
                    link_index = get_index_of_link_id(links_ids, int(params[1]))
                    if link_index != -1:
                        print_link(links_history[link_index], int(params[2]), int(params[3]))
                    else:
                        print("Invalid link_id")
                else:
                    print("Unexpected number of parameters")
            else:
                print("Unexpected number of parameters")
            return True
        elif choice[0] == "4":
            if len(choice) > 1 and choice[1] == " ":
                params = choice.split(" ")
                vehicle_index = get_index_of_vehicle_id(vehicles_ids, int(params[1]))
                if vehicle_index != -1:
                    print_vehicle(vehicles_history[vehicle_index])
                else:
                    print("Invalid vehicle_id")
            else:
                print("Unexpected number of parameters")
            return True
        elif choice[0] == "5" and len(choice) == 1:
            return False
        else:
            print("Unexpected choice, retry")
            return True


def main(argv):
    filepath = argv[0]
    vehicles_ids, vehicles_history, links_ids, links_history = read_data(filepath)
    c = menu(vehicles_ids, vehicles_history, links_ids, links_history)
    while c:
        c = menu(vehicles_ids, vehicles_history, links_ids, links_history)


if __name__ == '__main__':
    main(sys.argv[1:])
