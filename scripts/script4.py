import sys
import numpy as np
import pandas
import matplotlib.pyplot as plt
#from cycler import cycler

def plot_link_queue(id, df, ax, x):
	y = df[":root:link_" + str(id) + ":queue:vehicle_number"]
	ax.plot(x, y, label="link_" + str(id))
	
def plot_link_travel(id, df, ax, x):
	y = df[":root:link_" + str(id) + ":travel:vehicle_number"]
	ax.plot(x, y, label="link_" + str(id))

def main(argv):
	filepath = argv[0]
	df = pandas.read_csv(filepath, sep=";")
	fig, ax = plt.subplots()
	#ax.set_prop_cycle(cycler('color', ['c', 'm', 'y', 'k']) + cycler('lw', [1, 2, 3, 4]))
	x = df["time"]
	plot_link_queue(169, df, ax, x)
	plot_link_queue(181, df, ax, x)
	plot_link_queue(8, df, ax, x)
	plot_link_queue(297, df, ax, x)
	plot_link_queue(204, df, ax, x)
	
	'''plot_link_travel(15, df, ax, x)
	plot_link_travel(132, df, ax, x)
	plot_link_travel(93, df, ax, x)
	plot_link_travel(95, df, ax, x)
	plot_link_travel(109, df, ax, x)
	plot_link_travel(123, df, ax, x)
	plot_link_travel(79, df, ax, x)
	plot_link_travel(217, df, ax, x)
	plot_link_travel(90, df, ax, x)
	plot_link_travel(245, df, ax, x)'''
	ax.legend()
	plt.ylabel("Travel Length")
	plt.xlabel("Temps")
	plt.grid()
	plt.show()

if __name__ == '__main__':
	main(sys.argv[1:])
