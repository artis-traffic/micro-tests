import sys
import numpy as np
import pandas
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error


def read_data(filepath, filepath2, path):
    csv = pandas.read_csv(filepath, sep=";", header=0)
    csv2 = pandas.read_csv(filepath2, sep=";", header=0)

    travel_time_real = []
    t_real = []
    travel_time_calc = []
    t_calc = []

    line = 0
    for _, row in csv.iterrows():
        print("Reading real line ", line)
        if 18000 <= row["time"] <= 43200:
            if row["path"] == path and row["day"] == "2019-11-13":
                travel_time_real.append(row["travel_time_[s]"])
                t_real.append(row["time"])
        line += 1
    line = 0
    for _, row in csv2.iterrows():
        print("Reading calc line ", line)
        if (row[":root:traveltime_" + path + ":TimeObs"] >= 18000 and row[
            ":root:traveltime_" + path + ":TimeObs"] <= 43200):
            travel_time_calc.append(row[":root:traveltime_" + path + ":average_travel_time"])
            t_calc.append(row[":root:traveltime_" + path + ":TimeObs"])
        line += 1

    return travel_time_real, t_real, travel_time_calc, t_calc


def print_plot(travel_time_real, t_real, travel_time_calc, t_calc, path):
    colors = ["b", "g", "r", "c"]

    fig, ax = plt.subplots()
    ax.plot(t_real, travel_time_real, color='blue', label='travel_time_real')
    ax.plot(t_calc, travel_time_calc, color='cyan', label='travel_time_calc')
    ax.legend()
    plt.ylabel("average_travel_time")
    plt.xlabel("t")
    plt.title("Path " + str(path))
    plt.grid()
    plt.show()


def main(argv):
    filepath = "./../data/A15-A115/traveltimes_data.csv"
    filepath2 = argv[0]
    path = argv[1]
    travel_time_real, t_real, travel_time_calc, t_calc = read_data(filepath, filepath2, path)
    print_plot(travel_time_real, t_real, travel_time_calc, t_calc, path)


if __name__ == '__main__':
    main(sys.argv[1:])
