import json
import math
from copy import deepcopy


class Graph:
    def __init__(self):
        self.links = []
        self.pre_links = []
        self.junctions = []
        self.pre_junctions = []
        self.stops = []
        self.generators = []
        self.ends = []
        self.nodes = []

    def add_pre_link_to_graph(self, link):
        self.check_pre_link_intersection(link)
        self.pre_links.append(link)

    def add_link_to_graph(self, link):
        self.check_link_intersection(link)
        self.links.append(link)

    def check_pre_link_intersection(self, link):
        for l in self.pre_links:
            l_in = l.in_coord
            l_out = l.out_coord
            if l_in == link.in_coord and (l.id != link.id + '_R' and link.id != l.id + '_R'):
                # print(l_in, 'in', 'in')
                self.add_pre_junction(l, link, l.in_coord, 'in', 'in')
            if l_in == link.out_coord and (l.id != link.id + '_R' and link.id != l.id + '_R'):
                # print(l_in, 'in', 'out')
                self.add_pre_junction(l, link, l.in_coord, 'in', 'out')
            if l_out == link.in_coord and (l.id != link.id + '_R' and link.id != l.id + '_R'):
                # print(l_out, 'out', 'in')
                self.add_pre_junction(l, link, l.out_coord, 'out', 'in')
            if l_out == link.out_coord and (l.id != link.id + '_R' and link.id != l.id + '_R'):
                # print(l_out, 'out', 'out')
                self.add_pre_junction(l, link, l.out_coord, 'out', 'out')

    def check_link_intersection(self, link):
        for l in self.links:
            l_in = l.in_coord
            l_out = l.out_coord
            if l_in == link.in_coord and (l.id != link.id + '_R' and link.id != l.id + '_R'):
                # print(l_in, 'in', 'in')
                self.add_junction(l, link, l.in_coord, 'in', 'in')
            if l_in == link.out_coord and (l.id != link.id + '_R' and link.id != l.id + '_R'):
                # print(l_in, 'in', 'out')
                self.add_junction(l, link, l.in_coord, 'in', 'out')
            if l_out == link.in_coord and (l.id != link.id + '_R' and link.id != l.id + '_R'):
                # print(l_out, 'out', 'in')
                self.add_junction(l, link, l.out_coord, 'out', 'in')
            if l_out == link.out_coord and (l.id != link.id + '_R' and link.id != l.id + '_R'):
                # print(l_out, 'out', 'out')
                self.add_junction(l, link, l.out_coord, 'out', 'out')

    def add_roundabout_junction(self, coords):
        # id, coords, in_ports, out_ports, type
        in_ports = []
        out_ports = []
        links_out = []
        links_in = []
        for l in self.links:
            if l.out_coord == coords[0]:
                in_ports.append(l)
                links_out.append(l)
            if l.in_coord == coords[-1]:
                out_ports.append(l)
                links_in.append(l)
        junction = Junction('junction/' + str(len(self.junctions)), coords, in_ports, out_ports, 'junction')
        self.junctions.append(junction)
        for l in links_in:
            self.links[self.links.index(l)].in_ports.append(self.junctions[self.junctions.index(junction)])
        for l in links_out:
            self.links[self.links.index(l)].out_ports.append(self.junctions[self.junctions.index(junction)])

    def add_pre_junction(self, l1, l2, l_coord, way1, way2):
        is_new = True
        in_ports_junction = []
        out_ports_junction = []
        if way1 == 'in':
            out_ports_junction.append(l1)
        if way1 == 'out':
            in_ports_junction.append(l1)
        if way2 == 'in':
            out_ports_junction.append(l2)
        if way2 == 'out':
            in_ports_junction.append(l2)
        for j in self.pre_junctions:
            if l_coord == j.coords:
                is_new = False
                for l in in_ports_junction:
                    if l not in j.in_ports:
                        j.in_ports.append(l)
                for l in out_ports_junction:
                    if l not in j.out_ports:
                        j.out_ports.append(l)

                if j not in l1.in_ports and way1 == 'in':
                    l1.in_ports.append(j)
                if j not in l1.out_ports and way1 == 'out':
                    l1.out_ports.append(j)
                if j not in l2.in_ports and way2 == 'in':
                    l2.in_ports.append(j)
                if j not in l2.out_ports and way2 == 'out':
                    l2.out_ports.append(j)

        if is_new:
            junction = Junction('junction/' + str(len(self.pre_junctions)), l_coord, in_ports_junction,
                                out_ports_junction,
                                'junction')
            self.pre_junctions.append(junction)
            if way1 == 'in':
                l1.in_ports.append(junction)
            if way1 == 'out':
                l1.out_ports.append(junction)
            if way2 == 'in':
                l2.in_ports.append(junction)
            if way2 == 'out':
                l2.out_ports.append(junction)

    def add_junction(self, l1, l2, l_coord, way1, way2):
        is_new = True
        in_ports_junction = []
        out_ports_junction = []
        if way1 == 'in':
            out_ports_junction.append(l1)
        if way1 == 'out':
            in_ports_junction.append(l1)
        if way2 == 'in':
            out_ports_junction.append(l2)
        if way2 == 'out':
            in_ports_junction.append(l2)
        for j in self.junctions:
            if l_coord == j.coords:
                is_new = False
                for l in in_ports_junction:
                    if l not in j.in_ports:
                        j.in_ports.append(l)
                for l in out_ports_junction:
                    if l not in j.out_ports:
                        j.out_ports.append(l)

                if j not in l1.in_ports and way1 == 'in':
                    l1.in_ports.append(j)
                if j not in l1.out_ports and way1 == 'out':
                    l1.out_ports.append(j)
                if j not in l2.in_ports and way2 == 'in':
                    l2.in_ports.append(j)
                if j not in l2.out_ports and way2 == 'out':
                    l2.out_ports.append(j)

        if is_new:
            junction = Junction('junction/' + str(len(self.junctions)), l_coord, in_ports_junction, out_ports_junction,
                                'junction')
            self.junctions.append(junction)
            if way1 == 'in':
                l1.in_ports.append(junction)
            if way1 == 'out':
                l1.out_ports.append(junction)
            if way2 == 'in':
                l2.in_ports.append(junction)
            if way2 == 'out':
                l2.out_ports.append(junction)

    def merge_links(self):
        for l in list(self.pre_links):
            if l in self.pre_links:
                merge_type = ''
                if len(l.out_ports) > 0:
                    self.merge_link_output_junction(l, merge_type)
                if len(l.in_ports) > 0:
                    self.merge_link_input_junction(l, merge_type)

    def merge_link_output_junction(self, l, merge_type):
        out = l.out_ports[0]
        if out.id.startswith('stop'):
            out = out.out_ports[0]
        can_merge = False
        if len(out.in_ports) == 1 and len(out.out_ports) == 1:
            can_merge = True
            merge_type = 'default'
        if (len(out.in_ports) == 2 and len(out.out_ports) == 0 and
                (not out.in_ports[0].oneway or not out.in_ports[1].oneway)):
            can_merge = True
            merge_type = '2in'
        if (len(out.out_ports) == 2 and len(out.in_ports) == 0 and
                (not out.out_ports[0].oneway or not out.out_ports[1].oneway)):
            can_merge = True
            merge_type = '2out'
        if can_merge:
            if merge_type == 'default':
                second_link = out.out_ports[0]
            if merge_type == '2in':
                if l == out.in_ports[0]:
                    second_link = out.in_ports[1]
                else:
                    second_link = out.in_ports[0]
            if merge_type == '2out':
                if l == out.out_ports[0]:
                    second_link = out.out_ports[1]
                else:
                    second_link = out.out_ports[0]

            m = ((l.oneway and second_link.oneway)
                 or (not (l.oneway or second_link.oneway)))
            if second_link in self.pre_links and m:
                print('outmerge', l.id, out.id, second_link.id)
                self.finish_merge(out, l, merge_type, second_link)

    def merge_link_input_junction(self, l, merge_type):
        inp = l.in_ports[0]
        if inp.id.startswith('stop'):
            inp = inp.out_ports[0]
        can_merge = False
        if len(inp.in_ports) == 1 and len(inp.out_ports) == 1:
            can_merge = True
            merge_type = 'default'
        if (len(inp.in_ports) == 2 and len(inp.out_ports) == 0 and
                (not inp.in_ports[0].oneway or not inp.in_ports[1].oneway)):
            can_merge = True
            merge_type = '2in'
        if (len(inp.out_ports) == 2 and len(inp.in_ports) == 0 and
                (not inp.out_ports[0].oneway or not inp.out_ports[1].oneway)):
            can_merge = True
            merge_type = '2out'
        if can_merge:
            if merge_type == 'default':
                second_link = inp.in_ports[0]
            if merge_type == '2in':
                if l == inp.in_ports[0]:
                    second_link = inp.in_ports[1]
                else:
                    second_link = inp.in_ports[0]
            if merge_type == '2out':
                if l == inp.out_ports[0]:
                    second_link = inp.out_ports[1]
                else:
                    second_link = inp.out_ports[0]

            m = ((l.oneway and second_link.oneway)
                 or (not (l.oneway or second_link.oneway)))
            if second_link in self.pre_links and m:
                print('inmerge', l.id, inp.id, second_link.id, merge_type)
                self.finish_merge(inp, l, merge_type, second_link)

    def finish_merge(self, inp, l, merge_type, second_link):
        print('1', self.pre_links[self.pre_links.index(l)].name)
        print('2', self.pre_links[self.pre_links.index(second_link)].name)
        print(self.pre_links[self.pre_links.index(l)].length, self.pre_links[self.pre_links.index(l)].coords)
        print(self.pre_links[self.pre_links.index(second_link)].length,
              self.pre_links[self.pre_links.index(second_link)].coords)
        if merge_type == 'default':
            if inp.coords == self.pre_links[self.pre_links.index(l)].out_coord:
                self.pre_links[self.pre_links.index(l)].coords.extend(
                    self.pre_links[self.pre_links.index(second_link)].coords[1:])
            else:
                s_coords = deepcopy(self.pre_links[self.pre_links.index(second_link)].coords)
                f_coords = deepcopy(self.pre_links[self.pre_links.index(l)].coords[1:])
                print(len(s_coords), len(f_coords))
                s_coords.extend(f_coords)
                print(len(s_coords))
                self.pre_links[self.pre_links.index(l)].coords = s_coords
        if merge_type == '2in':
            s_coords = deepcopy(self.pre_links[self.pre_links.index(second_link)].coords[:-1])
            s_coords.reverse()
            self.pre_links[self.pre_links.index(l)].coords.extend(
                s_coords)
        if merge_type == '2out':
            s_coords = deepcopy(self.pre_links[self.pre_links.index(second_link)].coords)
            s_coords.reverse()
            f_coords = deepcopy(self.pre_links[self.pre_links.index(l)].coords[1:])
            print(len(s_coords), len(f_coords))
            s_coords.extend(f_coords)
            print(len(s_coords))
            self.pre_links[self.pre_links.index(l)].coords = s_coords

        if l.id not in self.pre_links[self.pre_links.index(l)].merged_links_id:
            self.pre_links[self.pre_links.index(l)].merged_links_id.append(l.id)
            self.pre_links[self.pre_links.index(l)].merged_links_coords.append([l.in_coord, l.out_coord])
        if second_link.coords[0] == self.pre_links[self.pre_links.index(l)].coords[0]:
            self.pre_links[self.pre_links.index(l)].merged_links_id.insert(0, second_link.id)
            self.pre_links[self.pre_links.index(l)].merged_links_coords.insert(0, [second_link.in_coord, second_link.out_coord])
        else:
            self.pre_links[self.pre_links.index(l)].merged_links_id.append(second_link.id)
            self.pre_links[self.pre_links.index(l)].merged_links_coords.append([second_link.in_coord, second_link.out_coord])

        self.pre_links[self.pre_links.index(l)].length += \
            self.pre_links[self.pre_links.index(second_link)].length
        self.pre_links[self.pre_links.index(l)].max_speed = \
            self.pre_links[self.pre_links.index(l)].get_max_speed(self.pre_links[self.pre_links.index(l)].type)
        self.pre_junctions.remove(inp)
        self.pre_links.remove(second_link)

    def add_stop(self, coords, direction):
        links = [i for i in self.links if len(i.out_ports) > 0 and not i.id.endswith('_R')]
        for l in links:
            for coord in l.coords:
                if coord == coords:
                    d_in = math.sqrt(pow(coord[0] - l.in_coord[0], 2) + pow(coord[1] - l.in_coord[1], 2))
                    d_out = math.sqrt(pow(coord[0] - l.out_coord[0], 2) + pow(coord[1] - l.out_coord[1], 2))
                    # use_reverse = d_in < d_out
                    use_reverse = direction == 'backward'
                    if use_reverse:
                        current_link = self.links[self.get_link_index_by_id(l.id + '_R')]
                    else:
                        current_link = l
                    for j in self.junctions:
                        if current_link in j.in_ports:
                            in_ports = []
                            out_ports = []
                            in_ports.append(l)
                            out_ports.append(j)
                            stop = Stop('stop/' + str(len(self.stops)), coords, in_ports, out_ports,
                                        [x.max_speed for x in j.out_ports])
                            current_link.out_ports[0] = stop
                            j.in_ports[j.in_ports.index(current_link)] = stop
                            self.stops.append(stop)

    def add_giveway(self, coords, direction):
        links = [i for i in self.links if len(i.out_ports) > 0 and not i.id.endswith('_R')]
        for l in links:
            for coord in l.coords:
                if coord == coords:
                    d_in = math.sqrt(pow(coord[0] - l.in_coord[0], 2) + pow(coord[1] - l.in_coord[1], 2))
                    d_out = math.sqrt(pow(coord[0] - l.out_coord[0], 2) + pow(coord[1] - l.out_coord[1], 2))
                    # use_reverse = d_in < d_out
                    use_reverse = direction == 'backward'
                    if use_reverse:
                        current_link = self.links[self.get_link_index_by_id(l.id + '_R')]
                    else:
                        current_link = l
                    self.links[self.links.index(current_link)].giveway = True

    def get_link_index_by_id(self, id):
        for i, e in enumerate(self.links):
            if e.id == id:
                return i
        return -1

    def get_junction_index_by_id(self, id):
        for i, e in enumerate(self.junctions):
            if e.id == id:
                return i
        return -1

    def add_concurrent_links(self):
        for l in self.links:
            if len(l.out_ports) > 0:
                out = l.out_ports[0]
                # if out.id.startswith('stop'):
                #     j_out = out.out_ports[0]
                # else:
                #     j_out = out
                # for input in j_out.in_ports:
                #     if input.id.startswith('stop'):
                #         if input.id != out.id:
                #             l.concurrent_stops.append(input)
                #     else:
                #         if input.id != l.id:
                #             l.concurrent_links.append(input)

                if not out.id.startswith('stop'):
                    j_out = out
                    for input in j_out.in_ports:
                        if input.id.startswith('stop'):
                            if input.id != out.id:
                                l.concurrent_stops.append(input)
                        else:
                            if input.id != l.id:
                                l.concurrent_links.append(input)
        for s in self.stops:
            if len(s.out_ports) > 0:
                out = s.out_ports[0]
                for input in out.in_ports:
                    if input.id != s.id:
                        if input.id.startswith('way'):
                            s.concurrent_links.append(input)
                        else:
                            s.concurrent_stops.append(input)

    def add_generators(self, primary_params, secondary_params, tertiary_params, residential_params):
        start_links = [l for l in self.links if len(l.in_ports) == 0]

        seed = 0
        for l in start_links:
            paths = []
            if l.type == 'primary':
                params = primary_params
            if l.type == 'secondary':
                params = secondary_params
            if l.type == 'tertiary':
                params = tertiary_params
            if l.type == 'residential':
                params = residential_params
            generator = Generator('generator/' + str(len(self.generators)), len(self.generators) * 1000,
                                  params[0], params[1], params[2], params[3], seed, paths, [l], l.coords[0])
            self.generators.append(generator)
            l.in_ports.append(generator)
            seed += 1

    def add_ends(self):
        end_links = [l for l in self.links if len(l.out_ports) == 0]
        for l in end_links:
            end = End('end/' + str(len(self.ends)), [l], l.coords[-1])
            self.ends.append(end)
            l.out_ports.append(end)

    def add_downstream_links(self):
        for l in self.links:
            for out in l.out_ports:
                if out.id.startswith('stop'):
                    out = out.out_ports[0]
                junction_outs = out.out_ports
                for j in junction_outs:
                    l.downstream_links.append(j)

    def remove_small_links(self):
        for l in list(self.links):
            print(l.id, l.length)
            if l.length < (l.max_speed * l.max_speed) / 2:
                in_port = l.in_ports[0]
                out_port = l.out_ports[0]
                if in_port.id.startswith('generator'):
                    for i in range(0, len(self.generators)):
                        if self.generators[i].id == in_port.id:
                            self.generators[i].out_ports[0] = out_port
                if in_port.id.startswith('junction'):
                    for i in range(0, len(self.junctions)):
                        if self.junctions[i].id == in_port.id:
                            junction = self.junctions[i]
                            for j in range(0, len(junction.out_ports)):
                                if junction.out_ports[j].id == l.id:
                                    self.junctions[i].out_ports[j] = out_port
                if out_port.id.startswith('stop'):
                    for i in range(0, len(self.stops)):
                        if self.stops[i].id == out_port.id:
                            self.stops[i].in_ports[0] = in_port
                if out_port.id.startswith('junction'):
                    for i in range(0, len(self.junctions)):
                        if self.junctions[i].id == out_port.id:
                            junction = self.junctions[i]
                            for j in range(0, len(junction.in_ports)):
                                if junction.in_ports[j].id == l.id:
                                    self.junctions[i].in_ports[j] = in_port
                if out_port.id.startswith('end'):
                    for i in range(0, len(self.ends)):
                        if self.ends[i].id == out_port.id:
                            self.ends[i].in_ports[0] = in_port
                self.links.remove(l)

    def add_priorities(self):
        for junction in self.junctions:
            print(junction.id)
            for current_link in junction.in_ports:
                if current_link.id.startswith('way'):
                    print(current_link.id)
                    for comp_link in current_link.concurrent_links:
                        if comp_link.id.startswith('way') and current_link.id != comp_link.id:
                            print("comp ", comp_link.id)
                            u1 = current_link.coords[-1][0] - current_link.coords[0][0]
                            u2 = current_link.coords[-1][1] - current_link.coords[0][1]
                            v1 = comp_link.coords[-1][0] - comp_link.coords[0][0]
                            v2 = comp_link.coords[-1][1] - comp_link.coords[0][1]
                            print(u1, u2)
                            print(v1, v2)
                            dot = u1 * -v2 + u2 * v1
                            print('dot', dot)
                            if comp_link.giveway or current_link.giveway:
                                if comp_link.giveway:
                                    self.links[self.links.index(current_link)].priorities.append(True)
                                if current_link.giveway:
                                    self.links[self.links.index(current_link)].priorities.append(False)
                            else:
                                if dot > 0:
                                    print(comp_link.id + ' right of ' + current_link.id)
                                    self.links[self.links.index(current_link)].priorities.append(False)
                                else:
                                    print(comp_link.id + ' left of ' + current_link.id)
                                    self.links[self.links.index(current_link)].priorities.append(True)
                    print('-----------')

            for current_stop in junction.in_ports:
                if current_stop.id.startswith('stop'):
                    for comp_stop in current_stop.concurrent_stops:
                        if comp_stop.id.startswith('stop') and current_stop.id != comp_stop.id:
                            current_link = current_stop.in_ports[0]
                            comp_link = comp_stop.in_ports[0]
                            u1 = current_link.coords[-1][0] - current_link.coords[0][0]
                            u2 = current_link.coords[-1][1] - current_link.coords[0][1]
                            v1 = comp_link.coords[-1][0] - comp_link.coords[0][0]
                            v2 = comp_link.coords[-1][1] - comp_link.coords[0][1]
                            dot = u1 * -v2 + u2 * v1
                            if dot > 0:
                                print(comp_link.id + ' right of ' + current_link.id)
                                self.stops[self.stops.index(current_stop)].priorities.append(False)
                            else:
                                print(comp_link.id + ' left of ' + current_link.id)
                                self.stops[self.stops.index(current_stop)].priorities.append(True)

    def add_priorities_direction(self):
        for junction in self.junctions:
            junction_size = len(junction.in_ports)
            # priority = 1
            # self.links[self.links.index(junction.in_ports[0])].priority = priority
            print(junction.id)
            for current_link in junction.in_ports:
                if current_link.id.startswith('way'):
                    print(current_link.id)
                    subs_coord_x = current_link.coords[-1][0] - current_link.coords[0][0]
                    subs_coord_y = current_link.coords[-1][1] - current_link.coords[0][1]
                    subs_coord = [subs_coord_x, subs_coord_y]
                    print(current_link.coords[0], current_link.coords[-1])
                    print(subs_coord)
                    direction = ''
                    if abs(subs_coord[0]) - abs(subs_coord[1]) > 0:
                        axis = 'x'
                    else:
                        axis = 'y'
                    if axis == 'x':
                        if subs_coord[0] > 0:
                            direction = 'right'
                        else:
                            direction = 'left'
                    if axis == 'y':
                        if subs_coord[1] > 0:
                            direction = 'up'
                        else:
                            direction = 'down'
                    print('direction', direction)

                    priorities = ['down', 'right', 'up', 'left']
                    self.links[self.links.index(current_link)].priority = priorities.index(direction)

    def export_to_json(self, filename):
        json_str = {'level': 'micro', 'inputs': [i.asdict() for i in self.generators],
                    'outputs': [i.asdict() for i in self.ends],
                    'junctions': [i.asdict() for i in self.junctions], 'stops': [i.asdict() for i in self.stops],
                    'links': [i.asdict() for i in self.links], 'nodes': [i for i in self.nodes]}
        with open(filename, 'w') as outfile:
            json.dump(json_str, outfile, indent=4, separators=(',', ': '))
        print('Exported ' + filename + ' to JSON.')


class Link:
    def __init__(self, id, name, coords, in_coord, out_coord, type, oneway, is_roundabout, merged_links_id = [], merged_links_coords = []):
        self.id = id
        self.name = name
        self.coords = coords
        self.in_coord = in_coord
        self.out_coord = out_coord

        R = 6371e3
        self.length = 0
        for i in range(0, len(self.coords) - 1):
            coord_1 = self.coords[i]
            coord_2 = self.coords[i + 1]
            u1 = coord_1[0] * math.pi / 180
            u2 = coord_2[0] * math.pi / 180
            du = (coord_2[0] - coord_1[0]) * math.pi / 180
            da = (coord_2[1] - coord_1[1]) * math.pi / 180
            a = math.sin(du / 2) * math.sin(du / 2) + \
                math.cos(u1) * math.cos(u2) * \
                math.sin(da / 2) * math.sin(da / 2)
            c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
            d = R * c
            self.length += d
        self.length = math.floor(self.length)
        if self.length < 15:
            self.length = 15
        self.max_speed = self.get_max_speed(type)
        self.max_speed = math.floor(self.max_speed)

        self.in_ports = []
        self.out_ports = []
        self.concurrent_links = []
        self.concurrent_stops = []
        self.downstream_links = []
        self.priorities = []
        self.oneway = oneway
        self.type = type
        self.is_roundabout = is_roundabout
        self.merged_links_id = merged_links_id
        self.merged_links_coords = merged_links_coords
        self.giveway = False

    def __str__(self):
        return 'id: ' + self.id + '\nname: ' + self.name + '\ninput_coordinates: ' + str(self.in_coord) + \
               '\noutput_coordinates: ' + str(self.out_coord) + '\nlength: ' + str(self.length) + \
               '\nmax_speed: ' + str(self.max_speed) + \
               '\ninput_ports: ' + str([i.id for i in self.in_ports]) + \
               '\noutput_ports: ' + str([o.id for o in self.out_ports]) + \
               '\nconcurrent_links: ' + str([o.id for o in self.concurrent_links]) + \
               '\nconcurrent_stops: ' + str([o.id for o in self.concurrent_stops]) + \
               '\ndownstream_links: ' + str([o.id for o in self.downstream_links]) + \
               '\npriorities: ' + str([o for o in self.priorities])

    def __eq__(self, other):
        if self.id == other.id:
            return True
        else:
            return False

    def get_max_speed(self, type):
        speed = 0
        if type == 'primary':
            speed = 90
        if type == 'secondary':
            speed = 70
        if type == 'tertiary':
            speed = 50
        if type == 'residential':
            speed = 50
        speed = 18
        speed /= 3.6
        if self.length < 7.5:
            speed = -1
        elif self.length < (speed * speed / 2) + 7.5:
            speed = math.sqrt((self.length - 7.5) * 2)
        return speed

    def asdict(self):
        return {'id': self.id, 'max_speed': self.max_speed, 'length': self.length,
                'input_ports_id': [i.id for i in self.in_ports], 'output_ports_id': [i.id for i in self.out_ports],
                'concurrent_links_id': [i.id for i in self.concurrent_links],
                'concurrent_stops_id': [i.id for i in self.concurrent_stops],
                'priorities': [o for o in self.priorities],
                'downstream_links_id': [i.id for i in self.downstream_links],
                'merged_links_id': [i for i in self.merged_links_id],
                'merged_links_coords': [i for i in self.merged_links_coords],
                'coords': self.coords}

    def preprocess_dict(self):
        if self.oneway:
            return {"type": "Feature",
                    "properties": {"@id": self.id, "highway": self.type, "name": self.name, 'oneway': 'yes',
                                   'roundabout': self.is_roundabout},
                    "geometry": {"type": "LineString", "coordinates": self.coords},
                    "merged_links_id": [i for i in self.merged_links_id],
                    "merged_links_coords": [i for i in self.merged_links_coords], "id": self.id}
        else:
            return {"type": "Feature", "properties": {"@id": self.id, "highway": self.type, "name": self.name,
                                                      'roundabout': self.is_roundabout},
                    "geometry": {"type": "LineString", "coordinates": self.coords},
                    "merged_links_id": [i for i in self.merged_links_id],
                    "merged_links_coords": [i for i in self.merged_links_coords], "id": self.id,
                    }


class Junction:
    def __init__(self, id, coords, in_ports, out_ports, type):
        self.id = id
        self.coords = coords
        self.in_ports = in_ports
        self.out_ports = out_ports
        self.type = type

    def __str__(self):
        return 'id: ' + self.id + '\n' + 'type: ' + self.type + '\n' + 'coordinates: ' + str(self.coords) + '\n' + \
               'input_ports: ' + str([i.id for i in self.in_ports]) + '\n' + 'output_ports: ' + str(
            [o.id for o in self.out_ports])

    def asdict(self):
        return {'id': self.id, 'input_ports_id': [i.id for i in self.in_ports],
                'output_ports_id': [i.id for i in self.out_ports], 'coords': self.coords}


class Stop:
    def __init__(self, id, coords, in_ports, out_ports, exits_links_speed):
        self.id = id
        self.coords = coords
        self.in_ports = in_ports
        self.out_ports = out_ports
        self.exits_links_speed = exits_links_speed
        self.concurrent_links = []
        self.concurrent_stops = []
        self.priorities = []

    def __str__(self):
        return 'id: ' + self.id + '\n' + 'coordinates: ' + str(self.coords) + '\n' + \
               'input_ports: ' + str([i.id for i in self.in_ports]) + '\n' + 'output_ports: ' + str(
            [o.id for o in self.out_ports]) + '\nconcurrent_links: ' + str([o.id for o in self.concurrent_links])

    def asdict(self):
        return {'id': self.id, 'input_ports_id': [i.id for i in self.in_ports],
                'output_ports_id': [i.id for i in self.out_ports], 'exits_links_speeds': self.exits_links_speed,
                'concurrent_links_id': [i.id for i in self.concurrent_links],
                'concurrent_stops_id': [i.id for i in self.concurrent_stops],
                'priorities': self.priorities, 'coords': self.coords}


class Generator:
    def __init__(self, id, start_index, min, max, mean, stddev, seed, paths, out_ports, coords):
        self.id = id
        self.start_index = start_index
        self.min = min
        self.max = max
        self.mean = mean
        self.stddev = stddev
        self.seed = seed
        self.paths = paths
        self.out_ports = out_ports
        self.coords = coords

    def asdict(self):
        return {'id': self.id, 'start index': self.start_index, 'min': self.min, 'max': self.max, 'mean': self.mean,
                'stddev': self.stddev, 'seed': self.seed, 'paths': self.paths,
                'output_ports_id': [i.id for i in self.out_ports], 'coords': self.coords}


class End:
    def __init__(self, id, in_ports, coords):
        self.id = id
        self.in_ports = in_ports
        self.coords = coords

    def asdict(self):
        return {'id': self.id, 'input_ports_id': [i.id for i in self.in_ports], 'coords': self.coords}


def pre_add_links(filename, filename_out, graph):
    f = open(filename)
    data = json.load(f)
    for feature in data['features']:
        geometry_type = feature['geometry']['type']
        id = feature['properties']['@id']
        if 'name' in feature['properties']:
            name = feature['properties']['name']
        else:
            name = feature['properties']['highway']
        if geometry_type == 'LineString':
            type = feature['properties']['highway']
            oneway = False
            if 'oneway' in feature['properties']:
                if feature['properties']['oneway'] == 'yes':
                    oneway = True

            is_roundabout = 'junction' in feature['properties'] and feature['properties']['junction'] == 'roundabout'
            if feature['geometry']['coordinates'][0] != feature['geometry']['coordinates'][-1]:
                mid_coords = feature['geometry']['coordinates'][1:-1]
                i = 1
                start_point = 0
                split_number = 0
                for coord in mid_coords:
                    for feature2 in data['features']:
                        id2 = feature2['properties']['@id']
                        if feature2['geometry']['type'] == 'LineString' and id != id2:
                            if feature2['geometry']['coordinates'][0] != feature2['geometry']['coordinates'][-1]:
                                coords2 = feature2['geometry']['coordinates']
                                for coord2 in coords2:
                                    if coord == coord2:
                                        # print('Split ', id, i, coord, id2, start_point)
                                        graph.add_pre_link_to_graph(
                                            Link(id + '_' + str(split_number), name,
                                                 feature['geometry']['coordinates'][start_point:i + 1],
                                                 feature['geometry']['coordinates'][start_point],
                                                 feature['geometry']['coordinates'][i], type, oneway, is_roundabout, [], []))
                                        start_point = i
                                        split_number += 1
                    i += 1

                if start_point == 0:
                    graph.add_pre_link_to_graph(
                        Link(id, name, feature['geometry']['coordinates'], feature['geometry']['coordinates'][0],
                             feature['geometry']['coordinates'][-1], type, oneway, is_roundabout, [], []))
                else:
                    graph.add_pre_link_to_graph(
                        Link(id + '_' + str(split_number), name, feature['geometry']['coordinates'][start_point:],
                             feature['geometry']['coordinates'][start_point],
                             feature['geometry']['coordinates'][-1], type, oneway, is_roundabout, [], []))
    f.close()

    graph.merge_links()
    json_str = {"features": [l.preprocess_dict() for l in graph.pre_links]}
    with open(filename_out, 'w') as outfile:
        json.dump(json_str, outfile, indent=4, separators=(',', ': '))
    print('Exported ' + filename_out + ' to JSON.')


def add_links(filename, graph):
    f = open(filename)
    data = json.load(f)
    for feature in data['features']:
        geometry_type = feature['geometry']['type']
        id = feature['properties']['@id']
        if 'name' in feature['properties']:
            name = feature['properties']['name']
        else:
            name = feature['properties']['highway']
        if geometry_type == 'LineString':
            type = feature['properties']['highway']
            oneway = False
            if 'oneway' in feature['properties']:
                if feature['properties']['oneway'] == 'yes':
                    oneway = True

            is_roundabout = 'roundabout' in feature['properties'] and feature['properties']['roundabout'] == True
            if feature['geometry']['coordinates'][0] != feature['geometry']['coordinates'][-1]: #and not is_roundabout:
                graph.add_link_to_graph(
                    Link(id, name, feature['geometry']['coordinates'], feature['geometry']['coordinates'][0],
                         feature['geometry']['coordinates'][-1], type, oneway, is_roundabout,
                         feature['merged_links_id'], feature['merged_links_coords']))
                if not oneway:
                    coords = deepcopy(feature['geometry']['coordinates'])
                    coords.reverse()
                    graph.add_link_to_graph(Link(id + '_R', name + ' SI', coords,
                                                 coords[0],
                                                 coords[-1], type, oneway, is_roundabout,
                                                 feature['merged_links_id'], feature['merged_links_coords']))

    for l in graph.links:
        if l.length <= 7.5:
            graph.links[graph.links.index(l)].length = 15
            graph.links[graph.links.index(l)].max_speed = \
                graph.links[graph.links.index(l)].get_max_speed(graph.links[graph.links.index(l)].type)
    # for feature in data['features']:
    #     geometry_type = feature['geometry']['type']
    #     id = feature['properties']['@id']
    #     if 'name' in feature['properties']:
    #         name = feature['properties']['name']
    #     else:
    #         name = feature['properties']['highway']
    #     if geometry_type == 'LineString':
    #         type = feature['properties']['highway']
    #         oneway = False
    #         if 'oneway' in feature['properties']:
    #             if feature['properties']['oneway'] == 'yes':
    #                 oneway = True
    #
    #         is_roundabout = 'roundabout' in feature['properties'] and feature['properties']['roundabout'] == True
    #         if feature['geometry']['coordinates'][0] != feature['geometry']['coordinates'][-1] and is_roundabout:
    #             graph.add_roundabout_junction(feature['geometry']['coordinates'])

    f.close()


def add_stops(filename, graph):
    f2 = open(filename)
    data_stop = json.load(f2)
    for feature in data_stop['features']:
        if 'stop' in feature['properties']['highway']:
            if 'direction' in feature['properties']:
                graph.add_stop(feature['geometry']['coordinates'], feature['properties']['direction'])
            else:
                graph.add_stop(feature['geometry']['coordinates'], 'forward')
    f2.close()


def add_giveway(filename, graph):
    f2 = open(filename)
    data_gw = json.load(f2)
    for feature in data_gw['features']:
        if 'give_way' in feature['properties']['highway']:
            if 'direction' in feature['properties']:
                graph.add_giveway(feature['geometry']['coordinates'], feature['properties']['direction'])
            else:
                graph.add_giveway(feature['geometry']['coordinates'], 'forward')
    f2.close()


# Python3 implementation to find the
# shortest path in a directed
# graph from source vertex to
# the destination vertex
class Pair:
    def __init__(self, first, second):
        self.first = first
        self.second = second

    def __repr__(self):
        return 'first id: ' + str(self.first.id) + ';length: ' + str(self.second)


infi = 1000000000


# Class of the node
class Node:

    # Adjacency list that shows the
    # vertexNumber of child vertex
    # and the weight of the edge
    def __init__(self, vertexNumber, id):
        self.vertexNumber = vertexNumber
        self.id = id
        self.children = []

    # Function to add the child for
    # the given node
    def add_child(self, vNumber, length):
        p = Pair(vNumber, length)
        self.children.append(p)

    def __str__(self):
        return 'number: ' + str(self.vertexNumber) + '\nid: ' + self.id + '\nchildren: ' + \
               str([o for o in self.children])


# Function to find the distance of
# the node from the given source
# vertex to the destination vertex
def dijkstra_dist(g, s, path):
    # Stores distance of each
    # vertex from source vertex
    dist = [infi for i in range(len(g))]

    # bool array that shows
    # whether the vertex 'i'
    # is visited or not
    visited = [False for i in range(len(g))]

    for i in range(len(g)):
        path[i] = -1
    dist[s.vertexNumber] = 0
    path[s.vertexNumber] = -1
    if s == 0:
        current = s
    else:
        current = s.vertexNumber
    current = s

    # Set of vertices that has
    # a parent (one or more)
    # marked as visited
    sett = set()
    while (True):

        # Mark current as visited
        visited[current.vertexNumber] = True
        for i in range(len(g[current.vertexNumber].children)):
            v = g[current.vertexNumber].children[i].first
            if (visited[v.vertexNumber]):
                continue

            # Inserting into the
            # visited vertex
            sett.add(v)
            alt = dist[current.vertexNumber] + g[current.vertexNumber].children[i].second

            # Condition to check the distance
            # is correct and update it
            # if it is minimum from the previous
            # computed distance
            if (alt < dist[v.vertexNumber]):
                dist[v.vertexNumber] = alt
                path[v.vertexNumber] = current
        if current in sett:
            sett.remove(current)
        if (len(sett) == 0):
            break

        # The new current
        minDist = infi
        index = 0

        # Loop to update the distance
        # of the vertices of the graph
        for a in sett:
            if (dist[a.vertexNumber] < minDist):
                minDist = dist[a.vertexNumber]
                index = a
        current = index
    return dist


# Function to print the path
# from the source vertex to
# the destination vertex
def print_path(path, i, s, dest):
    if (i != s):

        # Condition to check if
        # there is no path between
        # the vertices
        if (path[i.vertexNumber] == -1):
            print("Path not found!!")
            return
        print_path(path, path[i.vertexNumber], s, dest)
        # print(path[i.vertexNumber].id + " ")
        # if i == dest:
        #    print(i.id)


def get_path_non_recursive(path, dest, s):
    path_stack = []

    i = dest
    while i != s:
        if path[i.vertexNumber] == -1:
            print("Path not found!!")
            return []
        path_stack.append(i)
        i = path[i.vertexNumber]
    path_stack.append(s)
    # print([x.id for x in path_stack])
    return path_stack


def run_parser(input, output, primary_params, secondary_params, tertiary_params, residential_params):
    graph = Graph()
    
    pre_add_links(input + '.geojson', input + '_prepro.geojson', graph)

    add_links(input + '_prepro.geojson', graph)
    add_stops(input + '.geojson', graph)
    add_giveway(input + '.geojson', graph)
    graph.add_concurrent_links()
    graph.add_downstream_links()
    graph.add_generators(primary_params, secondary_params, tertiary_params, residential_params)
    graph.add_ends()
    graph.add_priorities()

    add_paths(graph)

    print(str(len(graph.generators)) + ' generators')
    print(str(len(graph.ends)) + ' ends')
    print(str(len(graph.links)) + ' links')
    print(str(len(graph.junctions)) + ' junctions')
    print(str(len(graph.stops)) + ' stops')
    graph.export_to_json(output + '.json')


def add_paths(graph):
    v = []
    # Loop to create the nodes
    i = 0
    for generator in graph.generators:
        a = Node(i, generator.id)
        v.append(a)
        i += 1
    for junction in graph.junctions:
        a = Node(i, junction.id)
        v.append(a)
        i += 1
    for end in graph.ends:
        a = Node(i, end.id)
        v.append(a)
        i += 1
    j = 0
    for generator in graph.generators:
        link = generator.out_ports[0]
        if link.id.startswith('way'):
            junction = link.out_ports[0]
            for i, e in enumerate(v):
                if e.id == junction.id:
                    if link.id.startswith('way'):
                        v[j].add_child(v[i], link.length)
        else:
            for i, e in enumerate(v):
                if e.id == link.id:
                    v[j].add_child(v[i], 0)
        j += 1
    generator_max_idx = j
    for junction in graph.junctions:
        for link in junction.out_ports:
            if link.id.startswith('way'):
                for next in link.out_ports:
                    if next.id.startswith('stop'):
                        next = next.out_ports[0]
                    for i, e in enumerate(v):
                        if e.id == next.id:
                            # if next.id.startswith('way'):
                            v[j].add_child(v[i], link.length)
                        # else:
                        #     v[j].add_child(v[i], 0)
            else:
                for i, e in enumerate(v):
                    if e.id == link.id:
                        v[j].add_child(v[i], 0)

        j += 1
    junction_max_index = j
    path = [0 for i in range(len(v))]
    for generator_idx in range(0, generator_max_idx):
        s = v[generator_idx]
        dist = dijkstra_dist(v, s, path)

        paths_i = []
        num_paths_i = []
        # Loop to print the distance of
        # every node from source vertex
        for i in range(junction_max_index, len(dist)):
            if (dist[i] == infi):
                # print("{} and {} are not connected".format(v[i].id, s.id))
                continue
            # print("Distance of {} vertex from source vertex {} is: {}".format(
            #    v[i].id, s.id, dist[i]))
            # print_path(path, v[i], s, v[i])
            path_stack = get_path_non_recursive(path, v[i], s)
            # path_stack.reverse()
            paths_i.append([])
            while len(path_stack) > 0:
                element = path_stack.pop()
                paths_i[len(paths_i) - 1].append(element)

        for x in range(0, len(paths_i)):
            num_paths_i.append([])
            for i in range(0, len(paths_i[x])):
                n = paths_i[x][i]
                node_id = n.id
                if not node_id.startswith('generator') and not node_id.startswith('end'):
                    junction = Junction
                    for j in graph.junctions:
                        if j.id == node_id:
                            junction = j
                    next_id = paths_i[x][i + 1].id

                    best_length = -1
                    for k in range(0, len(junction.out_ports)):
                        j_out = junction.out_ports[k]
                        # if j_out.id.startswith('end'):
                        #     num_paths_i[x].append(k)
                        # else:
                        #     out = j_out.out_ports[0]
                        #     if out.id.startswith('stop'):
                        #         out = out.out_ports[0]
                        #     if next_id == out.id:
                        #         num_paths_i[x].append(k)
                        l_out = j_out.out_ports[0]
                        if l_out.id.startswith('stop'):
                            l_out = l_out.out_ports[0]
                        if l_out.id == next_id:
                            if best_length == -1:
                                num_paths_i[x].append(k)
                            if j_out.length < best_length:
                                num_paths_i[x][-1] = k
                            best_length = j_out.length
        # print(num_paths_i)
        graph.generators[generator_idx].paths = num_paths_i


def main():
    input = 'calais'

    for i in range(1, 7):
        output = 'test_' + input + '_set' + str(i)

        primary_min = 5
        primary_max = 30
        primary_mean = 10
        primary_stddev = 10

        secondary_min = 10
        secondary_max = 60
        secondary_mean = 15
        secondary_stddev = 10

        tertiary_min = 20
        tertiary_max = 75
        tertiary_mean = 45
        tertiary_stddev = 10

        residential_min = 30
        residential_max = 90
        residential_mean = 60
        residential_stddev = 10

        primary_min = 2
        primary_max = 15
        primary_mean = 5
        primary_stddev = 5

        secondary_min = 5
        secondary_max = 30
        secondary_mean = 7
        secondary_stddev = 5

        tertiary_min = 10
        tertiary_max = 37
        tertiary_mean = 22
        tertiary_stddev = 5

        residential_min = 15
        residential_max = 45
        residential_mean = 30
        residential_stddev = 5
        # primary_min = 10
        # primary_max = 60
        # primary_mean = 20
        # primary_stddev = 20
        #
        # secondary_min = 20
        # secondary_max = 120
        # secondary_mean = 30
        # secondary_stddev = 20
        #
        # tertiary_min = 40
        # tertiary_max = 150
        # tertiary_mean = 90
        # tertiary_stddev = 20
        #
        # residential_min = 60
        # residential_max = 180
        # residential_mean = 120
        # residential_stddev = 20

        primary_params = [primary_min * i, primary_max * i, primary_mean * i, primary_stddev * i]
        secondary_params = [secondary_min * i, secondary_max * i, secondary_mean * i, secondary_stddev * i]
        tertiary_params = [tertiary_min * i, tertiary_max * i, tertiary_mean * i, tertiary_stddev * i]
        residential_params = [residential_min * i, residential_max * i, residential_mean * i, residential_stddev * i]

        run_parser(input, output, primary_params, secondary_params, tertiary_params, residential_params)

if __name__ == '__main__':
    main()
