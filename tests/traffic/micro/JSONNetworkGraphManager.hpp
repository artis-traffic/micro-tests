/**
 * @file tests/JSONNetworkMicroGraphManager.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_TESTS_JSON_NETWORK_MICRO_GRAPH_MANAGER_HPP
#define ARTIS_TRAFFIC_TESTS_JSON_NETWORK_MICRO_GRAPH_MANAGER_HPP

#include <artis-traffic/micro/utils/Generator.hpp>
#include <artis-traffic/micro/utils/JSONNetworkGraphManager.hpp>
#include <artis-traffic/micro/utils/JSONNetworkView.hpp>

#include <artis-traffic/micro/core/link-state-machine/LinkTypes.hpp>
#include <artis-traffic/micro/core/stop-state-machine/StopTypes.hpp>

#include <artis-traffic/micro/core/Link.hpp>
#include <artis-traffic/micro/core/Stop.hpp>

#include <artis-traffic/micro/utils/VehicleFactory.hpp>

template<class Vehicle, class VehicleEntry, class VehicleState,
  typename Time, typename Parameters = artis::common::NoParameters>
using JSONNetworkGraphManager = artis::traffic::micro::utils::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState,
  artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters>,
  artis::traffic::micro::core::LinkParameters,
  artis::traffic::micro::core::Stop<artis::traffic::micro::core::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>,
    artis::traffic::micro::core::StopParameters>,
  artis::traffic::micro::core::StopParameters,
  artis::traffic::micro::utils::Generator<VehicleFactory>,
  artis::traffic::micro::utils::GeneratorParameters,
  Time, Parameters>;

template<class Vehicle, class VehicleEntry, class VehicleState, class Network,
  typename Time, typename Parameters = artis::common::NoParameters>
using JSONLinkView = artis::traffic::micro::utils::JSONLinkView<Vehicle, VehicleEntry, VehicleState,
  artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters>,
  artis::traffic::micro::core::LinkParameters,
  artis::traffic::micro::core::Stop<artis::traffic::micro::core::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>,
    artis::traffic::micro::core::StopParameters>,
  artis::traffic::micro::core::StopParameters,
  artis::traffic::micro::utils::Generator<VehicleFactory>,
  artis::traffic::micro::utils::GeneratorParameters,
  Network, Time, Parameters>;

template<class Vehicle, class VehicleEntry, class VehicleState, class Network,
  typename Time, typename Parameters = artis::common::NoParameters>
using JSONStopView = artis::traffic::micro::utils::JSONStopView<Vehicle, VehicleEntry, VehicleState,
  artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters>,
  artis::traffic::micro::core::LinkParameters,
  artis::traffic::micro::core::Stop<artis::traffic::micro::core::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>,
    artis::traffic::micro::core::StopParameters>,
  artis::traffic::micro::core::StopParameters,
  artis::traffic::micro::utils::Generator<VehicleFactory>,
  artis::traffic::micro::utils::GeneratorParameters,
  Network, Time, Parameters>;

template<class Vehicle, class VehicleEntry, class VehicleState, class Network,
  typename Time, typename Parameters = artis::common::NoParameters>
using JSONJunctionView = artis::traffic::micro::utils::JSONJunctionView<Vehicle, VehicleEntry, VehicleState,
  artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters>,
  artis::traffic::micro::core::LinkParameters,
  artis::traffic::micro::core::Stop<artis::traffic::micro::core::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>,
    artis::traffic::micro::core::StopParameters>,
  artis::traffic::micro::core::StopParameters,
  artis::traffic::micro::utils::Generator<VehicleFactory>,
  artis::traffic::micro::utils::GeneratorParameters,
  Network, Time, Parameters>;

template<class Vehicle, class VehicleEntry, class VehicleState, class Network,
  typename Time, typename Parameters = artis::common::NoParameters>
using JSONGeneratorView = artis::traffic::micro::utils::JSONGeneratorView<Vehicle, VehicleEntry, VehicleState,
  artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters>,
  artis::traffic::micro::core::LinkParameters,
  artis::traffic::micro::core::Stop<artis::traffic::micro::core::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>,
    artis::traffic::micro::core::StopParameters>,
  artis::traffic::micro::core::StopParameters,
  artis::traffic::micro::utils::Generator<VehicleFactory>,
  artis::traffic::micro::utils::GeneratorParameters,
  Network, Time, Parameters>;

#endif //ARTIS_TRAFFIC_TESTS_JSON_NETWORK_MICRO_GRAPH_MANAGER_HPP
