/**
 * @file tests/crossroads.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crossroads_def.hpp"

#include <chrono>
#include <iostream>

using namespace std::chrono;

/*************************************************
 * Tests
 *************************************************/

/*TEST_CASE("four roads", "crossroads")
{
  CrossroadsParameters parameters = {{0, 5, 30., 8., 78364},
                                     {0, 5, 30., 8., 87364},
                                     {0, 5, 30., 8., 2938464},
                                     {0, 5, 30., 8., 12384},
                                     {180., 540., 0.5, 0},
                                     {180., 540., 0.5, 180.},
                                     {180., 540., 0.5, 360},
                                     {180., 540., 0.5, 540.}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
  artis::common::RootCoordinator<
      artis::common::DoubleTime, artis::pdevs::Coordinator<
          artis::common::DoubleTime,
          CrossroadsGraphManager,
          CrossroadsParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  REQUIRE(true);
}

TEST_CASE("node + switch", "crossroads")
{
  NodeSwitchParameters parameters = {{0, 5, 30., 8., 78364},
                                     {{180., 540., 0.5, 0},
                                      {{"link_1", "link_2", "link_3"}, 63525}}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
  artis::common::RootCoordinator<
      artis::common::DoubleTime, artis::pdevs::Coordinator<
          artis::common::DoubleTime,
          NodeSwitchGraphManager,
          NodeSwitchParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  rc.attachView("Counter", new NodeSwitchView());
  rc.switch_to_timed_observer(1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  const NodeSwitchView::Values &counter_1_values = rc.observer().view("Counter").get(
      "Counter_1:counter");
  const NodeSwitchView::Values &counter_2_values = rc.observer().view("Counter").get(
      "Counter_2:counter");
  const NodeSwitchView::Values &counter_3_values = rc.observer().view("Counter").get(
      "Counter_3:counter");
  const NodeSwitchView::Values &generator_values = rc.observer().view("Counter").get(
      "Generator:counter");
  unsigned int c_n_1, c_n_2, c_n_3, g_n;

  counter_1_values.back().second(c_n_1);
  counter_2_values.back().second(c_n_2);
  counter_3_values.back().second(c_n_3);
  generator_values.back().second(g_n);

  REQUIRE(g_n == c_n_1 + c_n_2 + c_n_3);
}*/