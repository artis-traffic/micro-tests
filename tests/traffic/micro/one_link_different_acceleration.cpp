/**
 * @file tests/one_link_different_acceleration.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/Output.hpp>
#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

#include <artis-traffic/micro/core/Link.hpp>
#include <artis-traffic/micro/core/Node.hpp>
#include <artis-traffic/micro/core/Disruptor.hpp>
#include <artis-traffic/micro/utils/End.hpp>

#include <artis-traffic/micro/core/vehicle-dynamics/TimeGippsVehicleDynamics.hpp>
#include <artis-traffic/micro/core/vehicle-dynamics/EventGippsVehicleDynamics.hpp>

#include "one_link_generator.hpp"
#include "MicroGraphManager.hpp"

#include <chrono>

#define BOOST_TEST_MODULE Link_OneLinkDifferentAcceleration_Tests

#include <boost/test/unit_test.hpp>

using namespace std::chrono;

/*************************************************
 * Tests
 *************************************************/

struct OnlyOneLinkParameters {
  artis::traffic::micro::core::LinkParameters link_parameters;
  GeneratorParameters generator_parameters;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class OneLinkGraphManager :
  public MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, OnlyOneLinkParameters> {
public:
  enum submodels {
    GENERATOR, LINK, END
  };

  OneLinkGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                      const OnlyOneLinkParameters &parameters,
                      const artis::common::NoParameters &graph_parameters)
    :
    MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, OnlyOneLinkParameters>(
      coordinator, parameters, graph_parameters),
    _generator("generator", parameters.generator_parameters),
    _link("link", parameters.link_parameters),
    _end("end", artis::common::NoParameters()) {
    this->add_child(GENERATOR, &_generator);
    this->add_child(LINK, &_link);
    this->add_child(END, &_end);

      this->out({&_generator, Generator::outputs::OUT})
              >> this->in({&_link, artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::inputs::IN});

    this->connect_link_to_end(_link, _end);
  }

  ~OneLinkGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime, Generator, GeneratorParameters> _generator;
    artis::pdevs::Simulator<artis::common::DoubleTime,
            artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
            artis::traffic::micro::core::LinkParameters> _link;
  artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::utils::End> _end;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class OneLinkView : public artis::traffic::core::View {
public:
  OneLinkView() {
//      selector("Link:vehicle_number",
//               {OneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
//                artis::traffic::micro::core::Link<Vehicle, VehicleEntry, VehicleState>::vars::VEHICLE_NUMBER});
      selector("Link:vehicle_positions",
               {OneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
                artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
      selector("Link:vehicle_indexes",
               {OneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
                artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
      selector("Link:vehicle_speed",
               {OneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
                artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
//        selector("Link:vehicle_state",
//                 {OneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
//                  artis::traffic::micro::core::Link<Vehicle, VehicleEntry, VehicleState>::vars::VEHICLE_STATE});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
void run_simulation(OnlyOneLinkParameters &parameters) {

  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      OneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>,
      OnlyOneLinkParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Link", new OneLinkView());
//  double step = 0.1;
//  rc.switch_to_timed_observer(step);
//
//  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

//  steady_clock::time_point t2 = steady_clock::now();
//
//  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);
//
//  std::cout << "Duration: " << time_span.count() << std::endl;
//
//  artis::observer::Output<artis::common::DoubleTime,
//    artis::observer::TimedIterator<artis::common::DoubleTime>>
//    output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), step});
}

// two vehicles - the second have an acceleration greater then the first
BOOST_AUTO_TEST_CASE(TestCase_Different1)
{
  OnlyOneLinkParameters parameters = {{10, 1000, 1, 0, 0, {}},
                                      {{
                                         {1, 4.5, 3, 0, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::STOPPED}},
                                         {2, 4.5, 3, 0, 10, 1.5, 1.5, 1, {}, {artis::traffic::micro::core::VehicleData::State::STOPPED}}
                                       },
                                           {0, 4}}
  };

  run_simulation<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>(parameters);

  BOOST_CHECK(true);
}

//two vehicles - the second have an acceleration greater then the first - second case
BOOST_AUTO_TEST_CASE(TestCase_Different2)
{
  OnlyOneLinkParameters parameters = {{10, 1000, 1, 0, 0, {}},
                                      {{
                                         {1, 4.5, 3, 0, 10, 0.5, 0.5, 1, {}, {artis::traffic::micro::core::VehicleData::State::STOPPED}},
                                         {2, 4.5, 3, 0, 10, 2, 2, 1, {}, {artis::traffic::micro::core::VehicleData::State::STOPPED}}
                                       },
                                           {0, 7}}
  };

  run_simulation<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>(parameters);

  BOOST_CHECK(true);
}

//two vehicles - the second have an acceleration greater then the first and stop
BOOST_AUTO_TEST_CASE(TestCase_Different3)
{
  OnlyOneLinkParameters parameters = {{10, 1000, 1, 0, 0, {}},
                                      {{
                                         {1, 4.5, 3, 0, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::STOPPED}},
                                         {2, 4.5, 3, 0, 10, 1.5, 1.5, 1, {}, {artis::traffic::micro::core::VehicleData::State::STOPPED}}
                                       },
                                           {0, 4}}
  };

  run_simulation<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>(parameters);

  BOOST_CHECK(true);
}
