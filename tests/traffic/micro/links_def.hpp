/**
 * @file tests/links_def.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_TESTS_LINKS_DEF_HPP
#define ARTIS_TRAFFIC_TESTS_LINKS_DEF_HPP

#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

#include <artis-traffic/micro/core/Node.hpp>
#include <artis-traffic/micro/core/Junction.hpp>
#include <artis-traffic/micro/core/Disruptor.hpp>

#include <artis-traffic/micro/utils/End.hpp>
#include <artis-traffic/micro/utils/Generator.hpp>

#include <artis-traffic/micro/core/vehicle-dynamics/EventGippsVehicleDynamics.hpp>
#include <artis-traffic/micro/core/vehicle-dynamics/TimeGippsVehicleDynamics.hpp>

#include "MicroGraphManager.hpp"

/************************************************************
 *  Link (L1) -> Link (L2) -> End (end)
 ************************************************************/

struct TwoLinksParameters {
  artis::traffic::micro::utils::GeneratorParameters generator_parameters;
  artis::traffic::micro::core::LinkParameters link_1_parameters;
  artis::traffic::micro::core::LinkParameters link_2_parameters;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class TwoLinksGraphManager :
  public MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, TwoLinksParameters> {
public:
  enum sub_models {
    GENERATOR, LINK_1, LINK_2, END
  };

  TwoLinksGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                       const TwoLinksParameters &parameters,
                       const artis::common::NoParameters &graph_parameters)
    :
    MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, TwoLinksParameters>(
      coordinator, parameters, graph_parameters),
    _generator("generator", parameters.generator_parameters),
    _link_1("L1", parameters.link_1_parameters),
    _link_2("L2", parameters.link_2_parameters),
    _end("end", artis::common::NoParameters()) {
    this->add_child(GENERATOR, &_generator);
    this->add_child(LINK_1, &_link_1);
    this->add_child(LINK_2, &_link_2);
    this->add_child(END, &_end);

    this->connect_generator_to_link(_generator, _link_1);
    this->connect_link_to_link(_link_1, _link_2);
    this->connect_link_to_end(_link_2, _end);
  }

  ~TwoLinksGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::Generator<VehicleFactory>,
    artis::traffic::micro::utils::GeneratorParameters> _generator;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::End> _end;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class Link1View : public artis::traffic::core::View {
public:
  Link1View() {
    selector("Link_1:vehicle_number",
             {TwoLinksGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_1:vehicle_positions",
             {TwoLinksGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class Link2View : public artis::traffic::core::View {
public:
  Link2View() {
    selector("Link_2:vehicle_number",
             {TwoLinksGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_2:vehicle_positions",
             {TwoLinksGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
  }
};

/************************************************************
 *  Link (L1) -> Node (node) -> Link (L2) -> End (end)
 ************************************************************/

struct TwoLinksWithNodeParameters {
  artis::traffic::micro::utils::GeneratorParameters generator_parameters;
  artis::traffic::micro::core::LinkParameters link_1_parameters;
  artis::traffic::micro::core::LinkParameters link_2_parameters;
  artis::traffic::micro::core::NodeParameters node_parameters;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class TwoLinksWithNodeGraphManager :
  public MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, TwoLinksWithNodeParameters> {
public:
  enum sub_models {
    GENERATOR, LINK_1, LINK_2, NODE, END
  };

  TwoLinksWithNodeGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                               const TwoLinksWithNodeParameters &parameters,
                               const artis::common::NoParameters &graph_parameters)
    :
    MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, TwoLinksWithNodeParameters>(
      coordinator, parameters, graph_parameters),
    _generator("generator", parameters.generator_parameters),
    _link_1("L1", parameters.link_1_parameters),
    _link_2("L2", parameters.link_2_parameters),
    _node("node", parameters.node_parameters),
    _end("end", artis::common::NoParameters()) {
    this->add_child(GENERATOR, &_generator);
    this->add_child(LINK_1, &_link_1);
    this->add_child(LINK_2, &_link_2);
    this->add_child(NODE, &_node);
    this->add_child(END, &_end);

    this->connect_generator_to_link(_generator, _link_1);
    this->connect_upstream_link_to_node(_link_1, _node, 0);
    this->connect_downstream_link_to_node(_link_2, _node, 0, 0);
    this->connect_link_to_end(_link_2, _end);
  }

  ~TwoLinksWithNodeGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::Generator<VehicleFactory>,
    artis::traffic::micro::utils::GeneratorParameters> _generator;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Node<Vehicle>,
    artis::traffic::micro::core::NodeParameters> _node;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::End> _end;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class TwoLink1WithNodeView : public artis::traffic::core::View {
public:
  TwoLink1WithNodeView() {
    selector("Link_1:vehicle_number",
             {TwoLinksWithNodeGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_1:vehicle_positions",
             {TwoLinksWithNodeGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_1:vehicle_speed",
             {TwoLinksWithNodeGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_1:vehicle_indexes",
             {TwoLinksWithNodeGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class TwoLink2WithNodeView : public artis::traffic::core::View {
public:
  TwoLink2WithNodeView() {
    selector("Link_2:vehicle_number",
             {TwoLinksWithNodeGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_2:vehicle_positions",
             {TwoLinksWithNodeGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_2:vehicle_speed",
             {TwoLinksWithNodeGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_2:vehicle_indexes",
             {TwoLinksWithNodeGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

/************************************************************
 *  Generator -> Link -> Node
 ************************************************************/

struct OnlyOneLinkParameters {
  artis::traffic::micro::utils::GeneratorParameters generator_parameters;
  artis::traffic::micro::core::LinkParameters link_parameters;
  artis::traffic::micro::core::NodeParameters node_parameters;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class OnlyOneLinkGraphManager :
  public MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, OnlyOneLinkParameters> {
public:
  enum sub_models {
    GENERATOR, LINK, NODE
  };

  OnlyOneLinkGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                          const OnlyOneLinkParameters &parameters,
                          const artis::common::NoParameters &graph_parameters)
    :
    MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, OnlyOneLinkParameters>(
      coordinator, parameters, graph_parameters),
    _generator("generator", parameters.generator_parameters),
    _link("link", parameters.link_parameters),
    _node("node", parameters.node_parameters) {
    this->add_child(GENERATOR, &_generator);
    this->add_child(LINK, &_link);
    this->add_child(NODE, &_node);

    this->connect_generator_to_link(_generator, _link);
    this->connect_upstream_link_to_node(_link, _node, 0);
  }

  ~OnlyOneLinkGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::Generator<VehicleFactory>, artis::traffic::micro::utils::GeneratorParameters> _generator;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>,
      artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters> _link;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Node<Vehicle>, artis::traffic::micro::core::NodeParameters> _node;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class OnlyOneView : public artis::traffic::core::View {
public:
  OnlyOneView() {
    selector("Link:vehicle_number",
             {OnlyOneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link:vehicle_positions",
             {OnlyOneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link:vehicle_speed",
             {OnlyOneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link:vehicle_state",
             {OnlyOneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_STATES});
    selector("Link:vehicle_indexes",
             {OnlyOneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

/************************************************************
 *  Generator -> Link -> Node
 ************************************************************/

struct OnlyOneLinkDisturbanceParameters {
  artis::traffic::micro::utils::GeneratorParameters generator_parameters;
  artis::traffic::micro::core::LinkParameters link_parameters;
  artis::traffic::micro::core::NodeParameters node_parameters;
  artis::traffic::micro::core::DisturbanceParameters disturbances_parameters;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class OnlyOneLinkDisturbanceGraphManager :
  public MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, OnlyOneLinkDisturbanceParameters> {
public:
  enum sub_models {
    GENERATOR, LINK, NODE, DISRUPTOR
  };

  OnlyOneLinkDisturbanceGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                                     const OnlyOneLinkDisturbanceParameters &parameters,
                                     const artis::common::NoParameters &graph_parameters)
    :
    MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, OnlyOneLinkDisturbanceParameters>(
      coordinator, parameters, graph_parameters),
    _generator("generator", parameters.generator_parameters),
    _link("link", parameters.link_parameters),
    _node("node", parameters.node_parameters),
    _disruptor("disruptor", parameters.disturbances_parameters) {
    this->add_child(GENERATOR, &_generator);
    this->add_child(LINK, &_link);
    this->add_child(NODE, &_node);
    this->add_child(DISRUPTOR, &_disruptor);

    this->connect_generator_to_link(_generator, _link);
    this->connect_upstream_link_to_node(_link, _node, 0);
    this->connect_disruptor_to_link(_disruptor, _link);
  }

  ~OnlyOneLinkDisturbanceGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::utils::Generator<VehicleFactory>, artis::traffic::micro::utils::GeneratorParameters> _generator;
  artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters> _link;
  artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::core::Node<Vehicle>, artis::traffic::micro::core::NodeParameters> _node;
  artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::core::Disruptor, artis::traffic::micro::core::DisturbanceParameters> _disruptor;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class OnlyOneDisturbanceView : public artis::traffic::core::View {
public:
  OnlyOneDisturbanceView() {
    selector("Link:vehicle_number",
             {OnlyOneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link:vehicle_positions",
             {OnlyOneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link:vehicle_speed",
             {OnlyOneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link:vehicle_state",
             {OnlyOneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_STATES});
    selector("Link:vehicle_indexes",
             {OnlyOneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

/************************************************************
 *  Link (L1) -> Junction -> Link (L2)
 *                Stop
 *                 ^
 *                 |
 *             Link (L3)
 ************************************************************/

struct ThreeLinksWithStopParameters {
  artis::traffic::micro::utils::GeneratorParameters generator1_parameters;
  artis::traffic::micro::utils::GeneratorParameters generator2_parameters;
  artis::traffic::micro::core::LinkParameters link_1_parameters;
  artis::traffic::micro::core::LinkParameters link_2_parameters;
  artis::traffic::micro::core::LinkParameters link_3_parameters;
  artis::traffic::micro::core::StopParameters stop_parameters;
  artis::traffic::micro::core::JunctionParameters junction_parameters;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class ThreeLinksWithStopGraphManager :
  public MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, ThreeLinksWithStopParameters> {
public:
  enum sub_models {
    GENERATOR_1, GENERATOR_2, LINK_1, LINK_2, LINK_3, STOP, JUNCTION, END
  };

  ThreeLinksWithStopGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                                 const ThreeLinksWithStopParameters &parameters,
                                 const artis::common::NoParameters &graph_parameters)
    :
    MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, ThreeLinksWithStopParameters>(
      coordinator, parameters, graph_parameters),
    _generator1("generator1", parameters.generator1_parameters),
    _generator2("generator2", parameters.generator2_parameters),
    _link_1("L1", parameters.link_1_parameters),
    _link_2("L2", parameters.link_2_parameters),
    _link_3("L3", parameters.link_3_parameters),
    _stop("stop", parameters.stop_parameters),
    _junction("junction", parameters.junction_parameters),
    _end("end", artis::common::NoParameters()) {
    this->add_child(GENERATOR_1, &_generator1);
    this->add_child(GENERATOR_2, &_generator2);
    this->add_child(LINK_1, &_link_1);
    this->add_child(LINK_2, &_link_2);
    this->add_child(LINK_3, &_link_3);
    this->add_child(STOP, &_stop);
    this->add_child(JUNCTION, &_junction);
    this->add_child(END, &_end);

    this->connect_generator_to_link(_generator1, _link_1);
    this->connect_generator_to_link(_generator2, _link_3);
    this->connect_upstream_link_to_junction(_link_1, _junction, 0);
    this->connect_downstream_link_to_junction(_link_2, _junction, 0, 0);
    this->connect_upstream_link_to_stop(_link_3, _stop, 0, 1);
    this->connect_stop_to_junction(_stop, _junction, 1);
    this->connect_concurrent_link_to_stop(_link_1, _stop);
    this->connect_link_to_end(_link_2, _end);
  }

  ~ThreeLinksWithStopGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::Generator<VehicleFactory>,
    artis::traffic::micro::utils::GeneratorParameters> _generator1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::Generator<VehicleFactory>,
    artis::traffic::micro::utils::GeneratorParameters> _generator2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_3;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Stop<artis::traffic::micro::core::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>, artis::traffic::micro::core::StopParameters>,
    artis::traffic::micro::core::StopParameters> _stop;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Junction<Vehicle>,
    artis::traffic::micro::core::JunctionParameters> _junction;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::End> _end;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class ThreeLink1WithStopView : public artis::traffic::core::View {
public:
  ThreeLink1WithStopView() {
    selector("Generator_1:counter",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::GENERATOR_1,
              artis::traffic::micro::utils::Generator<VehicleFactory>::vars::COUNTER});
    selector("Generator_2:counter",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::GENERATOR_2,
              artis::traffic::micro::utils::Generator<VehicleFactory>::vars::COUNTER});
    selector("Link_1:vehicle_number",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_1:vehicle_positions",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_1:vehicle_speed",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_1:vehicle_indexes",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_2:vehicle_number",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_2:vehicle_positions",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_2:vehicle_speed",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_2:vehicle_indexes",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_3:vehicle_number",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_3:vehicle_positions",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_3:vehicle_speed",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_3:vehicle_indexes",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Stop:vehicle_index",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::STOP,
              artis::traffic::micro::core::Stop<artis::traffic::micro::core::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>, artis::traffic::micro::core::StopParameters>::vars::VEHICLE_INDEX});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class ThreeLink2WithStopView : public artis::traffic::core::View {
public:
  ThreeLink2WithStopView() {
    selector("Link_2:vehicle_number",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_2:vehicle_positions",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_2:vehicle_speed",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_2:vehicle_indexes",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class ThreeLink3WithStopView : public artis::traffic::core::View {
public:
  ThreeLink3WithStopView() {
    selector("Link_3:vehicle_number",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_3:vehicle_positions",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_3:vehicle_speed",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_3:vehicle_indexes",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

/************************************************************
 *  Link (L1) -> Stop2 -> Junction -> Link (L2)
 *                      Stop
 *                       ^
 *                       |
 *                    Link (L3)
 ************************************************************/

struct ThreeLinksWith2StopsParameters {
  artis::traffic::micro::utils::GeneratorParameters generator1_parameters;
  artis::traffic::micro::utils::GeneratorParameters generator2_parameters;
  artis::traffic::micro::core::LinkParameters link_1_parameters;
  artis::traffic::micro::core::LinkParameters link_2_parameters;
  artis::traffic::micro::core::LinkParameters link_3_parameters;
  artis::traffic::micro::core::StopParameters stop_parameters;
  artis::traffic::micro::core::StopParameters stop_2_parameters;
  artis::traffic::micro::core::JunctionParameters junction_parameters;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class ThreeLinksWith2StopsGraphManager :
  public MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, ThreeLinksWith2StopsParameters> {
public:
  enum sub_models {
    GENERATOR_1, GENERATOR_2, LINK_1, LINK_2, LINK_3, STOP, STOP_2, JUNCTION, END
  };

  ThreeLinksWith2StopsGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                                   const ThreeLinksWith2StopsParameters &parameters,
                                   const artis::common::NoParameters &graph_parameters)
    :
    MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, ThreeLinksWith2StopsParameters>(
      coordinator, parameters, graph_parameters),
    _generator1("generator1", parameters.generator1_parameters),
    _generator2("generator2", parameters.generator2_parameters),
    _link_1("L1", parameters.link_1_parameters),
    _link_2("L2", parameters.link_2_parameters),
    _link_3("L3", parameters.link_3_parameters),
    _stop("stop", parameters.stop_parameters),
    _stop_2("stop2", parameters.stop_2_parameters),
    _junction("junction", parameters.junction_parameters),
    _end("end", artis::common::NoParameters()) {
    this->add_child(GENERATOR_1, &_generator1);
    this->add_child(GENERATOR_2, &_generator2);
    this->add_child(LINK_1, &_link_1);
    this->add_child(LINK_2, &_link_2);
    this->add_child(LINK_3, &_link_3);
    this->add_child(STOP, &_stop);
    this->add_child(STOP_2, &_stop_2);
    this->add_child(JUNCTION, &_junction);
    this->add_child(END, &_end);

    this->connect_generator_to_link(_generator1, _link_1);
    this->connect_generator_to_link(_generator2, _link_3);
    this->connect_upstream_link_to_stop(_link_1, _stop_2, 0, 1);
    this->connect_stop_to_junction(_stop_2, _junction, 0);
    this->connect_downstream_link_to_junction(_link_2, _junction, 0, 0);
    this->connect_upstream_link_to_stop(_link_3, _stop, 0, 1);
    this->connect_stop_to_junction(_stop, _junction, 1);
    this->connect_concurrent_stops(_stop, _stop_2, 0, 0);
    this->connect_link_to_end(_link_2, _end);
  }

  ~ThreeLinksWith2StopsGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::Generator<VehicleFactory>,
    artis::traffic::micro::utils::GeneratorParameters> _generator1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::Generator<VehicleFactory>,
    artis::traffic::micro::utils::GeneratorParameters> _generator2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_3;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Stop<artis::traffic::micro::core::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>, artis::traffic::micro::core::StopParameters>,
    artis::traffic::micro::core::StopParameters> _stop;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Stop<artis::traffic::micro::core::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>, artis::traffic::micro::core::StopParameters>,
    artis::traffic::micro::core::StopParameters> _stop_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Junction<Vehicle>,
    artis::traffic::micro::core::JunctionParameters> _junction;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::End> _end;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class ThreeLinks1With2StopsView : public artis::traffic::core::View {
public:
  ThreeLinks1With2StopsView() {
    selector("Generator_1:counter",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::GENERATOR_1,
              artis::traffic::micro::utils::Generator<VehicleFactory>::vars::COUNTER});
    selector("Generator_2:counter",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::GENERATOR_2,
              artis::traffic::micro::utils::Generator<VehicleFactory>::vars::COUNTER});
    selector("Link_1:vehicle_number",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_1:vehicle_positions",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_1:vehicle_speed",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_1:vehicle_indexes",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_2:vehicle_number",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_2:vehicle_positions",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_2:vehicle_speed",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_2:vehicle_indexes",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_3:vehicle_number",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_3:vehicle_positions",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_3:vehicle_speed",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_3:vehicle_indexes",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Stop:vehicle_index",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::STOP,
              artis::traffic::micro::core::Stop<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::StopParameters>::vars::VEHICLE_INDEX});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class ThreeLinks2With2StopsView : public artis::traffic::core::View {
public:
  ThreeLinks2With2StopsView() {
    selector("Link_2:vehicle_number",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_2:vehicle_positions",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_2:vehicle_speed",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_2:vehicle_indexes",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class ThreeLinks3With2StopsView : public artis::traffic::core::View {
public:
  ThreeLinks3With2StopsView() {
    selector("Link_3:vehicle_number",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_3:vehicle_positions",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_3:vehicle_speed",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_3:vehicle_indexes",
             {ThreeLinksWithStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

#endif //ARTIS_TRAFFIC_TESTS_LINKS_GRAPH_MANAGER_HPP
