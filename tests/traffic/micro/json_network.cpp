/**
* @file tests/json_network.cpp
* @author The ARTIS Development Team
    * See the AUTHORS or Authors.txt file
*/

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-traffic/micro/utils/JsonReader.hpp>

#include <artis-star/common/context/Context.hpp>
#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/Output.hpp>
#include <artis-star/common/observer/TimedIterator.hpp>

#include <artis-traffic/micro/core/Vehicle.hpp>

#include <artis-traffic/micro/core/vehicle-dynamics/EventGippsVehicleDynamics.hpp>
//#include <artis-traffic/micro/core/vehicle-dynamics/TimeGippsVehicleDynamics.hpp>

#include "JSONNetworkGraphManager.hpp"

#include <iostream>
#include <artis-traffic/micro/core/vehicle-dynamics/TimeGippsVehicleDynamics.hpp>

template<class Vehicle, class VehicleEntry, class VehicleState>
class Runner {
public:
  using Network = artis::traffic::micro::utils::Network<artis::traffic::micro::utils::InputData,
    artis::traffic::micro::utils::OutputData, artis::traffic::micro::utils::NodeData,
    artis::traffic::micro::utils::JunctionData, artis::traffic::micro::utils::StopData,
    artis::traffic::micro::utils::LinkData>;

  Runner(const std::string &path) : _path(path) {}

  void operator()(unsigned int index) {
    std::ifstream input(_path + "test_calais_set" + std::to_string(index) + ".json");

    if (input) {
      std::string str((std::istreambuf_iterator<char>(input)), std::istreambuf_iterator<char>());

//      artis::traffic::micro::utils::JsonReader<artis::traffic::micro::utils::InputData, artis::traffic::micro::utils::OutputData,
//              artis::traffic::micro::utils::NodeData, artis::traffic::micro::utils::JunctionData,
//              artis::traffic::micro::utils::StopData, artis::traffic::micro::utils::LinkData> reader;

        artis::traffic::micro::utils::JsonReader reader;

      reader.parse_network(str);

      const auto &network = reader.network();

      artis::common::context::Context<artis::common::DoubleTime> context(0, 1500);
      artis::common::RootCoordinator<
        artis::common::DoubleTime, artis::pdevs::Coordinator<
          artis::common::DoubleTime,
          JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime>,
          artis::common::NoParameters, Network>
      > rc(context, "root", artis::common::NoParameters(), network);

      rc.attachView("Link_event_set" + std::to_string(index),
                    new JSONLinkView<Vehicle, VehicleEntry, VehicleState, Network, artis::common::DoubleTime>(network));
//      rc.attachView("Stop_event_set" + std::to_string(index), new JSONStopView<Vehicle, VehicleEntry,
//                    VehicleState, Network, artis::common::DoubleTime>(network));
//      rc.attachView("Generator_event_set" + std::to_string(index),
//                    new JSONGeneratorView<Vehicle, VehicleEntry, VehicleState, Network, artis::common::DoubleTime>(network));
//      rc.attachView("Junction_time_set" + std::to_string(index), new JSONJunctionView<Vehicle, VehicleEntry, VehicleState>(network));
      rc.switch_to_timed_observer(0.1);

      std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();

      rc.run(context);

      std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
      std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double> >(t2 - t1);

      std::cout << "Set " + std::to_string(index) << std::endl;
      std::cout << "Duration: " << time_span.count() << std::endl;

      artis::common::observer::Output<artis::common::DoubleTime,
        artis::common::observer::TimedIterator<artis::common::DoubleTime>>
        output(rc.observer());

      output(context.begin(), context.end(), {context.begin(), 0.1});
    } else {
        std::cout<<"missing file"<<std::endl;
    }
  }

private:
  std::string _path;
};

int main() {
  Runner<artis::traffic::micro::core::Vehicle,
    artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>,
    artis::traffic::micro::core::EventGippsVehicleState> runner("../../../../scripts/");

  for (int i = 6; i < 7; i++) {
    runner(i);
  }
  return 0;
}
