/**
 * @file tests/links_junction.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/Output.hpp>
#include <artis-star/common/observer/TimedIterator.hpp>

#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>

#include <artis-traffic/micro/core/Link.hpp>
#include <artis-traffic/micro/core/Junction.hpp>
#include <artis-traffic/micro/core/Disruptor.hpp>
#include <artis-traffic/micro/utils/End.hpp>
#include <artis-traffic/micro/utils/Generator.hpp>

#include <artis-traffic/micro/core/vehicle-dynamics/TimeGippsVehicleDynamics.hpp>
#include <artis-traffic/micro/core/vehicle-dynamics/EventGippsVehicleDynamics.hpp>

#include "MicroGraphManager.hpp"
#include <artis-traffic/micro/utils/VehicleFactory.hpp>

#define BOOST_TEST_MODULE Links_Junction_Tests

#include <boost/test/unit_test.hpp>

#include <chrono>

using namespace std::chrono;

/*********************************************************
 *  Generator -> Link (L1) -> Junction -> Link (L2) -> End
 *********************************************************/

struct TwoLinksWithJunctionParameters {
  artis::traffic::micro::utils::GeneratorParameters generator_parameters;
  artis::traffic::micro::core::LinkParameters link_1_parameters;
  artis::traffic::micro::core::LinkParameters link_2_parameters;
  artis::traffic::micro::core::JunctionParameters junction_parameters;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class TwoLinksWithJunctionGraphManager :
  public MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, TwoLinksWithJunctionParameters> {
public:
  enum sub_models {
    GENERATOR, LINK_1, LINK_2, JUNCTION, END
  };

  TwoLinksWithJunctionGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                                   const TwoLinksWithJunctionParameters &parameters,
                                   const artis::common::NoParameters &graph_parameters)
    :
    MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, TwoLinksWithJunctionParameters>(
      coordinator, parameters, graph_parameters),
    _generator("generator", parameters.generator_parameters),
    _link_1("L1", parameters.link_1_parameters),
    _link_2("L2", parameters.link_2_parameters),
    _junction("junction", parameters.junction_parameters),
    _end("end", artis::common::NoParameters()) {
    this->add_child(GENERATOR, &_generator);
    this->add_child(LINK_1, &_link_1);
    this->add_child(LINK_2, &_link_2);
    this->add_child(JUNCTION, &_junction);
    this->add_child(END, &_end);

    this->connect_generator_to_link(_generator, _link_1);
    this->connect_upstream_link_to_junction(_link_1, _junction, 0);
    this->connect_downstream_link_to_junction(_link_2, _junction, 0, 0);
    this->connect_link_to_end(_link_2, _end);
  }

  ~TwoLinksWithJunctionGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::Generator<VehicleFactory>,
    artis::traffic::micro::utils::GeneratorParameters> _generator;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<
      artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>,
      artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<
      artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>,
      artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Junction<Vehicle>,
    artis::traffic::micro::core::JunctionParameters> _junction;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::End> _end;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class TwoLinkWithJunctionView : public artis::traffic::core::View {
public:
  TwoLinkWithJunctionView(const std::string &link_name, int ID) {
    selector(link_name + ":vehicle_number",
             {ID,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector(link_name + ":vehicle_positions",
             {ID,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector(link_name + ":vehicle_speed",
             {ID,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector(link_name + ":vehicle_indexes",
             {ID,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

/*************************************************
 * Tests
 *************************************************/

BOOST_AUTO_TEST_CASE(TestCase_Links_Junction1)
{
  TwoLinksWithJunctionParameters parameters = {{0,  5,   10., 8., 432453, {}},
                                               {10, 500, 1,   0,  0,      {}},
                                               {10, 500, 1,   0,  0,      {}},
                                               {1,  1}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 100);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      TwoLinksWithJunctionGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
      TwoLinksWithJunctionParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Link_junction_1",
//                new TwoLinkWithJunctionView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>(
//                  "Link_1",
//                  TwoLinksWithJunctionGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>::LINK_1));
//  rc.attachView("Link_junction_2",
//                new TwoLinkWithJunctionView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>(
//                  "Link_2",
//                  TwoLinksWithJunctionGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>::LINK_2));
//  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

//  artis::common::observer::Output<artis::common::DoubleTime,
//    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
//    output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

BOOST_AUTO_TEST_CASE(TestCase_Links_Junction2)
{
  TwoLinksWithJunctionParameters parameters = {{0,  5,   10., 8., 432453, {}},
                                               {10, 500, 1,   0,  0,      {}},
                                               {5,  500, 1,   0,  0,      {}},
                                               {1,  1}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      TwoLinksWithJunctionGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
      TwoLinksWithJunctionParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Link_junction_1", new TwoLinkWithJunctionView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>("Link_1", TwoLinksWithJunctionGraphManager<artis::traffic::micro::core::TimeGippsVehicleDynamics, artis::traffic::micro::core::TimeGippsVehicleState>::LINK_1));
//  rc.attachView("Link_junction_2", new TwoLinkWithJunctionView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>("Link_2", TwoLinksWithJunctionGraphManager<artis::traffic::micro::core::TimeGippsVehicleDynamics, artis::traffic::micro::core::TimeGippsVehicleState>::LINK_2));
//  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

//  artis::common::observer::Output<artis::common::DoubleTime,
//    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
//    output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}