/**
 * @file tests/links.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/Output.hpp>
#include <artis-star/common/observer/TimedIterator.hpp>

#include <artis-traffic/micro/core/Vehicle.hpp>

//#include <artis-traffic/micro/core/vehicle-dynamics/TimeGippsVehicleDynamics.hpp>
//#include <artis-traffic/micro/core/vehicle-dynamics/EventGippsVehicleDynamics.hpp>

#include "links_def.hpp"

#define BOOST_TEST_MODULE Links_Tests

#include <boost/test/unit_test.hpp>

#include <chrono>

using namespace std::chrono;

/*************************************************
 * Tests
 *************************************************/

// only one link and node is immediately opened
BOOST_AUTO_TEST_CASE(TestCase_Links1)
{
  OnlyOneLinkParameters parameters = {{0,     5,     10., 8., 74826, {}},
                                      {10,    500,   1,   0,  0,     {}},
                                      {2000., 2000., 0.5, 0., 1,     1}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 500);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      OnlyOneLinkGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>,
        artis::traffic::micro::core::TimeGippsVehicleState>,
      OnlyOneLinkParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Link", new OnlyOneView());
//  rc.switch_to_timed_observer(0.1);
//
//  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

//  steady_clock::time_point t2 = steady_clock::now();
//
//  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);
//
//  std::cout << "Duration: " << time_span.count() << std::endl;
//
//  artis::observer::Output<artis::common::DoubleTime,
//    artis::observer::TimedIterator<artis::common::DoubleTime>>
//    output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

// only one link and node is closed at t=0
BOOST_AUTO_TEST_CASE(TestCase_Links2)
{
  OnlyOneLinkParameters parameters = {{0,    4,    20., 5.,   200, {}},
                                      {10,   500,  1,   0,    0,   {}},
                                      {200., 200., 0.5, 200., 1,   1}};
//  OnlyOneLinkParameters parameters = {{0,    2,    10., 5., 200},
//                                      {10,   500,  51},
//                                      {200., 200., 0.5, 200.}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 1500);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      OnlyOneLinkGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>,
        artis::traffic::micro::core::TimeGippsVehicleState>,
      OnlyOneLinkParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  rc.attachView("Link", new OnlyOneView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>());
  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  artis::common::observer::Output<artis::common::DoubleTime,
    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
    output(rc.observer());

  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

// two links without node and same speed for two links
BOOST_AUTO_TEST_CASE(TestCase_Links3)
{
  TwoLinksParameters parameters = {{0,  5,   30., 8., 876354, {}},
                                   {10, 500, 1,   0,  0,      {}},
                                   {10, 500, 1,   0,  0,      {}}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      TwoLinksGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
      TwoLinksParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Link_1", new Link1View);
//  rc.attachView("Link_2", new Link2View);
//  rc.switch_to_timed_observer(0.1);

//  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

//  steady_clock::time_point t2 = steady_clock::now();
//
//  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);
//
//  std::cout << "Duration: " << time_span.count() << std::endl;
//
//  artis::observer::Output<artis::common::DoubleTime,
//    artis::observer::TimedIterator<artis::common::DoubleTime>> output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

// two links with node and different speed for links
BOOST_AUTO_TEST_CASE(TestCase_Links4)
{
  TwoLinksWithNodeParameters parameters = {{0,    5,    10., 8.,   432453, {}},
                                           {10,   500,  1,   0,    0,      {}},
                                           {5,    500,  1,   0,    0,      {}},
                                           {180., 180., 0.5, 180., 1,      1}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      TwoLinksWithNodeGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
      TwoLinksWithNodeParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Link_node_1", new TwoLink1WithNodeView);
//  rc.attachView("Link_node_2", new TwoLink2WithNodeView);
//  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

//  artis::common::observer::Output<artis::common::DoubleTime,
//    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
//    output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

// three links with stop, link 1 with 500 mean
BOOST_AUTO_TEST_CASE(TestCase_Links5)
{
  ThreeLinksWithStopParameters parameters = {{0,    5,   500., 8., 432453, {}},
                                             {1000, 5,   30.,  8., 432453, {}},
                                             {10,   500, 1,    0,  1,      {}},
                                             {10,   500, 1,    0,  0,      {}},
                                             {10,   500, 1,    0,  0,      {}},
                                             {{10}, 1,   0,    {}},
                                             {2,    1}
  };

  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      ThreeLinksWithStopGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
      ThreeLinksWithStopParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Link_stop_1", new ThreeLink1WithStopView);
//  rc.attachView("Link_stop_2", new ThreeLink2WithStopView);
//  rc.attachView("Link_stop_3", new ThreeLink3WithStopView);
//  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

//  artis::common::observer::Output<artis::common::DoubleTime,
//    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
//    output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

// three links with stop, link 1 with 50 mean
BOOST_AUTO_TEST_CASE(TestCase_Links6)
{
  ThreeLinksWithStopParameters parameters = {{0,    5,   50., 8., 432453, {}},
                                             {1000, 5,   30., 8., 432453, {}},
                                             {10,   500, 1,   0,  1,      {}},
                                             {10,   500, 1,   0,  0,      {}},
                                             {10,   500, 1,   0,  0,      {}},
                                             {{10}, 1,   0,   {}},
                                             {2,    1}
  };
  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      ThreeLinksWithStopGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
      ThreeLinksWithStopParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Link_stop_1", new ThreeLink1WithStopView);
//  rc.attachView("Link_stop_2", new ThreeLink2WithStopView);
//  rc.attachView("Link_stop_3", new ThreeLink3WithStopView);
//  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

//  artis::common::observer::Output<artis::common::DoubleTime,
//    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
//    output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

// test 2 with variable mean
BOOST_AUTO_TEST_CASE(TestCase_Links7)
{
  for (int seed = 100; seed < 1000; seed += 100) {
    for (double mean = 20; mean < 100; mean += 10) {
      std::cout << mean << " " << seed << std::endl;
      OnlyOneLinkParameters parameters = {{0,    5,    static_cast<double>(mean), 8.,   static_cast<unsigned long>(seed), {}},
                                          {10,   500,  1,                         0,    0,                                {}},
                                          {200., 400., 0.5,                       200., 1,                                1}};
      artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
      artis::common::RootCoordinator<
        artis::common::DoubleTime, artis::pdevs::Coordinator<
          artis::common::DoubleTime,
          OnlyOneLinkGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
          OnlyOneLinkParameters>
      > rc(context, "root", parameters, artis::common::NoParameters());

//      rc.attachView("Link_mean_" + std::to_string(int(mean)) + "_seed_" + std::to_string(seed), new OnlyOneView());
//      rc.switch_to_timed_observer(0.1);

      steady_clock::time_point t1 = steady_clock::now();

      rc.run(context);

      steady_clock::time_point t2 = steady_clock::now();

      duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

      std::cout << "Duration: " << time_span.count() << std::endl;

//      artis::common::observer::Output<artis::common::DoubleTime,
//        artis::common::observer::TimedIterator<artis::common::DoubleTime>>
//        output(rc.observer());
//
//      output(context.begin(), context.end(), {context.begin(), 0.1});

      BOOST_CHECK(true);
    }
  }
}

// three links with stop, more vehicles
BOOST_AUTO_TEST_CASE(TestCase_Links8)
{
  ThreeLinksWithStopParameters parameters = {{0,    5,   30., 8., 432453, {}},
                                             {1000, 5,   25., 8., 432453, {}},
                                             {10,   500, 1,   0,  1,      {}},
                                             {10,   500, 1,   0,  0,      {}},
                                             {10,   500, 1,   0,  0,      {}},
                                             {{10}, 1,   0, {}},
                                             {2,    1}
  };
  artis::common::context::Context<artis::common::DoubleTime> context(0, 835);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      ThreeLinksWithStopGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
      ThreeLinksWithStopParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Link_stop_1", new ThreeLink1WithStopView);
//  rc.attachView("Link_stop_2", new ThreeLink2WithStopView);
//  rc.attachView("Link_stop_3", new ThreeLink3WithStopView);
//  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

//  artis::common::observer::Output<artis::common::DoubleTime,
//      artis::common::observer::TimedIterator<artis::common::DoubleTime>>
//      output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

// three links with 2 stops, link 1 with 50 mean, L1 stop has priority
BOOST_AUTO_TEST_CASE(TestCase_Links_Stops2)
{
  ThreeLinksWith2StopsParameters parameters = {{0,    5,   50., 8., 432453, {}},
                                               {1000, 5,   30., 8., 432453, {}},
                                               {10,   500, 1,   0,  1,      {}},
                                               {10,   500, 1,   0,  0,      {}},
                                               {10,   500, 1,   0,  0,      {}},
                                               {{10}, 0,   1,   {false}},
                                               {{10}, 0,   1,   {true}},
                                               {2,    1}
  };
  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      ThreeLinksWith2StopsGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
      ThreeLinksWith2StopsParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Link_stop_1", new ThreeLink1WithStopView);
//  rc.attachView("Link_stop_2", new ThreeLink2WithStopView);
//  rc.attachView("Link_stop_3", new ThreeLink3WithStopView);
//  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

//  artis::common::observer::Output<artis::common::DoubleTime,
//    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
//    output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

// three links with 2 stops, same dates, L1 stop has priority
BOOST_AUTO_TEST_CASE(TestCase_Links_Stops2SameDate)
{
  ThreeLinksWith2StopsParameters parameters = {{0,    5,   30., 8., 432453, {}},
                                               {1000, 5,   30., 8., 432453, {}},
                                               {10,   500, 1,   0,  1,      {}},
                                               {10,   500, 1,   0,  0,      {}},
                                               {10,   500, 1,   0,  0,      {}},
                                               {{10}, 0,   1,   {false}},
                                               {{10}, 0,   1,   {true}},
                                               {2,    1}
  };
  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      ThreeLinksWith2StopsGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
      ThreeLinksWith2StopsParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Link_stop_1", new ThreeLink1WithStopView);
//  rc.attachView("Link_stop_2", new ThreeLink2WithStopView);
//  rc.attachView("Link_stop_3", new ThreeLink3WithStopView);
//  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

//  artis::common::observer::Output<artis::common::DoubleTime,
//    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
//    output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}