/**
 * @file tests/collector_def.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_TESTS_COLLECTOR_DEF_HPP
#define ARTIS_TRAFFIC_TESTS_COLLECTOR_DEF_HPP

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

#include <artis-traffic/micro/core/Collector.hpp>
#include <artis-traffic/micro/core/Inserter.hpp>
#include <artis-traffic/micro/core/Link.hpp>
#include <artis-traffic/micro/utils/Counter.hpp>
#include <artis-traffic/micro/utils/Generator.hpp>
#include <artis-traffic/micro/coupled/NodeSwitch.hpp>

/************************************************************
 *  Generator |
 *            --> Collector -> Counter
 *  Generator |
 ************************************************************/

struct CollectorParameters
{
  artis::traffic::micro::utils::GeneratorParameters generator_1_parameters;
  artis::traffic::micro::utils::GeneratorParameters generator_2_parameters;
  artis::traffic::micro::core::CollectorParameters collector_parameters;
};

class CollectorGraphManager :
    public artis::pdevs::GraphManager<artis::common::DoubleTime, CollectorParameters>
{
public:
  enum submodels
  {
    GENERATOR_1, GENERATOR_2, COLLECTOR, COUNTER
  };

  CollectorGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                        const CollectorParameters &parameters,
                        const artis::common::NoParameters &graph_parameters)
      :
      artis::pdevs::GraphManager<artis::common::DoubleTime, CollectorParameters>(
          coordinator, parameters, graph_parameters),
      _generator_1("generator_1", parameters.generator_1_parameters),
      _generator_2("generator_2", parameters.generator_2_parameters),
      _collector("collector", parameters.collector_parameters),
      _counter("counter", artis::common::NoParameters())
  {
    add_child(GENERATOR_1, &_generator_1);
    add_child(GENERATOR_2, &_generator_2);
    add_child(COLLECTOR, &_collector);
    add_child(COUNTER, &_counter);

    out({&_generator_1, artis::traffic::micro::utils::Generator::outputs::OUT})
        >> in({&_collector, artis::traffic::micro::core::Collector::inputs::IN_PRIMARY});
    out({&_generator_2, artis::traffic::micro::utils::Generator::outputs::OUT})
        >> in({&_collector, artis::traffic::micro::core::Collector::inputs::IN_SECONDARY});

    out({&_collector, artis::traffic::micro::core::Collector::outputs::OPEN})
        >> in({&_generator_1, artis::traffic::micro::utils::Generator::inputs::OPEN});
    out({&_collector, artis::traffic::micro::core::Collector::outputs::CLOSE})
        >> in({&_generator_1, artis::traffic::micro::utils::Generator::inputs::CLOSE});

    out({&_collector, artis::traffic::micro::core::Collector::outputs::OPEN})
        >> in({&_generator_2, artis::traffic::micro::utils::Generator::inputs::OPEN});
    out({&_collector, artis::traffic::micro::core::Collector::outputs::CLOSE})
        >> in({&_generator_2, artis::traffic::micro::utils::Generator::inputs::CLOSE});

    out({&_collector, artis::traffic::micro::core::Collector::outputs::OUT})
        >> in({&_counter, artis::traffic::micro::utils::Counter::inputs::IN});
  }

  ~CollectorGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime,
                          artis::traffic::micro::utils::Generator,
                          artis::traffic::micro::utils::GeneratorParameters> _generator_1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
                          artis::traffic::micro::utils::Generator,
                          artis::traffic::micro::utils::GeneratorParameters> _generator_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
                          artis::traffic::micro::core::Collector,
                          artis::traffic::micro::core::CollectorParameters> _collector;
  artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::utils::Counter> _counter;
};

class CollectorView : public artis::traffic::core::View
{
public:
  CollectorView()
  {
    selector("Counter:counter",
             {CollectorGraphManager::COUNTER, artis::traffic::micro::utils::Counter::vars::COUNTER});
    selector("Generator_1:counter", {CollectorGraphManager::GENERATOR_1,
                                     artis::traffic::micro::utils::Generator::vars::COUNTER});
    selector("Generator_2:counter", {CollectorGraphManager::GENERATOR_2,
                                     artis::traffic::micro::utils::Generator::vars::COUNTER});
  }
};

/*****************************************************************
 *              Generator -> Link |
 *                                --> Collector -> Link ----------->|
 *                                                                  --> Collector -> Link -> Counter
 *  Generator -> Link -> Inserter |   Generator -> Link -> Inserter |
 *****************************************************************/

struct InserterParameters
{
  artis::traffic::micro::utils::GeneratorParameters generator_1_parameters;
  artis::traffic::micro::utils::GeneratorParameters generator_2_parameters;
  artis::traffic::micro::utils::GeneratorParameters generator_3_parameters;
  artis::traffic::micro::core::LinkParameters link_1_parameters;
  artis::traffic::micro::core::LinkParameters link_2_parameters;
  artis::traffic::micro::core::LinkParameters link_3_parameters;
  artis::traffic::micro::core::LinkParameters link_4_parameters;
  artis::traffic::micro::core::LinkParameters link_5_parameters;
  artis::traffic::micro::core::LinkParameters link_6_parameters;
  artis::traffic::micro::core::LinkParameters link_7_parameters;
  artis::traffic::micro::core::CollectorParameters collector_1_parameters;
  artis::traffic::micro::core::InserterParameters inserter_1_parameters;
  artis::traffic::micro::core::CollectorParameters collector_2_parameters;
  artis::traffic::micro::core::InserterParameters inserter_2_parameters;
};

class InserterGraphManager :
    public artis::pdevs::GraphManager<artis::common::DoubleTime, InserterParameters>
{
public:
  enum submodels
  {
    GENERATOR_1,
    GENERATOR_2,
    GENERATOR_3,
    LINK_1,
    LINK_2,
    LINK_3,
    LINK_4,
    LINK_5,
    LINK_6,
    LINK_7,
    INSERTER_1,
    COLLECTOR_1,
    INSERTER_2,
    COLLECTOR_2,
    COUNTER_1,
    COUNTER_2,
    COUNTER_3,
    SWITCH_1,
    SWITCH_2
  };

  InserterGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                       const InserterParameters &parameters,
                       const artis::common::NoParameters &graph_parameters)
      :
      artis::pdevs::GraphManager<artis::common::DoubleTime, InserterParameters>(
          coordinator, parameters, graph_parameters),
      _generator_1("generator_1", parameters.generator_1_parameters),
      _generator_2("generator_2", parameters.generator_2_parameters),
      _generator_3("generator_3", parameters.generator_3_parameters),
      _link_1("link_1", parameters.link_1_parameters),
      _link_2("link_2", parameters.link_2_parameters),
      _link_3("link_3", parameters.link_3_parameters),
      _link_4("link_4", parameters.link_4_parameters),
      _link_5("link_5", parameters.link_5_parameters),
      _link_6("link_6", parameters.link_6_parameters),
      _link_7("link_7", parameters.link_7_parameters),
      _inserter_1("inserter_1", parameters.inserter_1_parameters),
      _collector_1("collector_1", parameters.collector_1_parameters),
      _inserter_2("inserter_2", parameters.inserter_2_parameters),
      _collector_2("collector_2", parameters.collector_2_parameters),
      _counter_1("counter_1", artis::common::NoParameters()),
      _counter_2("counter_1", artis::common::NoParameters()),
      _counter_3("counter_1", artis::common::NoParameters()),
      _switch_1("switch_1", {{1000000, 0, 0.5, 0}, {{"out_1", "out_2"}, 87374}}, graph_parameters),
      _switch_2("switch_2", {{1000000, 0, 0.5, 0}, {{"out_1", "out_2"}, 6636}}, graph_parameters)
  {
    add_child(GENERATOR_1, &_generator_1);
    add_child(GENERATOR_2, &_generator_2);
    add_child(GENERATOR_3, &_generator_3);
    add_child(LINK_1, &_link_1);
    add_child(LINK_2, &_link_2);
    add_child(LINK_3, &_link_3);
    add_child(LINK_4, &_link_4);
    add_child(LINK_5, &_link_5);
    add_child(LINK_6, &_link_6);
    add_child(LINK_7, &_link_7);
    add_child(INSERTER_1, &_inserter_1);
    add_child(COLLECTOR_1, &_collector_1);
    add_child(INSERTER_2, &_inserter_2);
    add_child(COLLECTOR_2, &_collector_2);
    add_child(COUNTER_1, &_counter_1);
    add_child(COUNTER_2, &_counter_2);
    add_child(COUNTER_3, &_counter_3);
    add_child(SWITCH_1, &_switch_1);
    add_child(SWITCH_2, &_switch_2);

    // generator_1 <-> link_1
    out({&_generator_1, artis::traffic::micro::utils::Generator::outputs::OUT})
        >> in({&_link_1, artis::traffic::micro::core::Link::inputs::IN});
    out({&_link_1, artis::traffic::micro::core::Link::outputs::OPEN})
        >> in({&_generator_1, artis::traffic::micro::utils::Generator::inputs::OPEN});
    out({&_link_1, artis::traffic::micro::core::Link::outputs::CLOSE})
        >> in({&_generator_1, artis::traffic::micro::utils::Generator::inputs::CLOSE});

    // generator_2 <-> link_2
    out({&_generator_2, artis::traffic::micro::utils::Generator::outputs::OUT})
        >> in({&_link_2, artis::traffic::micro::core::Link::inputs::IN});
    out({&_link_2, artis::traffic::micro::core::Link::outputs::OPEN})
        >> in({&_generator_2, artis::traffic::micro::utils::Generator::inputs::OPEN});
    out({&_link_2, artis::traffic::micro::core::Link::outputs::CLOSE})
        >> in({&_generator_2, artis::traffic::micro::utils::Generator::inputs::CLOSE});

    // generator_3 <-> link_4
    out({&_generator_3, artis::traffic::micro::utils::Generator::outputs::OUT})
        >> in({&_link_4, artis::traffic::micro::core::Link::inputs::IN});
    out({&_link_4, artis::traffic::micro::core::Link::outputs::OPEN})
        >> in({&_generator_3, artis::traffic::micro::utils::Generator::inputs::OPEN});
    out({&_link_4, artis::traffic::micro::core::Link::outputs::CLOSE})
        >> in({&_generator_3, artis::traffic::micro::utils::Generator::inputs::CLOSE});

    // link_1 <-> switch_1
    out({&_link_1, artis::traffic::micro::core::Link::outputs::OUT})
        >> in({&_switch_1, artis::traffic::micro::coupled::NodeSwitchGraphManager::inputs::IN});
    out({&_switch_1, artis::traffic::micro::coupled::NodeSwitchGraphManager::outputs::OPEN})
        >> in({&_link_1, artis::traffic::micro::core::Link::inputs::OPEN});
    out({&_switch_1, artis::traffic::micro::coupled::NodeSwitchGraphManager::outputs::CLOSE})
        >> in({&_link_1, artis::traffic::micro::core::Link::inputs::CLOSE});

    // switch_1 <-> collector_1
    out({&_switch_1, artis::traffic::micro::coupled::NodeSwitchGraphManager::outputs::OUT})
        >> in({&_collector_1, artis::traffic::micro::core::Collector::inputs::IN_PRIMARY});
    out({&_collector_1, artis::traffic::micro::core::Collector::outputs::OPEN})
        >> in({&_switch_1, artis::traffic::micro::coupled::NodeSwitchGraphManager::inputs::OPEN});
    out({&_collector_1, artis::traffic::micro::core::Collector::outputs::CLOSE})
        >> in({&_switch_1, artis::traffic::micro::coupled::NodeSwitchGraphManager::inputs::CLOSE});

    // switch_1 <-> link_6
    out({&_switch_1, artis::traffic::micro::coupled::NodeSwitchGraphManager::outputs::OUT + 1})
        >> in({&_link_6, artis::traffic::micro::core::Link::inputs::IN});
    out({&_link_6, artis::traffic::micro::core::Link::outputs::OPEN})
        >> in({&_switch_1, artis::traffic::micro::coupled::NodeSwitchGraphManager::inputs::OPEN});
    out({&_link_6, artis::traffic::micro::core::Link::outputs::CLOSE})
        >> in({&_switch_1, artis::traffic::micro::coupled::NodeSwitchGraphManager::inputs::CLOSE});

    // inserter_1 <-> collector_1
    out({&_inserter_1, artis::traffic::micro::core::Inserter::outputs::OUT})
        >> in({&_collector_1, artis::traffic::micro::core::Collector::inputs::IN_SECONDARY});
    out({&_collector_1, artis::traffic::micro::core::Collector::outputs::OPEN})
        >> in({&_inserter_1, artis::traffic::micro::core::Inserter::inputs::OPEN});
    out({&_collector_1, artis::traffic::micro::core::Collector::outputs::CLOSE})
        >> in({&_inserter_1, artis::traffic::micro::core::Inserter::inputs::CLOSE});

    // link_2 <-> inserter_1
    out({&_link_2, artis::traffic::micro::core::Link::outputs::OUT})
        >> in({&_inserter_1, artis::traffic::micro::core::Inserter::inputs::IN});
    out({&_inserter_1, artis::traffic::micro::core::Inserter::outputs::OPEN})
        >> in({&_link_2, artis::traffic::micro::core::Link::inputs::OPEN});
    out({&_inserter_1, artis::traffic::micro::core::Inserter::outputs::CLOSE})
        >> in({&_link_2, artis::traffic::micro::core::Link::inputs::CLOSE});

    // collector_1 <-> link_3
    out({&_collector_1, artis::traffic::micro::core::Collector::outputs::OUT})
        >> in({&_link_3, artis::traffic::micro::core::Link::inputs::IN});
    out({&_link_3, artis::traffic::micro::core::Link::outputs::OPEN})
        >> in({&_collector_1, artis::traffic::micro::core::Collector::inputs::OPEN});
    out({&_link_3, artis::traffic::micro::core::Link::outputs::CLOSE})
        >> in({&_collector_1, artis::traffic::micro::core::Collector::inputs::CLOSE});

    // link_1 <-> inserter_1
    out({&_link_1, artis::traffic::micro::core::Link::outputs::STATE})
        >> in({&_inserter_1, artis::traffic::micro::core::Inserter::inputs::STATE});
    out({&_inserter_1, artis::traffic::micro::core::Inserter::outputs::STATE})
        >> in({&_link_1, artis::traffic::micro::core::Link::inputs::STATE});

    // switch_1 <-> inserter_1
    out({&_switch_1, artis::traffic::micro::coupled::NodeSwitchGraphManager::outputs::STATE})
        >> in({&_inserter_1, artis::traffic::micro::core::Inserter::inputs::STATE});
    out({&_inserter_1, artis::traffic::micro::core::Inserter::outputs::STATE})
        >> in({&_switch_1, artis::traffic::micro::coupled::NodeSwitchGraphManager::inputs::STATE});

    // link_3 <-> switch_2
    out({&_link_3, artis::traffic::micro::core::Link::outputs::OUT})
        >> in({&_switch_2, artis::traffic::micro::coupled::NodeSwitchGraphManager::inputs::IN});
    out({&_switch_2, artis::traffic::micro::coupled::NodeSwitchGraphManager::outputs::OPEN})
        >> in({&_link_3, artis::traffic::micro::core::Link::inputs::OPEN});
    out({&_switch_2, artis::traffic::micro::coupled::NodeSwitchGraphManager::outputs::CLOSE})
        >> in({&_link_3, artis::traffic::micro::core::Link::inputs::CLOSE});

    // switch_2 <-> collector_2
    out({&_switch_2, artis::traffic::micro::coupled::NodeSwitchGraphManager::outputs::OUT})
        >> in({&_collector_2, artis::traffic::micro::core::Collector::inputs::IN_PRIMARY});
    out({&_collector_2, artis::traffic::micro::core::Collector::outputs::OPEN})
        >> in({&_switch_2, artis::traffic::micro::coupled::NodeSwitchGraphManager::inputs::OPEN});
    out({&_collector_2, artis::traffic::micro::core::Collector::outputs::CLOSE})
        >> in({&_switch_2, artis::traffic::micro::coupled::NodeSwitchGraphManager::inputs::CLOSE});

    // switch_2 <-> link_7
    out({&_switch_2, artis::traffic::micro::coupled::NodeSwitchGraphManager::outputs::OUT + 1})
        >> in({&_link_7, artis::traffic::micro::core::Link::inputs::IN});
    out({&_link_7, artis::traffic::micro::core::Link::outputs::OPEN})
        >> in({&_switch_2, artis::traffic::micro::coupled::NodeSwitchGraphManager::inputs::OPEN});
    out({&_link_7, artis::traffic::micro::core::Link::outputs::CLOSE})
        >> in({&_switch_2, artis::traffic::micro::coupled::NodeSwitchGraphManager::inputs::CLOSE});

    // inserter_2 <-> collector_2
    out({&_inserter_2, artis::traffic::micro::core::Inserter::outputs::OUT})
        >> in({&_collector_2, artis::traffic::micro::core::Collector::inputs::IN_SECONDARY});
    out({&_collector_2, artis::traffic::micro::core::Collector::outputs::OPEN})
        >> in({&_inserter_2, artis::traffic::micro::core::Inserter::inputs::OPEN});
    out({&_collector_2, artis::traffic::micro::core::Collector::outputs::CLOSE})
        >> in({&_inserter_2, artis::traffic::micro::core::Inserter::inputs::CLOSE});

    // link_4 <-> inserter_2
    out({&_link_4, artis::traffic::micro::core::Link::outputs::OUT})
        >> in({&_inserter_2, artis::traffic::micro::core::Inserter::inputs::IN});
    out({&_inserter_2, artis::traffic::micro::core::Inserter::outputs::OPEN})
        >> in({&_link_4, artis::traffic::micro::core::Link::inputs::OPEN});
    out({&_inserter_2, artis::traffic::micro::core::Inserter::outputs::CLOSE})
        >> in({&_link_4, artis::traffic::micro::core::Link::inputs::CLOSE});

    // collector_2 <-> link_5
    out({&_collector_2, artis::traffic::micro::core::Collector::outputs::OUT})
        >> in({&_link_5, artis::traffic::micro::core::Link::inputs::IN});
    out({&_link_5, artis::traffic::micro::core::Link::outputs::OPEN})
        >> in({&_collector_2, artis::traffic::micro::core::Collector::inputs::OPEN});
    out({&_link_5, artis::traffic::micro::core::Link::outputs::CLOSE})
        >> in({&_collector_2, artis::traffic::micro::core::Collector::inputs::CLOSE});

    // link_3 <-> inserter_2
    out({&_link_3, artis::traffic::micro::core::Link::outputs::STATE})
        >> in({&_inserter_2, artis::traffic::micro::core::Inserter::inputs::STATE});
    out({&_inserter_2, artis::traffic::micro::core::Inserter::outputs::STATE})
        >> in({&_link_3, artis::traffic::micro::core::Link::inputs::STATE});

    // switch_2 <-> inserter_2
    out({&_switch_2, artis::traffic::micro::coupled::NodeSwitchGraphManager::outputs::STATE})
        >> in({&_inserter_2, artis::traffic::micro::core::Inserter::inputs::STATE});
    out({&_inserter_2, artis::traffic::micro::core::Inserter::outputs::STATE})
        >> in({&_switch_2, artis::traffic::micro::coupled::NodeSwitchGraphManager::inputs::STATE});

    // link_5 -> counter_1
    out({&_link_5, artis::traffic::micro::core::Link::outputs::OUT})
        >> in({&_counter_1, artis::traffic::micro::utils::Counter::inputs::IN});
    out({&_counter_1, artis::traffic::micro::utils::Counter::outputs::OPEN})
        >> in({&_link_5, artis::traffic::micro::core::Link::inputs::OPEN});

    // link_6 -> counter_2
    out({&_link_6, artis::traffic::micro::core::Link::outputs::OUT})
        >> in({&_counter_2, artis::traffic::micro::utils::Counter::inputs::IN});
    out({&_counter_2, artis::traffic::micro::utils::Counter::outputs::OPEN})
        >> in({&_link_6, artis::traffic::micro::core::Link::inputs::OPEN});

    // link_7 -> counter_3
    out({&_link_7, artis::traffic::micro::core::Link::outputs::OUT})
        >> in({&_counter_3, artis::traffic::micro::utils::Counter::inputs::IN});
    out({&_counter_3, artis::traffic::micro::utils::Counter::outputs::OPEN})
        >> in({&_link_7, artis::traffic::micro::core::Link::inputs::OPEN});

  }

  ~InserterGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime,
                          artis::traffic::micro::utils::Generator,
                          artis::traffic::micro::utils::GeneratorParameters> _generator_1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
                          artis::traffic::micro::utils::Generator,
                          artis::traffic::micro::utils::GeneratorParameters> _generator_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
                          artis::traffic::micro::utils::Generator,
                          artis::traffic::micro::utils::GeneratorParameters> _generator_3;
  artis::pdevs::Simulator<artis::common::DoubleTime,
                          artis::traffic::micro::core::Link,
                          artis::traffic::micro::core::LinkParameters> _link_1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
                          artis::traffic::micro::core::Link,
                          artis::traffic::micro::core::LinkParameters> _link_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
                          artis::traffic::micro::core::Link,
                          artis::traffic::micro::core::LinkParameters> _link_3;
  artis::pdevs::Simulator<artis::common::DoubleTime,
                          artis::traffic::micro::core::Link,
                          artis::traffic::micro::core::LinkParameters> _link_4;
  artis::pdevs::Simulator<artis::common::DoubleTime,
                          artis::traffic::micro::core::Link,
                          artis::traffic::micro::core::LinkParameters> _link_5;
  artis::pdevs::Simulator<artis::common::DoubleTime,
                          artis::traffic::micro::core::Link,
                          artis::traffic::micro::core::LinkParameters> _link_6;
  artis::pdevs::Simulator<artis::common::DoubleTime,
                          artis::traffic::micro::core::Link,
                          artis::traffic::micro::core::LinkParameters> _link_7;
  artis::pdevs::Simulator<artis::common::DoubleTime,
                          artis::traffic::micro::core::Inserter,
                          artis::traffic::micro::core::InserterParameters> _inserter_1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
                          artis::traffic::micro::core::Collector,
                          artis::traffic::micro::core::CollectorParameters> _collector_1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
                          artis::traffic::micro::core::Inserter,
                          artis::traffic::micro::core::InserterParameters> _inserter_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
                          artis::traffic::micro::core::Collector,
                          artis::traffic::micro::core::CollectorParameters> _collector_2;
  artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::utils::Counter> _counter_1;
  artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::utils::Counter> _counter_2;
  artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::utils::Counter> _counter_3;
  artis::pdevs::Coordinator<artis::common::DoubleTime,
                            artis::traffic::micro::coupled::NodeSwitchGraphManager,
                            artis::traffic::micro::coupled::NodeSwitchParameters> _switch_1;
  artis::pdevs::Coordinator<artis::common::DoubleTime,
                            artis::traffic::micro::coupled::NodeSwitchGraphManager,
                            artis::traffic::micro::coupled::NodeSwitchParameters> _switch_2;
};

class InserterView : public artis::traffic::core::View
{
public:
  InserterView()
  {
    selector("Counter_1:counter",
             {InserterGraphManager::COUNTER_1, artis::traffic::micro::utils::Counter::vars::COUNTER});
    selector("Counter_2:counter",
             {InserterGraphManager::COUNTER_2, artis::traffic::micro::utils::Counter::vars::COUNTER});
    selector("Counter_3:counter",
             {InserterGraphManager::COUNTER_3, artis::traffic::micro::utils::Counter::vars::COUNTER});
    selector("Generator_1:counter", {InserterGraphManager::GENERATOR_1,
                                     artis::traffic::micro::utils::Generator::vars::COUNTER});
    selector("Generator_2:counter", {InserterGraphManager::GENERATOR_2,
                                     artis::traffic::micro::utils::Generator::vars::COUNTER});
    selector("Generator_3:counter", {InserterGraphManager::GENERATOR_3,
                                     artis::traffic::micro::utils::Generator::vars::COUNTER});
    selector("Link_1:vehicle_number", {InserterGraphManager::LINK_1,
                                       artis::traffic::micro::core::Link::vars::VEHICLE_NUMBER});
    selector("Link_2:vehicle_number", {InserterGraphManager::LINK_2,
                                       artis::traffic::micro::core::Link::vars::VEHICLE_NUMBER});
    selector("Link_3:vehicle_number", {InserterGraphManager::LINK_3,
                                       artis::traffic::micro::core::Link::vars::VEHICLE_NUMBER});
    selector("Link_4:vehicle_number", {InserterGraphManager::LINK_4,
                                       artis::traffic::micro::core::Link::vars::VEHICLE_NUMBER});
    selector("Link_5:vehicle_number", {InserterGraphManager::LINK_5,
                                       artis::traffic::micro::core::Link::vars::VEHICLE_NUMBER});
    selector("Link_6:vehicle_number", {InserterGraphManager::LINK_6,
                                       artis::traffic::micro::core::Link::vars::VEHICLE_NUMBER});
    selector("Link_7:vehicle_number", {InserterGraphManager::LINK_7,
                                       artis::traffic::micro::core::Link::vars::VEHICLE_NUMBER});
    selector("Link_1:vehicle_positions", {InserterGraphManager::LINK_1,
                                          artis::traffic::micro::core::Link::vars::VEHICLE_POSITIONS});
    selector("Link_2:vehicle_positions", {InserterGraphManager::LINK_2,
                                          artis::traffic::micro::core::Link::vars::VEHICLE_POSITIONS});
    selector("Link_3:vehicle_positions", {InserterGraphManager::LINK_3,
                                          artis::traffic::micro::core::Link::vars::VEHICLE_POSITIONS});
    selector("Link_4:vehicle_positions", {InserterGraphManager::LINK_4,
                                          artis::traffic::micro::core::Link::vars::VEHICLE_POSITIONS});
    selector("Link_5:vehicle_positions", {InserterGraphManager::LINK_5,
                                          artis::traffic::micro::core::Link::vars::VEHICLE_POSITIONS});
    selector("Link_6:vehicle_positions", {InserterGraphManager::LINK_6,
                                          artis::traffic::micro::core::Link::vars::VEHICLE_POSITIONS});
    selector("Link_7:vehicle_positions", {InserterGraphManager::LINK_7,
                                          artis::traffic::micro::core::Link::vars::VEHICLE_POSITIONS});
    selector("Link_1:vehicle_indexes", {InserterGraphManager::LINK_1,
                                        artis::traffic::micro::core::Link::vars::VEHICLE_INDEXES});
    selector("Link_2:vehicle_indexes", {InserterGraphManager::LINK_2,
                                        artis::traffic::micro::core::Link::vars::VEHICLE_INDEXES});
    selector("Link_3:vehicle_indexes", {InserterGraphManager::LINK_3,
                                        artis::traffic::micro::core::Link::vars::VEHICLE_INDEXES});
    selector("Link_4:vehicle_indexes", {InserterGraphManager::LINK_4,
                                        artis::traffic::micro::core::Link::vars::VEHICLE_INDEXES});
    selector("Link_5:vehicle_indexes", {InserterGraphManager::LINK_5,
                                        artis::traffic::micro::core::Link::vars::VEHICLE_INDEXES});
    selector("Link_6:vehicle_indexes", {InserterGraphManager::LINK_6,
                                        artis::traffic::micro::core::Link::vars::VEHICLE_INDEXES});
    selector("Link_7:vehicle_indexes", {InserterGraphManager::LINK_7,
                                        artis::traffic::micro::core::Link::vars::VEHICLE_INDEXES});
    selector("Collector_1:counter", {InserterGraphManager::COLLECTOR_1,
                                     artis::traffic::micro::core::Collector::vars::VEHICLE_NUMBER});
    selector("Collector_2:counter", {InserterGraphManager::COLLECTOR_2,
                                     artis::traffic::micro::core::Collector::vars::VEHICLE_NUMBER});
    selector("Inserter_1:counter", {InserterGraphManager::INSERTER_1,
                                    artis::traffic::micro::core::Inserter::vars::VEHICLE_NUMBER});
    selector("Inserter_1:index", {InserterGraphManager::INSERTER_1,
                                  artis::traffic::micro::core::Inserter::vars::VEHICLE_INDEX});
    selector("Inserter_2:counter", {InserterGraphManager::INSERTER_2,
                                    artis::traffic::micro::core::Inserter::vars::VEHICLE_NUMBER});
    selector("Inserter_2:index", {InserterGraphManager::INSERTER_2,
                                  artis::traffic::micro::core::Inserter::vars::VEHICLE_INDEX});
    selector("Switch_1:counter", {InserterGraphManager::SWITCH_1,
                                  artis::traffic::micro::coupled::NodeSwitchGraphManager::submodels::NODE,
                                  artis::traffic::micro::core::Node::vars::VEHICLE_NUMBER});
    selector("Switch_2:counter", {InserterGraphManager::SWITCH_2,
                                  artis::traffic::micro::coupled::NodeSwitchGraphManager::submodels::NODE,
                                  artis::traffic::micro::core::Node::vars::VEHICLE_NUMBER});
  }
};

#endif