/**
 * @file tests/links_stop_mean.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/Output.hpp>
#include <artis-star/common/observer/TimedIterator.hpp>
#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

#include <artis-traffic/micro/core/Link.hpp>
#include <artis-traffic/micro/core/Junction.hpp>
#include <artis-traffic/micro/core/Disruptor.hpp>
#include <artis-traffic/micro/utils/End.hpp>

#include <artis-traffic/micro/core/vehicle-dynamics/TimeGippsVehicleDynamics.hpp>
#include <artis-traffic/micro/core/vehicle-dynamics/EventGippsVehicleDynamics.hpp>

#include "MicroGraphManager.hpp"

#define BOOST_TEST_MODULE Link_LinksStopMean_Tests

#include <boost/test/unit_test.hpp>

#include <chrono>

using namespace std::chrono;

/*************************************************
 * Tests
 *************************************************/

struct TwoLinksStopParameters {
  artis::traffic::micro::core::LinkParameters link_1_parameters;
  artis::traffic::micro::core::LinkParameters link_2_parameters;
  artis::traffic::micro::core::StopParameters stop_parameters;
  artis::traffic::micro::core::JunctionParameters junction_parameters;
  artis::traffic::micro::utils::GeneratorParameters generator_parameters;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class TwoLinksStopGraphManager :
  public MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, TwoLinksStopParameters> {
public:
  enum submodels {
    GENERATOR, LINK_1, STOP, JUNCTION, LINK_2, END
  };

  TwoLinksStopGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                           const TwoLinksStopParameters &parameters,
                           const artis::common::NoParameters &graph_parameters)
    :
    MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, TwoLinksStopParameters>(
      coordinator, parameters, graph_parameters),
    _generator("generator", parameters.generator_parameters),
    _link_1("link_1", parameters.link_1_parameters),
    _stop("stop", parameters.stop_parameters),
    _junction("junction", parameters.junction_parameters),
    _link_2("link_2", parameters.link_2_parameters),
    _end("end", artis::common::NoParameters()) {
    this->add_child(GENERATOR, &_generator);
    this->add_child(LINK_1, &_link_1);
    this->add_child(STOP, &_stop);
    this->add_child(JUNCTION, &_junction);
    this->add_child(LINK_2, &_link_2);
    this->add_child(END, &_end);

    this->connect_generator_to_link(_generator, _link_1);
    this->connect_upstream_link_to_stop(_link_1, _stop, 0, 1);
    this->connect_stop_to_junction(_stop, _junction, 0);
    this->connect_downstream_link_to_junction(_link_2, _junction, 0, 0);
    this->connect_link_to_end(_link_2, _end);
  }

  ~TwoLinksStopGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::utils::Generator<VehicleFactory>, artis::traffic::micro::utils::GeneratorParameters> _generator;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Stop<artis::traffic::micro::core::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>, artis::traffic::micro::core::StopParameters>,
    artis::traffic::micro::core::StopParameters> _stop;
  artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::core::Junction<Vehicle>, artis::traffic::micro::core::JunctionParameters> _junction;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_2;
  artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::utils::End> _end;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class TwoLinksStopView : public artis::traffic::core::View {
public:
  TwoLinksStopView() {
    selector("Link_1:vehicle_number",
             {TwoLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_1:vehicle_positions",
             {TwoLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_1:vehicle_indexes",
             {TwoLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_1:vehicle_speed",
             {TwoLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_1:event_count",
             {TwoLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::EVENT_COUNT});
    selector("Link_1:event_frequency",
             {TwoLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::EVENT_FREQUENCY});
    selector("Link_2:vehicle_positions",
             {TwoLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_2:vehicle_indexes",
             {TwoLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_2:vehicle_speed",
             {TwoLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Stop:vehicle_index",
             {TwoLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::STOP,
              artis::traffic::micro::core::Stop<artis::traffic::micro::core::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>, artis::traffic::micro::core::StopParameters>::vars::VEHICLE_INDEX});
//        selector("Link:vehicle_state",
//                 {TwoLinksStopGraphManager<VehicleEntry, VehicleState>::LINK,
//                  artis::traffic::micro::core::Link<VehicleEntry, VehicleState>::vars::VEHICLE_STATE});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
void run_simulation(TwoLinksStopParameters &parameters, int mean, int seed, int link_length) {

  artis::common::context::Context<artis::common::DoubleTime> context(0, 600);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      TwoLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>,
      TwoLinksStopParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  rc.attachView("Link_stop_event_length_" + std::to_string(link_length) + "_mean_" + std::to_string(mean) + "_seed_" +
                std::to_string(seed), new TwoLinksStopView<Vehicle, VehicleEntry, VehicleState>);
  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  artis::common::observer::Output<artis::common::DoubleTime,
    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
    output(rc.observer());

  output(context.begin(), context.end(), {context.begin(), 0.1});
}

// lower_speed_and_cant_fully_accelerate
BOOST_AUTO_TEST_CASE(TestCase_2LinkStopMean)
{
  for (int link_length = 870; link_length <= 1070; link_length += 200) {
    for (int seed = 1000; seed <= 1000; seed += 100) {
      for (int mean = 5; mean <= 30; mean += 5) {

        TwoLinksStopParameters parameters = {{10,   (double) link_length, 1,             0,  0,                    {}},
                                             {10,   300,                  1,             0,  0,                    {}},
                                             {{10}, 0,                    0,             {}},
                                             {1,    1},
                                             {0,    5,                    (double) mean, 8., (unsigned long) seed, {}}};
        std::cout << link_length << " " << mean << " " << seed << std::endl;
        run_simulation<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>(
          parameters, mean, seed, link_length);
      }
    }
  }
  BOOST_CHECK(true);
}