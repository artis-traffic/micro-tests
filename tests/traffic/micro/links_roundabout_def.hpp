/**
 * @file tests/links_roundabout_def.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_TESTS_LINKS_ROUNDABOUT_DEF_HPP
#define ARTIS_TRAFFIC_TESTS_LINKS_ROUNDABOUT_DEF_HPP

#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

#include <artis-traffic/micro/core/Link.hpp>
#include <artis-traffic/micro/core/Roundabout.hpp>
#include <artis-traffic/micro/core/Stop.hpp>
#include <artis-traffic/micro/core/Disruptor.hpp>
#include <artis-traffic/micro/utils/End.hpp>
#include <artis-traffic/micro/utils/Generator.hpp>

#include "MicroGraphManager.hpp"
#include <artis-traffic/micro/utils/VehicleFactory.hpp>

/************************************************************
 *  Link (L1) -> Roundabout (roundabout) -> Link (L2) -> End (end)
 *                   |
 *                   v
 *                Link (L3)
 ************************************************************/

struct ThreeLinksWithRoundaboutParameters {
  artis::traffic::micro::utils::GeneratorParameters generator_parameters;
  artis::traffic::micro::core::LinkParameters link_1_parameters;
  artis::traffic::micro::core::LinkParameters link_2_parameters;
  artis::traffic::micro::core::LinkParameters link_3_parameters;
  artis::traffic::micro::core::RoundaboutParameters roundabout_parameters;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class ThreeLinksWithRoundaboutGraphManager :
    public MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, ThreeLinksWithRoundaboutParameters> {
public:
  enum submodels {
    GENERATOR, LINK_1, LINK_2, LINK_3, ROUNDABOUT, END, END_2
  };

  ThreeLinksWithRoundaboutGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                                     const ThreeLinksWithRoundaboutParameters &parameters,
                                     const artis::common::NoParameters &graph_parameters)
      :
      MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, ThreeLinksWithRoundaboutParameters>(
          coordinator, parameters, graph_parameters),
      _generator("generator", parameters.generator_parameters),
      _link_1("L1", parameters.link_1_parameters),
      _link_2("L2", parameters.link_2_parameters),
      _link_3("L3", parameters.link_3_parameters),
      _roundabout("roundabout", parameters.roundabout_parameters),
      _end("end", artis::common::NoParameters()),
      _end_2("end_2", artis::common::NoParameters()){
    this->add_child(GENERATOR, &_generator);
    this->add_child(LINK_1, &_link_1);
    this->add_child(LINK_2, &_link_2);
    this->add_child(LINK_3, &_link_3);
    this->add_child(ROUNDABOUT, &_roundabout);
    this->add_child(END, &_end);
    this->add_child(END_2, &_end_2);

    this->connect_generator_to_link(_generator, _link_1);
    this->connect_upstream_link_to_roundabout(_link_1, _roundabout, 0, 1);
    this->connect_downstream_link_to_roundabout(_link_2, _roundabout, 0, 0);
    this->connect_downstream_link_to_roundabout(_link_3, _roundabout, 1, 1);
    this->connect_link_to_end(_link_2, _end);
    this->connect_link_to_end(_link_3, _end_2);
  }

  ~ThreeLinksWithRoundaboutGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime,
  artis::traffic::micro::utils::Generator<VehicleFactory>,
  artis::traffic::micro::utils::GeneratorParameters> _generator;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_3;
  artis::pdevs::Simulator<artis::common::DoubleTime,
  artis::traffic::micro::core::Roundabout<Vehicle>,
  artis::traffic::micro::core::RoundaboutParameters> _roundabout;
  artis::pdevs::Simulator<artis::common::DoubleTime,
  artis::traffic::micro::utils::End> _end;
  artis::pdevs::Simulator<artis::common::DoubleTime,
  artis::traffic::micro::utils::End> _end_2;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class ThreeLink1WithRoundaboutView : public artis::traffic::core::View {
public:
  ThreeLink1WithRoundaboutView() {
    selector("Link_1:vehicle_number",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_1:vehicle_positions",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_1:vehicle_speed",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_1:vehicle_indexes",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class ThreeLink2WithRoundaboutView : public artis::traffic::core::View {
public:
  ThreeLink2WithRoundaboutView() {
    selector("Link_2:vehicle_number",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_2:vehicle_positions",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_2:vehicle_speed",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_2:vehicle_indexes",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class ThreeLink3WithRoundaboutView : public artis::traffic::core::View {
public:
  ThreeLink3WithRoundaboutView() {
    selector("Link_3:vehicle_number",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_3:vehicle_positions",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_3:vehicle_speed",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_3:vehicle_indexes",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class ThreeLinkGlobalWithRoundaboutView : public artis::traffic::core::View {
public:
  ThreeLinkGlobalWithRoundaboutView() {
    selector("Link_1:vehicle_number",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_1:vehicle_positions",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_1:vehicle_speed",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_1:vehicle_indexes",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_2:vehicle_number",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_2:vehicle_positions",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_2:vehicle_speed",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_2:vehicle_indexes",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_3:vehicle_number",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_3:vehicle_positions",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_3:vehicle_speed",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_3:vehicle_indexes",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Roundabout:vehicle_number",
             {ThreeLinksWithRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::ROUNDABOUT,
              artis::traffic::micro::core::Roundabout<Vehicle>::vars::VEHICLE_NUMBER});
  }
};

/************************************************************
 *                 Link (L4)
 *                   |
 *                   v
 *  Link (L1) -> Roundabout (roundabout) -> Link (L2) -> End (end)
 *                   |
 *                   v
 *                Link (L3)
 ************************************************************/

struct FourLinksTwoInRoundaboutParameters {
  artis::traffic::micro::utils::GeneratorParameters generator_parameters;
  artis::traffic::micro::utils::GeneratorParameters generator_2_parameters;
  artis::traffic::micro::core::LinkParameters link_1_parameters;
  artis::traffic::micro::core::LinkParameters link_2_parameters;
  artis::traffic::micro::core::LinkParameters link_3_parameters;
  artis::traffic::micro::core::LinkParameters link_4_parameters;
  artis::traffic::micro::core::RoundaboutParameters roundabout_parameters;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
  class FourLinksTwoInRoundaboutGraphManager :
    public MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, FourLinksTwoInRoundaboutParameters> {
public:
  enum submodels {
    GENERATOR, GENERATOR_2, LINK_1, LINK_2, LINK_3, LINK_4, ROUNDABOUT, END, END_2
  };

  FourLinksTwoInRoundaboutGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                                     const FourLinksTwoInRoundaboutParameters &parameters,
                                     const artis::common::NoParameters &graph_parameters)
      :
      MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, FourLinksTwoInRoundaboutParameters>(
          coordinator, parameters, graph_parameters),
      _generator("generator", parameters.generator_parameters),
      _generator_2("generator2", parameters.generator_2_parameters),
      _link_1("L1", parameters.link_1_parameters),
      _link_2("L2", parameters.link_2_parameters),
      _link_3("L3", parameters.link_3_parameters),
      _link_4("L4", parameters.link_4_parameters),
      _roundabout("roundabout", parameters.roundabout_parameters),
      _end("end", artis::common::NoParameters()),
      _end_2("end_2", artis::common::NoParameters()){
    this->add_child(GENERATOR, &_generator);
    this->add_child(GENERATOR_2, &_generator_2);
    this->add_child(LINK_1, &_link_1);
    this->add_child(LINK_2, &_link_2);
    this->add_child(LINK_3, &_link_3);
    this->add_child(LINK_4, &_link_4);
    this->add_child(ROUNDABOUT, &_roundabout);
    this->add_child(END, &_end);
    this->add_child(END_2, &_end_2);

    this->connect_generator_to_link(_generator, _link_1);
    this->connect_generator_to_link(_generator_2, _link_4);
    this->connect_upstream_link_to_roundabout(_link_1, _roundabout, 0, 2);
    this->connect_upstream_link_to_roundabout(_link_4, _roundabout, 1, 2);
    this->connect_downstream_link_to_roundabout(_link_2, _roundabout, 0, 0);
    this->connect_downstream_link_to_roundabout(_link_3, _roundabout, 1, 1);
    this->connect_concurrent_links(_link_1, _link_4);
    this->connect_link_to_end(_link_2, _end);
    this->connect_link_to_end(_link_3, _end_2);
  }

  ~FourLinksTwoInRoundaboutGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime,
      artis::traffic::micro::utils::Generator<VehicleFactory>,
      artis::traffic::micro::utils::GeneratorParameters> _generator;
  artis::pdevs::Simulator<artis::common::DoubleTime,
      artis::traffic::micro::utils::Generator<VehicleFactory>,
      artis::traffic::micro::utils::GeneratorParameters> _generator_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_3;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_4;
  artis::pdevs::Simulator<artis::common::DoubleTime,
      artis::traffic::micro::core::Roundabout<Vehicle>,
      artis::traffic::micro::core::RoundaboutParameters> _roundabout;
  artis::pdevs::Simulator<artis::common::DoubleTime,
      artis::traffic::micro::utils::End> _end;
  artis::pdevs::Simulator<artis::common::DoubleTime,
      artis::traffic::micro::utils::End> _end_2;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class FourLink1TwoInRoundaboutView : public artis::traffic::core::View {
public:
  FourLink1TwoInRoundaboutView() {
    selector("Link_1:vehicle_number",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_1:vehicle_positions",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_1:vehicle_speed",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_1:vehicle_indexes",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class FourLink2TwoInRoundaboutView : public artis::traffic::core::View {
public:
  FourLink2TwoInRoundaboutView() {
    selector("Link_2:vehicle_number",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_2:vehicle_positions",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_2:vehicle_speed",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_2:vehicle_indexes",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class FourLink3TwoInRoundaboutView : public artis::traffic::core::View {
public:
  FourLink3TwoInRoundaboutView() {
    selector("Link_3:vehicle_number",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_3:vehicle_positions",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_3:vehicle_speed",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_3:vehicle_indexes",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class FourLink4TwoInRoundaboutView : public artis::traffic::core::View {
public:
  FourLink4TwoInRoundaboutView() {
    selector("Link_4:vehicle_number",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_4:vehicle_positions",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_4:vehicle_speed",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_4:vehicle_indexes",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class FourLinkGlobalTwoInRoundaboutView : public artis::traffic::core::View {
public:
  FourLinkGlobalTwoInRoundaboutView() {
    selector("Link_1:vehicle_number",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_1:vehicle_positions",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_1:vehicle_speed",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_1:vehicle_indexes",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_2:vehicle_number",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_2:vehicle_positions",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_2:vehicle_speed",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_2:vehicle_indexes",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_3:vehicle_number",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_3:vehicle_positions",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_3:vehicle_speed",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_3:vehicle_indexes",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_4:vehicle_number",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_4:vehicle_positions",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_4:vehicle_speed",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_4:vehicle_indexes",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Roundabout:vehicle_number",
             {FourLinksTwoInRoundaboutGraphManager<Vehicle, VehicleEntry, VehicleState>::ROUNDABOUT,
              artis::traffic::micro::core::Roundabout<Vehicle>::vars::VEHICLE_NUMBER});
  }
};

#endif //ARTIS_TRAFFIC_TESTS_LINKS_ROUNDABOUT_DEF_HPP
