/**
 * @file tests/one_link_with_disturbances.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/Output.hpp>
#include <artis-star/common/observer/TimedIterator.hpp>
#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

#include <artis-traffic/micro/core/Disruptor.hpp>
#include <artis-traffic/micro/core/Link.hpp>
#include <artis-traffic/micro/utils/End.hpp>

#include <artis-traffic/micro/core/vehicle-dynamics/TimeGippsVehicleDynamics.hpp>
#include <artis-traffic/micro/core/vehicle-dynamics/EventGippsVehicleDynamics.hpp>

#include "one_link_generator.hpp"
#include "MicroGraphManager.hpp"

#define BOOST_TEST_MODULE Link_OneLinkWithDisturbances_Tests

#include <boost/test/unit_test.hpp>

#include <chrono>

using namespace std::chrono;

/*************************************************
 * Tests
 *************************************************/
struct OnlyOneLinkWithDisturbancesParameters {
  artis::traffic::micro::core::LinkParameters link_parameters;
  artis::traffic::micro::core::DisturbanceParameters disruptor_parameters;
  GeneratorParameters generator_parameters;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class OneLinkWithDisturbancesGraphManager :
  public MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, OnlyOneLinkWithDisturbancesParameters> {
public:
  enum submodels {
    GENERATOR, LINK, END, DISRUPTOR
  };

  OneLinkWithDisturbancesGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                                      const OnlyOneLinkWithDisturbancesParameters &parameters,
                                      const artis::common::NoParameters &graph_parameters)
    :
    MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, OnlyOneLinkWithDisturbancesParameters>(
      coordinator, parameters, graph_parameters),
    _generator("generator", parameters.generator_parameters),
    _link("link", parameters.link_parameters),
    _end("end", artis::common::NoParameters()),
    _disruptor("disruptor", parameters.disruptor_parameters) {
    this->add_child(GENERATOR, &_generator);
    this->add_child(LINK, &_link);
    this->add_child(END, &_end);
    this->add_child(DISRUPTOR, &_disruptor);

      this->out({&_generator, Generator::outputs::OUT})
              >> this->in({&_link, artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::inputs::IN});

    this->connect_link_to_end(_link, _end);
    this->connect_disruptor_to_link(_disruptor, _link);
  }

  ~OneLinkWithDisturbancesGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime, Generator, GeneratorParameters> _generator;
    artis::pdevs::Simulator<artis::common::DoubleTime,
            artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
            artis::traffic::micro::core::LinkParameters> _link;
  artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::utils::End> _end;
  artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::core::Disruptor, artis::traffic::micro::core::DisturbanceParameters> _disruptor;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class OneLinkWithDisturbancesView : public artis::traffic::core::View {
public:
  OneLinkWithDisturbancesView() {
//      selector("Link:vehicle_number",
//               {OneLinkWithDisturbancesGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
//                artis::traffic::micro::core::Link<Vehicle, VehicleEntry, VehicleState>::vars::VEHICLE_NUMBER});
      selector("Link:vehicle_positions",
               {OneLinkWithDisturbancesGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
                artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
      selector("Link:vehicle_indexes",
               {OneLinkWithDisturbancesGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
                artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
      selector("Link:vehicle_speed",
               {OneLinkWithDisturbancesGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
                artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
//        selector("Link:vehicle_state",
//                 {OneLinkWithDisturbancesGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
//                  artis::traffic::micro::core::Link<Vehicle, VehicleEntry, VehicleState>::vars::VEHICLE_STATE});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
void run_simulator(const OnlyOneLinkWithDisturbancesParameters &parameters) {
  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      OneLinkWithDisturbancesGraphManager<Vehicle, VehicleEntry, VehicleState>,
      OnlyOneLinkWithDisturbancesParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Link", new OneLinkWithDisturbancesView());
//  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

//  artis::common::observer::Output<artis::common::DoubleTime,
//    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
//    output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), 0.1});
}

// only one link and one vehicle with one disturbance
BOOST_AUTO_TEST_CASE(TestCase_Disturbance1)
{
  OnlyOneLinkWithDisturbancesParameters parameters = {{10,                                                                                                                                                                       1000, 1, 0, 0, {}},
                                                      {{
                                                         {10, artis::traffic::micro::core::DisturbanceTypes::values::ACCELERATION, {2, 1, 2}, 1}}},
                                                      {{ {1,  4.5,                                                                 3,         10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}}}, {0}}};

  run_simulator<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>(
    parameters);

  BOOST_CHECK(true);
}

// only one link and one vehicle with one disturbance DOWN - long disturbance and vehicle 2 at 4
BOOST_AUTO_TEST_CASE(TestCase_Disturbance2)
{
  OnlyOneLinkWithDisturbancesParameters parameters = {{10, 1000, 1, 0, 0, {}},
                                                      {{
                                                         {10, artis::traffic::micro::core::DisturbanceTypes::values::DECELERATION, {5, 30, 5}, 1}}},
                                                      {{
                                                         {1,  4.5,                                                                 3,          10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}},
                                                         {2, 4.5, 3, 10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}}
                                                       },  {0, 4}}};

  run_simulator<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>(
    parameters);

  BOOST_CHECK(true);
}

// only one link and one vehicle with one disturbance DOWN - long disturbance and vehicle 2 at 6
BOOST_AUTO_TEST_CASE(TestCase_Disturbance3)
{
  OnlyOneLinkWithDisturbancesParameters parameters = {{10, 1000, 1, 0, 0, {}},
                                                      {{
                                                         {10, artis::traffic::micro::core::DisturbanceTypes::values::DECELERATION, {5, 30, 5}, 1}}},
                                                      {{
                                                         {1,  4.5,                                                                 3,          10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}},
                                                         {2, 4.5, 3, 10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}}
                                                       },  {0, 6}}};

  run_simulator<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>(
    parameters);

  BOOST_CHECK(true);
}

// only one link and one vehicle with one disturbance DOWN - short disturbance and vehicle 2 at 4
BOOST_AUTO_TEST_CASE(TestCase_Disturbance4)
{
  OnlyOneLinkWithDisturbancesParameters parameters = {{10, 1000, 1, 0, 0, {}},
                                                      {{
                                                         {10, artis::traffic::micro::core::DisturbanceTypes::values::DECELERATION, {5, 5, 5}, 1}}},
                                                      {{
                                                         {1,  4.5,                                                                 3,         10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}},
                                                         {2, 4.5, 3, 10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}}
                                                       },  {0, 4}}};

  run_simulator<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>(
    parameters);

  BOOST_CHECK(true);
}

// only one link and one vehicle with one disturbance DOWN - short disturbance and vehicle 2 at 6
BOOST_AUTO_TEST_CASE(TestCase_Disturbance5)
{
  OnlyOneLinkWithDisturbancesParameters parameters = {{10, 1000, 1, 0, 0, {}},
                                                      {{
                                                         {10, artis::traffic::micro::core::DisturbanceTypes::values::DECELERATION, {5, 5, 5}, 1}}},
                                                      {{
                                                         {1,  4.5,                                                                 3,         10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}},
                                                         {2, 4.5, 3, 10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}}
                                                       },  {0, 6}}};

  run_simulator<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>(
    parameters);

  BOOST_CHECK(true);
}

// only one link and one vehicle with one disturbance UP
BOOST_AUTO_TEST_CASE(TestCase_Disturbance6)
{
  OnlyOneLinkWithDisturbancesParameters parameters = {{10, 1000, 1, 0, 0, {}},
                                                      {{
                                                         {10, artis::traffic::micro::core::DisturbanceTypes::values::ACCELERATION, {5, 5, 5}, 1}}},
                                                      {{
                                                         {1,  4.5,                                                                 3,         10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}},
                                                         {2, 4.5, 3, 10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}}
                                                       },  {0, 4}}};

  run_simulator<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>(
    parameters);

  BOOST_CHECK(true);
}

// only one link, one vehicle with one disturbance DOWN and two followers
// collision with 0, 1, 2, ok with 0, 2, 6
BOOST_AUTO_TEST_CASE(TestCase_Disturbance7)
{
  OnlyOneLinkWithDisturbancesParameters parameters = {{10, 1000, 1, 0, 0, {}},
                                                      {{
                                                         {5, artis::traffic::micro::core::DisturbanceTypes::values::DECELERATION, {5, 5, 5}, 1},
                                                       }},
                                                      {{
                                                         {1, 4.5,                                                                 3,         10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}},
                                                         {2, 4.5, 3, 10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}},
                                                         {3, 4.5, 3, 10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}}
                                                       },  {0, 2, 8}}};

  run_simulator<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>(
    parameters);

  BOOST_CHECK(true);
}

// only one link and one vehicle with 2 disturbances
BOOST_AUTO_TEST_CASE(TestCase_Disturbance8)
{
  OnlyOneLinkWithDisturbancesParameters parameters = {{10, 1000, 1, 0, 0, {}},
                                                      {{
                                                         {5, artis::traffic::micro::core::DisturbanceTypes::values::DECELERATION, {5, 5, 5}, 1},
                                                       }},
                                                      {{
                                                         {1, 4.5,                                                                 3,         10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}},
                                                         {2, 4.5, 3, 10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}},
                                                         {3, 4.5, 3, 10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}}
                                                       },  {0, 2, 6}}};

  run_simulator<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>(
    parameters);

  BOOST_CHECK(true);
}

// only one link and one vehicle with 2 disturbances
BOOST_AUTO_TEST_CASE(TestCase_Disturbance9)
{
  OnlyOneLinkWithDisturbancesParameters parameters = {{10, 1000, 1, 0, 0, {}},
                                                      {{
                                                         {10, artis::traffic::micro::core::DisturbanceTypes::values::ACCELERATION, {5, 5, 5}, 1},
                                                         {60, artis::traffic::micro::core::DisturbanceTypes::values::DECELERATION, {5, 5, 5}, 1}}},
                                                      {{
                                                         {1,  4.5,                                                                 3,         10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}},
                                                         {2,  4.5,                                                                 3,         10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}},
                                                       },
                                                           {0, 3}}};

  run_simulator<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>(
    parameters);

  BOOST_CHECK(true);
}

// only one link and one vehicle with one disturbance DOWN - long disturbance at 0 speed and vehicle 2 at 4
//BOOST_AUTO_TEST_CASE(TestCase_Disturbance10)
//{
//  OnlyOneLinkWithDisturbancesParameters parameters = {{10, 1000, 30},
//                                                      {{
//                                                         {10, artis::traffic::micro::core::DisturbanceTypes::values::DECELERATION, {10, 30, 10}, 1}}},
//                                                      {{
//                                                         {1,  4.5,                                                                 3,          10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}},
//                                                         {2, 4.5, 3, 10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}}
//                                                       },  {0, 4}}};
//
//  run_simulator(parameters);
//
//  BOOST_CHECK(true);
//}

// only one link and one vehicle with one disturbance DOWN - long disturbance and vehicle 2 at 4 - more vehicles
BOOST_AUTO_TEST_CASE(TestCase_Disturbance11)
{
  OnlyOneLinkWithDisturbancesParameters parameters = {{10, 500, 1, 0, 0, {}},
                                                      {{
                                                         {10, artis::traffic::micro::core::DisturbanceTypes::values::DECELERATION, {5, 30, 5}, 0}}},
                                                      {{
                                                         {0,  4.5,                                                                 3,          10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}},
                                                         {1, 4.5, 3, 10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}},
                                                         {2, 4.5, 3, 10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}},
                                                         {3, 4.5, 3, 10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}},
                                                         {4, 4.5, 3, 10, 10, 1, 1, 1, {}, {artis::traffic::micro::core::VehicleData::State::RUNNING}}
                                                       },  {0, 4, 8, 12}}};

  run_simulator<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>(
    parameters);

  BOOST_CHECK(true);
}