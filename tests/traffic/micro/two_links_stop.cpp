/**
 * @file tests/two_links_stop.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/Output.hpp>
#include <artis-star/common/observer/TimedIterator.hpp>
#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

#include <artis-traffic/micro/core/Link.hpp>
#include <artis-traffic/micro/core/Junction.hpp>
#include <artis-traffic/micro/core/Disruptor.hpp>
#include <artis-traffic/micro/utils/End.hpp>

#include <artis-traffic/micro/core/vehicle-dynamics/TimeGippsVehicleDynamics.hpp>
#include <artis-traffic/micro/core/vehicle-dynamics/EventGippsVehicleDynamics.hpp>

#include "one_link_generator.hpp"
#include "MicroGraphManager.hpp"

#define BOOST_TEST_MODULE Link_TwoLinksStop_Tests

#include <boost/test/unit_test.hpp>

#include <chrono>

using namespace std::chrono;

/*************************************************
 * Tests
 *************************************************/

struct TwoLinksStopParameters {
  artis::traffic::micro::core::LinkParameters link_1_parameters;
  artis::traffic::micro::core::LinkParameters link_2_parameters;
  artis::traffic::micro::core::StopParameters stop_parameters;
  artis::traffic::micro::core::JunctionParameters junction_parameters;
  GeneratorParameters generator_parameters;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class TwoLinksStopGraphManager :
  public MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, TwoLinksStopParameters> {
public:
  enum submodels {
    GENERATOR, LINK_1, STOP, JUNCTION, LINK_2, END
  };

  TwoLinksStopGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                      const TwoLinksStopParameters &parameters,
                      const artis::common::NoParameters &graph_parameters)
    :
    MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, TwoLinksStopParameters>(
      coordinator, parameters, graph_parameters),
    _generator("generator", parameters.generator_parameters),
    _link_1("link_1", parameters.link_1_parameters),
    _stop("stop", parameters.stop_parameters),
    _junction("junction", parameters.junction_parameters),
    _link_2("link_2", parameters.link_2_parameters),
    _end("end", artis::common::NoParameters()) {
    this->add_child(GENERATOR, &_generator);
    this->add_child(LINK_1, &_link_1);
    this->add_child(STOP, &_stop);
    this->add_child(JUNCTION, &_junction);
    this->add_child(LINK_2, &_link_2);
    this->add_child(END, &_end);

      this->out({&_generator, Generator::outputs::OUT})
              >> this->in({&_link_1, artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::inputs::IN});
    this->connect_upstream_link_to_stop(_link_1, _stop, 0, 1);
    this->connect_stop_to_junction(_stop, _junction, 0);
    this->connect_downstream_link_to_junction(_link_2, _junction, 0, 0);
    this->connect_link_to_end(_link_2, _end);
  }

  ~TwoLinksStopGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime, Generator, GeneratorParameters> _generator;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Stop<artis::traffic::micro::core::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>, artis::traffic::micro::core::StopParameters>,
    artis::traffic::micro::core::StopParameters> _stop;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Junction<Vehicle>,
    artis::traffic::micro::core::JunctionParameters> _junction;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_2;
  artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::utils::End> _end;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class TwoLinksStopView : public artis::traffic::core::View {
public:
  TwoLinksStopView() {
//      selector("Link:vehicle_number",
//               {TwoLinksStopGraphManager::LINK,
//                artis::traffic::micro::core::Link::vars::VEHICLE_NUMBER});
    selector("Link_1:vehicle_positions",
             {TwoLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_1:vehicle_indexes",
             {TwoLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_1:vehicle_speed",
             {TwoLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_1:event_count",
             {TwoLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::EVENT_COUNT});
    selector("Link_1:event_frequency",
             {TwoLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::EVENT_FREQUENCY});
    selector("Link_2:vehicle_positions",
             {TwoLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_2:vehicle_indexes",
             {TwoLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_2:vehicle_speed",
             {TwoLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Stop:vehicle_index",
             {TwoLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::STOP,
              artis::traffic::micro::core::Stop<artis::traffic::micro::core::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>, artis::traffic::micro::core::StopParameters>::vars::VEHICLE_INDEX});
//        selector("Link:vehicle_state",
//                 {TwoLinksStopGraphManager::LINK,
//                  artis::traffic::micro::core::Link::vars::VEHICLE_STATE});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
void run_simulation(TwoLinksStopParameters &parameters, const std::string& view_name) {

  artis::common::context::Context<artis::common::DoubleTime> context(0, 40);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      TwoLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>,
      TwoLinksStopParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  double timestep = 0.1;
  rc.attachView(view_name, new TwoLinksStopView<Vehicle, VehicleEntry, VehicleState>());
  rc.switch_to_timed_observer(timestep);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  artis::common::observer::Output<artis::common::DoubleTime,
    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
    output(rc.observer());

  output(context.begin(), context.end(), {context.begin(), timestep});
}

// lower_speed_and_cant_fully_accelerate
BOOST_AUTO_TEST_CASE(TestCase_2LinkStop1)
{
  artis::traffic::micro::core::Vehicle vehicle1 = {0, 4.5, 3, 10, 10, 1, 1, 1, {},
                                                   {artis::traffic::micro::core::VehicleData::State::RUNNING}};

  artis::traffic::micro::core::Vehicle vehicle2 = {1, 4.5, 3, 10, 30, 1, 1, 1, {},
                                                   {artis::traffic::micro::core::VehicleData::State::RUNNING}};

  std::vector<double> times = {0, 3};
  std::vector<artis::traffic::micro::core::Vehicle> vehicles = {vehicle1, vehicle2};
  TwoLinksStopParameters parameters = {{10,       80,  1,   0,   0, {}},
                                       {10,       80,  1,   0,   0, {}},
                                       {{10}, 0, 0, {}},
                                       {1, 1},
                                      {vehicles, times}};

  run_simulation<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>(
    parameters, "Link_1_t");
  run_simulation<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>(
    parameters, "Link_1_e");

  BOOST_CHECK(true);
}

// same_speed_and_cant_fully_accelerate
BOOST_AUTO_TEST_CASE(TestCase_2LinkStop2)
{
  artis::traffic::micro::core::Vehicle vehicle1 = {0, 4.5, 3, 10, 10, 1, 1, 1, {},
                                                   {artis::traffic::micro::core::VehicleData::State::RUNNING}};

  artis::traffic::micro::core::Vehicle vehicle2 = {1, 4.5, 3, 10, 30, 1, 1, 1, {},
                                                   {artis::traffic::micro::core::VehicleData::State::RUNNING}};

  std::vector<double> times = {0, 1};
  std::vector<artis::traffic::micro::core::Vehicle> vehicles = {vehicle1, vehicle2};
  TwoLinksStopParameters parameters = {{10,       80,  1,   0,   0, {}},
                                       {10,       80,  1,   0,   0, {}},
                                       {{10}, 0, 0, {}},
                                       {1, 1},
                                       {vehicles, times}};

  run_simulation<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>(
    parameters, "Link_1_t");
  run_simulation<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>(
    parameters, "Link_1_e");

  BOOST_CHECK(true);
}

// 3 vehicles, v2 lower speed
BOOST_AUTO_TEST_CASE(TestCase_2LinkStop3)
{
  artis::traffic::micro::core::Vehicle vehicle1 = {0, 4.5, 3, 10, 10, 1, 1, 1, {},
                                                   {artis::traffic::micro::core::VehicleData::State::RUNNING}};

  artis::traffic::micro::core::Vehicle vehicle2 = {1, 4.5, 3, 10, 30, 1, 1, 1, {},
                                                   {artis::traffic::micro::core::VehicleData::State::RUNNING}};

  artis::traffic::micro::core::Vehicle vehicle3 = {2, 4.5, 3, 7, 30, 1, 1, 1, {},
                                                   {artis::traffic::micro::core::VehicleData::State::RUNNING}};

  std::vector<double> times = {0, 2, 3};
  std::vector<artis::traffic::micro::core::Vehicle> vehicles = {vehicle1, vehicle2, vehicle3};
  TwoLinksStopParameters parameters = {{10,       80,  1,   0,   0, {}},
                                       {10,       80,  1,   0,   0, {}},
                                       {{10}, 0, 0, {}},
                                       {1, 1},
                                       {vehicles, times}};

  run_simulation<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>(
    parameters, "Link_1_t");
  run_simulation<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>(
    parameters, "Link_1_e");

  BOOST_CHECK(true);
}

// 3 vehicles, v1 v2 speed 4
BOOST_AUTO_TEST_CASE(TestCase_2LinkStop4)
{
  artis::traffic::micro::core::Vehicle vehicle1 = {0, 4.5, 3, 10, 10, 1, 1, 1, {},
                                                   {artis::traffic::micro::core::VehicleData::State::RUNNING}};

  artis::traffic::micro::core::Vehicle vehicle2 = {1, 4.5, 3, 4, 30, 1, 1, 1, {},
                                                   {artis::traffic::micro::core::VehicleData::State::RUNNING}};

  artis::traffic::micro::core::Vehicle vehicle3 = {2, 4.5, 3, 4, 30, 1, 1, 1, {},
                                                   {artis::traffic::micro::core::VehicleData::State::RUNNING}};

  std::vector<double> times = {0, 1, 3};
  std::vector<artis::traffic::micro::core::Vehicle> vehicles = {vehicle1, vehicle2, vehicle3};
  TwoLinksStopParameters parameters = {{10,       80,  1,   0,   0, {}},
                                       {10,       80,  1,   0,   0, {}},
                                       {{10}, 0, 0, {}},
                                       {1, 1},
                                       {vehicles, times}};

  run_simulation<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>(
    parameters, "Link_1_t");
  run_simulation<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>(
    parameters, "Link_1_e");

  BOOST_CHECK(true);
}

// same_speed_and_cant_fully_accelerate
BOOST_AUTO_TEST_CASE(TestCase_2LinkStop5)
{
  artis::traffic::micro::core::Vehicle vehicle1 = {0, 4.5, 3, 10, 10, 1, 1, 1, {},
                                                   {artis::traffic::micro::core::VehicleData::State::RUNNING}};

  artis::traffic::micro::core::Vehicle vehicle2 = {1, 4.5, 3, 10, 30, 1, 1, 1, {},
                                                   {artis::traffic::micro::core::VehicleData::State::RUNNING}};

  std::vector<double> times = {0, 6};
  std::vector<artis::traffic::micro::core::Vehicle> vehicles = {vehicle1, vehicle2};
  TwoLinksStopParameters parameters = {{10,       160,  1,   0,   0, {}},
                                       {10,       160,  1,   0,   0, {}},
                                       {{10}, 0, 0, {}},
                                       {1, 1},
                                       {vehicles, times}};

  run_simulation<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>(
    parameters, "Link_1_t");
  run_simulation<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>(
    parameters, "Link_1_e");

  BOOST_CHECK(true);
}

// 3 vehicles, v1 v2 speed 4
BOOST_AUTO_TEST_CASE(TestCase_2LinkStop6)
{
  artis::traffic::micro::core::Vehicle vehicle1 = {0, 4.5, 3, 10, 10, 1, 1, 1, {},
                                                   {artis::traffic::micro::core::VehicleData::State::RUNNING}};

  artis::traffic::micro::core::Vehicle vehicle2 = {1, 4.5, 3, 4, 30, 1, 1, 1, {},
                                                   {artis::traffic::micro::core::VehicleData::State::RUNNING}};

  artis::traffic::micro::core::Vehicle vehicle3 = {2, 4.5, 3, 4, 30, 1, 1, 1, {},
                                                   {artis::traffic::micro::core::VehicleData::State::RUNNING}};

  artis::traffic::micro::core::Vehicle vehicle4 = {3, 4.5, 3, 4, 30, 1, 1, 1, {},
                                                   {artis::traffic::micro::core::VehicleData::State::RUNNING}};

  std::vector<double> times = {0, 1, 3, 6};
  std::vector<artis::traffic::micro::core::Vehicle> vehicles = {vehicle1, vehicle2, vehicle3, vehicle4};
  TwoLinksStopParameters parameters = {{10,       80,  1,   0,   0, {}},
                                       {10,       80,  1,   0,   0, {}},
                                       {{10}, 0, 0, {}},
                                       {1, 1},
                                       {vehicles, times}};

  run_simulation<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>(
    parameters, "Link_1_t");
  run_simulation<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>(
    parameters, "Link_1_e");

  BOOST_CHECK(true);
}