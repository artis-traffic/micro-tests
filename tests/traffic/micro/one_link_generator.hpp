/**
 * @file tests/one_link_generator.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_ONE_LINK_GENERATOR_HPP
#define ARTIS_TRAFFIC_ONE_LINK_GENERATOR_HPP

#include <artis-traffic/micro/utils/VehicleListGenerator.hpp>
#include <artis-traffic/micro/core/Vehicle.hpp>

using GeneratorParameters = artis::traffic::micro::utils::VehicleListGeneratorParameters<artis::traffic::micro::core::Vehicle>;

using Generator = artis::traffic::micro::utils::VehicleListGenerator<artis::traffic::micro::core::Vehicle>;

#endif //ARTIS_TRAFFIC_ONE_LINK_GENERATOR_HPP
