/**
 * @file tests/links_paths.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/Output.hpp>
#include <artis-star/common/observer/TimedIterator.hpp>

#include <artis-traffic/micro/core/vehicle-dynamics/TimeGippsVehicleDynamics.hpp>
#include <artis-traffic/micro/core/vehicle-dynamics/EventGippsVehicleDynamics.hpp>

#include "links_roundabout_def.hpp"

#define BOOST_TEST_MODULE Links_Roundabout_Tests

#include <boost/test/unit_test.hpp>

#include <chrono>

using namespace std::chrono;

/*************************************************
 * Tests
 *************************************************/
BOOST_AUTO_TEST_CASE(TestCase_SimplePathsOpenRoundabout)
{
  ThreeLinksWithRoundaboutParameters parameters = {{0,    10,    30., 10., 432453, {{0},{1}}},
                                                 {10,   500,    2, 0, 0, {}},
                                                 {10,    500,   1, 0, 0, {}},
                                                 {10,    500,    1, 0, 0, {}},
                                                 {0.5, 1, 1, 2}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 200);
  artis::common::RootCoordinator<
      artis::common::DoubleTime, artis::pdevs::Coordinator<
          artis::common::DoubleTime,
          ThreeLinksWithRoundaboutGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
          ThreeLinksWithRoundaboutParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  rc.attachView("Link_roundabout_1", new ThreeLink1WithRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_2", new ThreeLink2WithRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_3", new ThreeLink3WithRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_global", new ThreeLinkGlobalWithRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  artis::common::observer::Output<artis::common::DoubleTime,
      artis::common::observer::TimedIterator<artis::common::DoubleTime>>
      output(rc.observer());

  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

BOOST_AUTO_TEST_CASE(TestCase_SimplePathsOpenRoundabout2)
{
  ThreeLinksWithRoundaboutParameters parameters = {{0,    5,    10., 10., 432453, {{0},{1}}},
                                                   {10,   500,    2, 0, 0, {}},
                                                   {10,    500,    1, 0, 0, {}},
                                                   {10,    500,    1, 0, 0, {}},
                                                   {0.5, 1, 1, 2}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 1500);
  artis::common::RootCoordinator<
      artis::common::DoubleTime, artis::pdevs::Coordinator<
          artis::common::DoubleTime,
          ThreeLinksWithRoundaboutGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
          ThreeLinksWithRoundaboutParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  rc.attachView("Link_roundabout_1", new ThreeLink1WithRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_2", new ThreeLink2WithRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_3", new ThreeLink3WithRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_global", new ThreeLinkGlobalWithRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  artis::common::observer::Output<artis::common::DoubleTime,
      artis::common::observer::TimedIterator<artis::common::DoubleTime>>
      output(rc.observer());

  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

BOOST_AUTO_TEST_CASE(TestCase_2In2Out2VehiclesCloseRoundaboutCapacity1)
{
  FourLinksTwoInRoundaboutParameters parameters = {{0,    0,    100., 1., 432453, {{0},{1}}},
                                                 {1000,    0,    106., 1., 432453, {{0},{1}}},
                                                 {10,   500,   2, 1, 0, {}},
                                                 {10,    500,    1, 0, 0, {}},
                                                 {10,    500,    1, 0, 0, {}},
                                                 {10,    500,    2, 1, 0, {}},
                                                 {0.5, 1, 1, 2}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 250);
  artis::common::RootCoordinator<
      artis::common::DoubleTime, artis::pdevs::Coordinator<
          artis::common::DoubleTime,
          FourLinksTwoInRoundaboutGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
          FourLinksTwoInRoundaboutParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  rc.attachView("Link_roundabout_1", new FourLink1TwoInRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_2", new FourLink2TwoInRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_3", new FourLink3TwoInRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_4", new FourLink4TwoInRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_global", new FourLinkGlobalTwoInRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  artis::common::observer::Output<artis::common::DoubleTime,
      artis::common::observer::TimedIterator<artis::common::DoubleTime>>
      output(rc.observer());

  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

BOOST_AUTO_TEST_CASE(TestCase_2In2Out2VehiclesCloseRoundaboutCapacity2)
{
  FourLinksTwoInRoundaboutParameters parameters = {{0,    0,    100., 1., 432453, {{0},{1}}},
                                                   {1000,    0,    106., 1., 432453, {{0},{1}}},
                                                   {10,   500,    2, 1, 0, {}},
                                                   {10,    500,    1, 0, 0, {}},
                                                   {10,    500,    1, 0, 0, {}},
                                                   {10,    500,   2, 1, 0, {}},
                                                   {0.5, 2, 2, 2}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 350.1);
  artis::common::RootCoordinator<
      artis::common::DoubleTime, artis::pdevs::Coordinator<
          artis::common::DoubleTime,
          FourLinksTwoInRoundaboutGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
          FourLinksTwoInRoundaboutParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  rc.attachView("Link_roundabout_1", new FourLink1TwoInRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_2", new FourLink2TwoInRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_3", new FourLink3TwoInRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_4", new FourLink4TwoInRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_global", new FourLinkGlobalTwoInRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  artis::common::observer::Output<artis::common::DoubleTime,
      artis::common::observer::TimedIterator<artis::common::DoubleTime>>
      output(rc.observer());

  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}
BOOST_AUTO_TEST_CASE(TestCase_2In2Out2VehiclesClose2RoundaboutCapacity2)
{
  FourLinksTwoInRoundaboutParameters parameters = {{0,    0,    100., 1., 432453, {{0},{1}}},
                                                   {1000,    0,    101., 1., 432453, {{0},{1}}},
                                                   {10,   500,    2, 1, 0, {}},
                                                   {10,    500,    1, 0, 0, {}},
                                                   {10,    500,    1, 0, 0, {}},
                                                   {10,    500,    2, 1, 0, {}},
                                                   {0.5, 2, 2, 2}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 250);
  artis::common::RootCoordinator<
      artis::common::DoubleTime, artis::pdevs::Coordinator<
          artis::common::DoubleTime,
          FourLinksTwoInRoundaboutGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
          FourLinksTwoInRoundaboutParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  rc.attachView("Link_roundabout_1", new FourLink1TwoInRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_2", new FourLink2TwoInRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_3", new FourLink3TwoInRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_4", new FourLink4TwoInRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_global", new FourLinkGlobalTwoInRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  artis::common::observer::Output<artis::common::DoubleTime,
      artis::common::observer::TimedIterator<artis::common::DoubleTime>>
      output(rc.observer());

  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

BOOST_AUTO_TEST_CASE(TestCase_2In2Out2VehiclesClose3RoundaboutCapacity2)
{
  FourLinksTwoInRoundaboutParameters parameters = {{0,    0,    100., 1., 12345, {{0},{1}}},
                                                   {1000,    0,    101., 1., 65421, {{0},{1}}},
                                                   {10,   500,    2, 1, 0, {}},
                                                   {10,    500,    1, 0, 0, {}},
                                                   {10,    500,    1, 0, 0, {}},
                                                   {10,    500,    2, 1, 0, {}},
                                                   {0.5, 2, 2, 2}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 250);
  artis::common::RootCoordinator<
      artis::common::DoubleTime, artis::pdevs::Coordinator<
          artis::common::DoubleTime,
          FourLinksTwoInRoundaboutGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
          FourLinksTwoInRoundaboutParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  rc.attachView("Link_roundabout_1", new FourLink1TwoInRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_2", new FourLink2TwoInRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_3", new FourLink3TwoInRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_4", new FourLink4TwoInRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_roundabout_global", new FourLinkGlobalTwoInRoundaboutView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  artis::common::observer::Output<artis::common::DoubleTime,
      artis::common::observer::TimedIterator<artis::common::DoubleTime>>
      output(rc.observer());

  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}