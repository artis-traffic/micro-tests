/**
 * @file tests/crossroads_def.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_TESTS_CROSSROADS_DEF_HPP
#define ARTIS_TRAFFIC_TESTS_CROSSROADS_DEF_HPP

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/Output.hpp>
#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

#include <artis-traffic/micro/core/Node.hpp>
#include <artis-traffic/micro/coupled/NodeSwitch.hpp>
#include <artis-traffic/micro/utils/Counter.hpp>
#include <artis-traffic/micro/utils/Generator.hpp>

/************************************************************
 *                  Generator
 *                     |
 *                     V
 *                    Node
 *
 *  Generator -> Node      Node <- Generator
 *
 *                    Node
 *                      ^
 *                      |
 *                  Generator
 ************************************************************/

struct CrossroadsParameters
{
  artis::traffic::micro::utils::GeneratorParameters west_generator_parameters;
  artis::traffic::micro::utils::GeneratorParameters east_generator_parameters;
  artis::traffic::micro::utils::GeneratorParameters north_generator_parameters;
  artis::traffic::micro::utils::GeneratorParameters south_generator_parameters;
  artis::traffic::micro::core::NodeParameters west_node_parameters;
  artis::traffic::micro::core::NodeParameters east_node_parameters;
  artis::traffic::micro::core::NodeParameters north_node_parameters;
  artis::traffic::micro::core::NodeParameters south_node_parameters;
};

class CrossroadsGraphManager :
    public artis::pdevs::GraphManager<artis::common::DoubleTime, CrossroadsParameters>
{
public:
  enum submodels
  {
    WEST_GENERATOR, EAST_GENERATOR, NORTH_GENERATOR, SOUTH_GENERATOR,
    WEST_NODE, EAST_NODE, NORTH_NODE, SOUTH_NODE
  };

  CrossroadsGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                         const CrossroadsParameters &parameters,
                         const artis::common::NoParameters &graph_parameters)
      :
      artis::pdevs::GraphManager<artis::common::DoubleTime, CrossroadsParameters>(
          coordinator, parameters, graph_parameters),
      _west_generator("west_generator", parameters.west_generator_parameters),
      _east_generator("east_generator", parameters.east_generator_parameters),
      _north_generator("north_generator", parameters.north_generator_parameters),
      _south_generator("south_generator", parameters.south_generator_parameters),
      _west_node("west_node", parameters.west_node_parameters),
      _east_node("east_node", parameters.east_node_parameters),
      _north_node("north_node", parameters.north_node_parameters),
      _south_node("south_node", parameters.south_node_parameters)
  {
    add_child(WEST_GENERATOR, &_west_generator);
    add_child(EAST_GENERATOR, &_east_generator);
    add_child(NORTH_GENERATOR, &_north_generator);
    add_child(SOUTH_GENERATOR, &_south_generator);
    add_child(WEST_NODE, &_west_node);
    add_child(EAST_NODE, &_east_node);
    add_child(NORTH_NODE, &_north_node);
    add_child(SOUTH_NODE, &_south_node);

    // west
    out({&_west_generator, artis::traffic::micro::utils::Generator::outputs::OUT})
        >> in({&_west_node, artis::traffic::micro::core::Node::inputs::IN});
    out({&_west_node, artis::traffic::micro::core::Node::outputs::CLOSE})
        >> in({&_west_generator, artis::traffic::micro::utils::Generator::inputs::CLOSE});
    out({&_west_node, artis::traffic::micro::core::Node::outputs::OPEN})
        >> in({&_west_generator, artis::traffic::micro::utils::Generator::inputs::OPEN});

    // east
    out({&_east_generator, artis::traffic::micro::utils::Generator::outputs::OUT})
        >> in({&_east_node, artis::traffic::micro::core::Node::inputs::IN});
    out({&_east_node, artis::traffic::micro::core::Node::outputs::CLOSE})
        >> in({&_east_generator, artis::traffic::micro::utils::Generator::inputs::CLOSE});
    out({&_east_node, artis::traffic::micro::core::Node::outputs::OPEN})
        >> in({&_east_generator, artis::traffic::micro::utils::Generator::inputs::OPEN});

    // north
    out({&_north_generator, artis::traffic::micro::utils::Generator::outputs::OUT})
        >> in({&_north_node, artis::traffic::micro::core::Node::inputs::IN});
    out({&_north_node, artis::traffic::micro::core::Node::outputs::CLOSE})
        >> in({&_north_generator, artis::traffic::micro::utils::Generator::inputs::CLOSE});
    out({&_north_node, artis::traffic::micro::core::Node::outputs::OPEN})
        >> in({&_north_generator, artis::traffic::micro::utils::Generator::inputs::OPEN});

    // south
    out({&_south_generator, artis::traffic::micro::utils::Generator::outputs::OUT})
        >> in({&_south_node, artis::traffic::micro::core::Node::inputs::IN});
    out({&_south_node, artis::traffic::micro::core::Node::outputs::CLOSE})
        >> in({&_south_generator, artis::traffic::micro::utils::Generator::inputs::CLOSE});
    out({&_south_node, artis::traffic::micro::core::Node::outputs::OPEN})
        >> in({&_south_generator, artis::traffic::micro::utils::Generator::inputs::OPEN});
  }

  ~CrossroadsGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime,
  artis::traffic::micro::utils::Generator,
  artis::traffic::micro::utils::GeneratorParameters> _west_generator;
  artis::pdevs::Simulator<artis::common::DoubleTime,
  artis::traffic::micro::utils::Generator,
  artis::traffic::micro::utils::GeneratorParameters> _east_generator;
  artis::pdevs::Simulator<artis::common::DoubleTime,
  artis::traffic::micro::utils::Generator,
  artis::traffic::micro::utils::GeneratorParameters> _north_generator;
  artis::pdevs::Simulator<artis::common::DoubleTime,
  artis::traffic::micro::utils::Generator,
  artis::traffic::micro::utils::GeneratorParameters> _south_generator;
  artis::pdevs::Simulator<artis::common::DoubleTime,
  artis::traffic::micro::core::Node,
  artis::traffic::micro::core::NodeParameters> _west_node;
  artis::pdevs::Simulator<artis::common::DoubleTime,
  artis::traffic::micro::core::Node,
  artis::traffic::micro::core::NodeParameters> _east_node;
  artis::pdevs::Simulator<artis::common::DoubleTime,
  artis::traffic::micro::core::Node,
  artis::traffic::micro::core::NodeParameters> _north_node;
  artis::pdevs::Simulator<artis::common::DoubleTime,
  artis::traffic::micro::core::Node,
  artis::traffic::micro::core::NodeParameters> _south_node;
};

class CrossroadsView : public artis::traffic::core::View
{
public:
  CrossroadsView()
  {}
};

/************************************************************
 *                                |-> Counter
 *  Generator -> Node -> Switch --|-> Counter
 *                                |-> Counter
 ************************************************************/

struct NodeSwitchParameters
{
  artis::traffic::micro::utils::GeneratorParameters generator_parameters;
  artis::traffic::micro::coupled::NodeSwitchParameters node_switch_parameters;
};

class NodeSwitchGraphManager :
    public artis::pdevs::GraphManager<artis::common::DoubleTime, NodeSwitchParameters>
{
public:
  enum submodels
  {
    GENERATOR, NODE_SWITCH, COUNTER
  };

  NodeSwitchGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                         const NodeSwitchParameters &parameters,
                         const artis::common::NoParameters &graph_parameters)
      :
      artis::pdevs::GraphManager<artis::common::DoubleTime, NodeSwitchParameters>(
          coordinator, parameters, graph_parameters),
      _generator("generator", parameters.generator_parameters),
      _node_switch("node_switch", parameters.node_switch_parameters, graph_parameters)
  {
    add_child(GENERATOR, &_generator);
    add_child(NODE_SWITCH, &_node_switch);

    unsigned int index = 0;

    for (const std::string &output_name: parameters.node_switch_parameters.switch_parameters
        .outputs) {
      _counters.push_back(
          new artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::utils::Counter>(
              output_name, artis::common::NoParameters()));
      add_child(COUNTER + index, _counters.back());
      ++index;
    }

    out({&_generator, artis::traffic::micro::utils::Generator::outputs::OUT})
        >> in({&_node_switch, artis::traffic::micro::coupled::NodeSwitchGraphManager::inputs::IN});
    out({&_node_switch, artis::traffic::micro::coupled::NodeSwitchGraphManager::outputs::OPEN})
        >> in({&_generator, artis::traffic::micro::utils::Generator::inputs::OPEN});
    out({&_node_switch, artis::traffic::micro::coupled::NodeSwitchGraphManager::outputs::CLOSE})
        >> in({&_generator, artis::traffic::micro::utils::Generator::inputs::CLOSE});

    for (unsigned int i = 0;
         i < (unsigned int) parameters.node_switch_parameters.switch_parameters.outputs.size();
         ++i) {
      out({&_node_switch, artis::traffic::micro::coupled::NodeSwitchGraphManager::outputs::OUT + i})
          >> in({_counters[i], artis::traffic::micro::utils::Counter::inputs::IN});
    }
  }

  ~NodeSwitchGraphManager() override
  {
    std::for_each(_counters.begin(), _counters.end(),
                  std::default_delete<artis::pdevs::Simulator<artis::common::DoubleTime,
                  artis::traffic::micro::utils::Counter>>());
  }

private:
  artis::pdevs::Simulator<artis::common::DoubleTime,
  artis::traffic::micro::utils::Generator,
  artis::traffic::micro::utils::GeneratorParameters> _generator;
  artis::pdevs::Coordinator<artis::common::DoubleTime,
  artis::traffic::micro::coupled::NodeSwitchGraphManager,
  artis::traffic::micro::coupled::NodeSwitchParameters> _node_switch;
  std::vector<artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::utils::Counter> *>
  _counters;
};

class NodeSwitchView : public artis::traffic::core::View
{
public:
  NodeSwitchView()
  {
    selector("Counter_1:counter",
             {NodeSwitchGraphManager::COUNTER, artis::traffic::micro::utils::Counter::vars::COUNTER});
    selector("Counter_2:counter",
             {NodeSwitchGraphManager::COUNTER + 1,
              artis::traffic::micro::utils::Counter::vars::COUNTER});
    selector("Counter_3:counter",
             {NodeSwitchGraphManager::COUNTER + 2,
              artis::traffic::micro::utils::Counter::vars::COUNTER});
    selector("Generator:counter",
             {NodeSwitchGraphManager::GENERATOR,
              artis::traffic::micro::utils::Generator::vars::COUNTER});
  }
};

#endif