/**
 * @file tests/links_paths.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/Output.hpp>
#include <artis-star/common/observer/TimedIterator.hpp>

#include <artis-traffic/micro/core/Vehicle.hpp>

#include <artis-traffic/micro/core/vehicle-dynamics/TimeGippsVehicleDynamics.hpp>
#include <artis-traffic/micro/core/vehicle-dynamics/EventGippsVehicleDynamics.hpp>

#include "links_paths_def.hpp"

#define BOOST_TEST_MODULE Links_Paths_Tests

#include <boost/test/unit_test.hpp>

#include <chrono>

using namespace std::chrono;

/*************************************************
 * Tests
 *************************************************/
BOOST_AUTO_TEST_CASE(TestCase_SimplePathsOpenJunction)
{
  ThreeLinksWithJunctionParameters parameters = {{0,  10,  30., 10., 432453, {{0}, {1}}},
                                                 {10, 500, 2,   0,   0,      {}},
                                                 {10, 500, 1,   0,   0,      {}},
                                                 {10, 500, 1,   0,   0,      {}},
                                                 {1,  2}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      ThreeLinksWithJunctionGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
      ThreeLinksWithJunctionParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Link_junction_1", new ThreeLink1WithJunctionView);
//  rc.attachView("Link_junction_2", new ThreeLink2WithJunctionView);
//  rc.attachView("Link_junction_3", new ThreeLink3WithJunctionView);
//  rc.attachView("Link_junction_global", new ThreeLinkGlobalWithJunctionView);
//  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

//  artis::common::observer::Output<artis::common::DoubleTime,
//      artis::common::observer::TimedIterator<artis::common::DoubleTime>>
//      output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

BOOST_AUTO_TEST_CASE(TestCase_SimplePathsOpenJunction2)
{
  ThreeLinksWithJunctionParameters parameters = {{0,  50,  15., 10., 432453, {{0}, {1}}},
                                                 {10, 500, 2,   0,   0,      {}},
                                                 {10, 500, 1,   0,   0,      {}},
                                                 {10, 500, 1,   0,   0,      {}},
                                                 {1,  2}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      ThreeLinksWithJunctionGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
      ThreeLinksWithJunctionParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Link_junction_1", new ThreeLink1WithJunctionView);
//  rc.attachView("Link_junction_2", new ThreeLink2WithJunctionView);
//  rc.attachView("Link_junction_3", new ThreeLink3WithJunctionView);
//  rc.attachView("Link_junction_global", new ThreeLinkGlobalWithJunctionView);
//  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

//  artis::common::observer::Output<artis::common::DoubleTime,
//      artis::common::observer::TimedIterator<artis::common::DoubleTime>>
//      output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

//BOOST_AUTO_TEST_CASE(TestCase_SimplePathsDelayStart)
//{
//  ThreeLinksWithJunctionParameters parameters = {{0,    5,    15., 10., 432453, {{0},{1}}},
//                                             {10,   500,  30, 1},
//                                             {10,    500,  30, 1},
//                                             {10,    500,  30, 1},
//                                             {3600., 90., 0.5, 100., 1, 2}};
//  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
//  artis::common::RootCoordinator<
//      artis::common::DoubleTime, artis::pdevs::Coordinator<
//          artis::common::DoubleTime,
//          ThreeLinksWithJunctionGraphManager,
//          ThreeLinksWithJunctionParameters>
//  > rc(context, "root", parameters, artis::common::NoParameters());
//
////  rc.attachView("Link_junction_1", new ThreeLink1WithJunctionView);
////  rc.attachView("Link_junction_2", new ThreeLink2WithJunctionView);
////  rc.attachView("Link_junction_3", new ThreeLink3WithJunctionView);
////  rc.attachView("Link_junction_global", new ThreeLinkGlobalWithJunctionView);
////  rc.switch_to_timed_observer(0.1);
//
//  steady_clock::time_point t1 = steady_clock::now();
//
//  rc.run(context);
//
//  steady_clock::time_point t2 = steady_clock::now();
//
//  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);
//
//  std::cout << "Duration: " << time_span.count() << std::endl;
//
////  artis::common::observer::Output<artis::common::DoubleTime,
////      artis::common::observer::TimedIterator<artis::common::DoubleTime>>
////      output(rc.observer());
////
////  output(context.begin(), context.end(), {context.begin(), 0.1});
//
//  BOOST_CHECK(true);
//}
//
//BOOST_AUTO_TEST_CASE(TestCase_SimplePathsDelayStart2)
//{
//  ThreeLinksWithJunctionParameters parameters = {{0,    50,    15., 10., 432453, {{0},{1}}},
//                                             {10,   500,  30, 1},
//                                             {10,    500,  30, 1},
//                                             {10,    500,  30, 1},
//                                             {3600., 90., 0.5, 100., 1, 2}};
//  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
//  artis::common::RootCoordinator<
//      artis::common::DoubleTime, artis::pdevs::Coordinator<
//          artis::common::DoubleTime,
//          ThreeLinksWithJunctionGraphManager,
//          ThreeLinksWithJunctionParameters>
//  > rc(context, "root", parameters, artis::common::NoParameters());
//
////  rc.attachView("Link_junction_1", new ThreeLink1WithJunctionView);
////  rc.attachView("Link_junction_2", new ThreeLink2WithJunctionView);
////  rc.attachView("Link_junction_3", new ThreeLink3WithJunctionView);
////  rc.attachView("Link_junction_global", new ThreeLinkGlobalWithJunctionView);
////  rc.switch_to_timed_observer(0.1);
//
//  steady_clock::time_point t1 = steady_clock::now();
//
//  rc.run(context);
//
//  steady_clock::time_point t2 = steady_clock::now();
//
//  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);
//
//  std::cout << "Duration: " << time_span.count() << std::endl;
//
////  artis::common::observer::Output<artis::common::DoubleTime,
////      artis::common::observer::TimedIterator<artis::common::DoubleTime>>
////      output(rc.observer());
////
////  output(context.begin(), context.end(), {context.begin(), 0.1});
//
//  BOOST_CHECK(true);
//}

//BOOST_AUTO_TEST_CASE(TestCase_SimplePaths3)
//{
//  ThreeLinksWithJunctionParameters parameters = {{0,    5,    10., 8., 432453, {{0},{1}}},
//                                             {10,   500,  30, 1},
//                                             {10,    500,  30, 1},
//                                             {10,    500,  30, 1},
//                                             {180., 90., 0.5, 0., 1, 2}};
//  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
//  artis::common::RootCoordinator<
//      artis::common::DoubleTime, artis::pdevs::Coordinator<
//          artis::common::DoubleTime,
//          ThreeLinksWithJunctionGraphManager,
//          ThreeLinksWithJunctionParameters>
//  > rc(context, "root", parameters, artis::common::NoParameters());
//
////  rc.attachView("Link_junction_1", new ThreeLink1WithJunctionView);
////  rc.attachView("Link_junction_2", new ThreeLink2WithJunctionView);
////  rc.attachView("Link_junction_3", new ThreeLink3WithJunctionView);
////  rc.attachView("Link_junction_global", new ThreeLinkGlobalWithJunctionView);
////  rc.switch_to_timed_observer(0.1);
//
//  steady_clock::time_point t1 = steady_clock::now();
//
//  rc.run(context);
//
//  steady_clock::time_point t2 = steady_clock::now();
//
//  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);
//
//  std::cout << "Duration: " << time_span.count() << std::endl;
//
////  artis::common::observer::Output<artis::common::DoubleTime,
////      artis::common::observer::TimedIterator<artis::common::DoubleTime>>
////      output(rc.observer());
////
////  output(context.begin(), context.end(), {context.begin(), 0.1});
//
//  BOOST_CHECK(true);
//}
//
//BOOST_AUTO_TEST_CASE(TestCase_SimplePaths4)
//{
//  ThreeLinksWithJunctionParameters parameters = {{0,    5,    10., 8., 432453, {{0},{1}}},
//                                             {10,   500,  30, 1},
//                                             {10,    500,  30, 1},
//                                             {10,    500,  30, 1},
//                                             {90., 390., 0.5, 0., 1, 2}};
//  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
//  artis::common::RootCoordinator<
//      artis::common::DoubleTime, artis::pdevs::Coordinator<
//          artis::common::DoubleTime,
//          ThreeLinksWithJunctionGraphManager,
//          ThreeLinksWithJunctionParameters>
//  > rc(context, "root", parameters, artis::common::NoParameters());
//
////  rc.attachView("Link_junction_1", new ThreeLink1WithJunctionView);
////  rc.attachView("Link_junction_2", new ThreeLink2WithJunctionView);
////  rc.attachView("Link_junction_3", new ThreeLink3WithJunctionView);
////  rc.attachView("Link_junction_global", new ThreeLinkGlobalWithJunctionView);
////  rc.switch_to_timed_observer(0.1);
//
//  steady_clock::time_point t1 = steady_clock::now();
//
//  rc.run(context);
//
//  steady_clock::time_point t2 = steady_clock::now();
//
//  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);
//
//  std::cout << "Duration: " << time_span.count() << std::endl;
//
////  artis::common::observer::Output<artis::common::DoubleTime,
////      artis::common::observer::TimedIterator<artis::common::DoubleTime>>
////      output(rc.observer());
////
////  output(context.begin(), context.end(), {context.begin(), 0.1});
//
//  BOOST_CHECK(true);
//}

BOOST_AUTO_TEST_CASE(TestCase_2In2Out2VehiclesClose)
{
  FourLinksTwoInJunctionParameters parameters = {{0,    0,   100., 1., 432453, {{0}, {1}}},
                                                 {1000, 0,   106., 1., 432453, {{0}, {1}}},
                                                 {10,   500, 2,    1,  0,      {true}},
                                                 {10,   500, 1,    0,  0,      {}},
                                                 {10,   500, 1,    0,  0,      {}},
                                                 {10,   500, 2,    1,  0,      {false}},
                                                 {2,    2}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 250);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      FourLinksTwoInJunctionGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
      FourLinksTwoInJunctionParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  rc.attachView("Link_junction_1",
                new FourLink1TwoInJunctionView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_junction_2",
                new FourLink2TwoInJunctionView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_junction_3",
                new FourLink3TwoInJunctionView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_junction_4",
                new FourLink4TwoInJunctionView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_junction_global",
                new FourLinkGlobalTwoInJunctionView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  artis::common::observer::Output<artis::common::DoubleTime,
    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
    output(rc.observer());

  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

BOOST_AUTO_TEST_CASE(TestCase_2In2Out2VehiclesClose2)
{
  FourLinksTwoInJunctionParameters parameters = {{0,    0,   100., 1., 12345, {{0}}},
                                                 {1000, 0,   101., 1., 65421, {{0}}},
                                                 {10,   500, 2,    1,  0,     {true}},
                                                 {10,   500, 1,    0,  0,     {}},
                                                 {10,   500, 1,    0,  0,     {}},
                                                 {10,   500, 2,    1,  0,     {false}},
                                                 {2,    2}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 250);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      FourLinksTwoInJunctionGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
      FourLinksTwoInJunctionParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  rc.attachView("Link_junction_1",
                new FourLink1TwoInJunctionView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_junction_2",
                new FourLink2TwoInJunctionView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_junction_3",
                new FourLink3TwoInJunctionView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_junction_4",
                new FourLink4TwoInJunctionView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_junction_global",
                new FourLinkGlobalTwoInJunctionView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  artis::common::observer::Output<artis::common::DoubleTime,
    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
    output(rc.observer());

  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

BOOST_AUTO_TEST_CASE(TestCase_2In2Out2VehiclesSameDate)
{
  FourLinksTwoInJunctionParameters parameters = {{0,    0,   100., 1., 432453, {{0}}},
                                                 {1000, 0,   100., 1., 432453, {{0}}},
                                                 {10,   500, 2,    1,  0,      {true}},
                                                 {10,   500, 1,    0,  0,      {}},
                                                 {10,   500, 1,    0,  0,      {}},
                                                 {10,   500, 2,    1,  0,      {false}},
                                                 {2,    2}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 250);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      FourLinksTwoInJunctionGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
      FourLinksTwoInJunctionParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  rc.attachView("Link_junction_1",
                new FourLink1TwoInJunctionView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_junction_2",
                new FourLink2TwoInJunctionView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_junction_3",
                new FourLink3TwoInJunctionView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_junction_4",
                new FourLink4TwoInJunctionView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_junction_global",
                new FourLinkGlobalTwoInJunctionView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  artis::common::observer::Output<artis::common::DoubleTime,
    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
    output(rc.observer());

  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

BOOST_AUTO_TEST_CASE(TestCase_2In2OutOpenJunction)
{
  FourLinksTwoInJunctionParameters parameters = {{0,    10,  30., 10., 432453, {{0}, {1}}},
                                                 {1000, 10,  35., 11., 432453, {{0}, {1}}},
                                                 {10,   500, 2,   1,   0,      {true}},
                                                 {10,   500, 1,   0,   0,      {}},
                                                 {10,   500, 1,   0,   0,      {}},
                                                 {10,   500, 2,   1,   0,      {false}},
                                                 {2,    2}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      FourLinksTwoInJunctionGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
      FourLinksTwoInJunctionParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Link_junction_1", new FourLink1TwoInJunctionView);
//  rc.attachView("Link_junction_2", new FourLink2TwoInJunctionView);
//  rc.attachView("Link_junction_3", new FourLink3TwoInJunctionView);
//  rc.attachView("Link_junction_4", new FourLink4TwoInJunctionView);
//  rc.attachView("Link_junction_global", new FourLinkGlobalTwoInJunctionView);
//  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

//  artis::common::observer::Output<artis::common::DoubleTime,
//      artis::common::observer::TimedIterator<artis::common::DoubleTime>>
//      output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

BOOST_AUTO_TEST_CASE(TestCase_2In2OutOpenJunction2)
{
  FourLinksTwoInJunctionParameters parameters = {{0,    5,   15., 10., 432453, {{0}, {1}}},
                                                 {1000, 5,   19., 11., 432453, {{0}, {1}}},
                                                 {10,   500, 2,   1,   0,      {true}},
                                                 {10,   500, 1,   0,   0,      {}},
                                                 {10,   500, 1,   0,   0,      {}},
                                                 {10,   500, 2,   1,   0,      {false}},
                                                 {2,    2}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 2400);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      FourLinksTwoInJunctionGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
      FourLinksTwoInJunctionParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Link_junction_1", new FourLink1TwoInJunctionView);
//  rc.attachView("Link_junction_2", new FourLink2TwoInJunctionView);
//  rc.attachView("Link_junction_3", new FourLink3TwoInJunctionView);
//  rc.attachView("Link_junction_4", new FourLink4TwoInJunctionView);
//  rc.attachView("Link_junction_global", new FourLinkGlobalTwoInJunctionView);
//  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

//  artis::common::observer::Output<artis::common::DoubleTime,
//      artis::common::observer::TimedIterator<artis::common::DoubleTime>>
//      output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

BOOST_AUTO_TEST_CASE(TestCase_4LinksConcurrentStop1)
{
  FourLinksCuttingStopParameters parameters = {{0,    5,   15., 10., 432453, {{0}}},
                                               {1000, 7,   55., 11., 432453, {{0}}},
                                               {2000, 12,  30., 11., 432453, {{0}}},
                                               {10,   500, 1,   1,   1,      {true}},
                                               {10,   500, 1,   1,   1,      {false}},
                                               {10,   500, 1,   0,   0,      {}},
                                               {10,   500, 1,   0,   0,      {}},
                                               {3,    1},
                                               {{10}, 2,   0,   {}}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 1500);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      FourLinksCuttingStopGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
      FourLinksCuttingStopParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Link_junction_1", new FourLink1CuttingStopView);
//  rc.attachView("Link_junction_2", new FourLink2CuttingStopView);
//  rc.attachView("Link_junction_3", new FourLink3CuttingStopView);
//  rc.attachView("Link_junction_4", new FourLink4CuttingStopView);
//  rc.attachView("Link_junction_global", new FourLinkGlobalCuttingStopView);
  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

//  artis::common::observer::Output<artis::common::DoubleTime,
//      artis::common::observer::TimedIterator<artis::common::DoubleTime>>
//      output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

BOOST_AUTO_TEST_CASE(TestCase_8LinksStops1)
{
  EightLinksWithStopsParameters parameters = {{0,    6,   17., 10., 432453, {{0}, {1}, {2}}},
                                              {1000, 10,  55., 11., 432453, {{0}, {1}, {2}}},
                                              {2000, 17,  30., 11., 432453, {{0}, {1}, {2}}},
                                              {3000, 15,  60., 11., 432453, {{0}, {1}, {2}}},
                                              {10,   500, 4,   1,   2,      {true}},
                                              {10,   500, 1,   0,   0,      {}},
                                              {10,   500, 1,   0,   0,      {}},
                                              {10,   500, 4,   0,   0,      {}},
                                              {10,   500, 1,   0,   0,      {}},
                                              {10,   500, 4,   1,   2,      {false}},
                                              {10,   500, 4,   0,   0,      {}},
                                              {10,   500, 1,   0,   0,      {}},
                                              {4,    4},
                                              {{10}, 2,   0,   {}},
                                              {{10}, 2,   0,   {}}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 1500);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      EightLinksWithStopsGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
      EightLinksWithStopsParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Link_junction_global", new EightLinkGlobalWithStopsView);
//  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

//  artis::common::observer::Output<artis::common::DoubleTime,
//      artis::common::observer::TimedIterator<artis::common::DoubleTime>>
//      output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}

BOOST_AUTO_TEST_CASE(TestCase_4LinksConcurrentStop2)
{
  FourLinksCuttingStopParameters parameters = {{0,    5,   15., 10., 432453, {{0}}},
                                               {1000, 7,   18., 11., 432453, {{0}}},
                                               {2000, 12,  30., 11., 432453, {{0}}},
                                               {10,   500, 1,   1,   1,      {true}},
                                               {10,   500, 1,   1,   1,      {false}},
                                               {10,   500, 1,   0,   0,      {}},
                                               {10,   500, 1,   0,   0,      {}},
                                               {3,    1},
                                               {{10}, 2,   0,   {}}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 100);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      FourLinksCuttingStopGraphManager<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>,
      FourLinksCuttingStopParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  rc.attachView("Link_junction_1",
                new FourLink1CuttingStopView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_junction_2",
                new FourLink2CuttingStopView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_junction_3",
                new FourLink3CuttingStopView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_junction_4",
                new FourLink4CuttingStopView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.attachView("Link_junction_global",
                new FourLinkGlobalCuttingStopView<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>);
  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  artis::common::observer::Output<artis::common::DoubleTime,
    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
    output(rc.observer());

  output(context.begin(), context.end(), {context.begin(), 0.1});

  BOOST_CHECK(true);
}