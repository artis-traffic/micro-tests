/**
 * @file tests/collector.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/common/RootCoordinator.hpp>

#include "collector_def.hpp"

#include <chrono>
#include <iostream>

using namespace std::chrono;

/************************************************************
 *  Tests
 ************************************************************/

/*TEST_CASE("collector + 2 generators", "collector")
{
  CollectorParameters parameters = {{0, 5, 15., 8., 78364},
                                    {0, 5, 30., 8., 76352},
                                    {0.5}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
  artis::common::RootCoordinator<
      artis::common::DoubleTime, artis::pdevs::Coordinator<
          artis::common::DoubleTime,
          CollectorGraphManager,
          CollectorParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  artis::common::Trace<artis::common::DoubleTime>::trace().clear();

  rc.attachView("Counter", new CollectorView());
  rc.switch_to_timed_observer(1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  const CollectorView::Values &counter_values = rc.observer().view("Counter").get(
      "Counter:counter");
  const CollectorView::Values &generator_1_values = rc.observer().view("Counter").get(
      "Generator_1:counter");
  const CollectorView::Values &generator_2_values = rc.observer().view("Counter").get(
      "Generator_2:counter");
  unsigned int c_n, g_n_1, g_n_2;

  counter_values.back().second(c_n);
  generator_1_values.back().second(g_n_1);
  generator_2_values.back().second(g_n_2);

  REQUIRE(g_n_1 + g_n_2 == c_n);
}

TEST_CASE("3 links + inserter/collector", "collector")
{
  InserterParameters parameters = {{0, 5, 5., 8., 78364},
                                   {100000, 5, 30., 8., 76352},
                                   {200000, 5, 30., 8., 76652},
                                   {10, 500, 30, false},
                                   {10, 500, 30, true},
                                   {10, 500, 30, false},
                                   {10, 500, 30, true},
                                   {10, 500, 30, false},
                                   {10, 500, 30, true},
                                   {10, 500, 30, true},
                                   {0.1}, {200},
                                   {0.1}, {200}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
  artis::common::RootCoordinator<
      artis::common::DoubleTime, artis::pdevs::Coordinator<
          artis::common::DoubleTime,
          InserterGraphManager,
          InserterParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  artis::common::Trace<artis::common::DoubleTime>::trace().clear();

  rc.attachView("System", new InserterView());
  rc.switch_to_timed_observer(1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  const InserterView::Values &counter_1_values = rc.observer().view("System").get(
      "Counter_1:counter");
  const InserterView::Values &counter_2_values = rc.observer().view("System").get(
      "Counter_2:counter");
  const InserterView::Values &counter_3_values = rc.observer().view("System").get(
      "Counter_3:counter");
  const InserterView::Values &generator_1_values = rc.observer().view("System").get(
      "Generator_1:counter");
  const InserterView::Values &generator_2_values = rc.observer().view("System").get(
      "Generator_2:counter");
  const InserterView::Values &generator_3_values = rc.observer().view("System").get(
      "Generator_3:counter");
  const InserterView::Values &link_1_values = rc.observer().view("System").get(
      "Link_1:vehicle_number");
  const InserterView::Values &link_2_values = rc.observer().view("System").get(
      "Link_2:vehicle_number");
  const InserterView::Values &link_3_values = rc.observer().view("System").get(
      "Link_3:vehicle_number");
  const InserterView::Values &link_4_values = rc.observer().view("System").get(
      "Link_4:vehicle_number");
  const InserterView::Values &link_5_values = rc.observer().view("System").get(
      "Link_5:vehicle_number");
  const InserterView::Values &link_6_values = rc.observer().view("System").get(
      "Link_6:vehicle_number");
  const InserterView::Values &link_7_values = rc.observer().view("System").get(
      "Link_7:vehicle_number");
  const InserterView::Values &collector_1_values = rc.observer().view("System").get(
      "Collector_1:counter");
  const InserterView::Values &inserter_1_values = rc.observer().view("System").get(
      "Inserter_1:counter");
  const InserterView::Values &collector_2_values = rc.observer().view("System").get(
      "Collector_2:counter");
  const InserterView::Values &inserter_2_values = rc.observer().view("System").get(
      "Inserter_2:counter");
  const InserterView::Values &switch_1_values = rc.observer().view("System").get(
      "Switch_1:counter");
  const InserterView::Values &switch_2_values = rc.observer().view("System").get(
      "Switch_2:counter");

  for (size_t i = 0; i < counter_1_values.size(); ++i) {
    unsigned int c_n_1, c_n_2, c_n_3, g_n_1, g_n_2, g_n_3, l_n_1, l_n_2, l_n_3, l_n_4, l_n_5,
        l_n_6, l_n_7, col_n_1, i_n_1, col_n_2, i_n_2, sw_n_1, sw_n_2;

    counter_1_values.at(i).second(c_n_1);
    counter_2_values.at(i).second(c_n_2);
    counter_3_values.at(i).second(c_n_3);
    generator_1_values.at(i).second(g_n_1);
    generator_2_values.at(i).second(g_n_2);
    generator_3_values.at(i).second(g_n_3);
    link_1_values.at(i).second(l_n_1);
    link_2_values.at(i).second(l_n_2);
    link_3_values.at(i).second(l_n_3);
    link_4_values.at(i).second(l_n_4);
    link_5_values.at(i).second(l_n_5);
    link_6_values.at(i).second(l_n_6);
    link_7_values.at(i).second(l_n_7);
    collector_1_values.at(i).second(col_n_1);
    inserter_1_values.at(i).second(i_n_1);
    collector_2_values.at(i).second(col_n_2);
    inserter_2_values.at(i).second(i_n_2);
    switch_1_values.at(i).second(sw_n_1);
    switch_2_values.at(i).second(sw_n_2);

    REQUIRE(g_n_1 + (g_n_2 - 100000) + (g_n_3 - 200000)
                == c_n_1 + c_n_2 + c_n_3 + l_n_1 + l_n_2 + l_n_3 + l_n_4 + l_n_5 + l_n_6 + l_n_7
                    + col_n_1 + i_n_1 + col_n_2 + i_n_2 + sw_n_1 + sw_n_2);
  }
}*/