/**
 * @file tests/four_links_stop.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/Output.hpp>
#include <artis-star/common/observer/TimedIterator.hpp>
#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

#include <artis-traffic/micro/core/Link.hpp>
#include <artis-traffic/micro/core/Junction.hpp>
#include <artis-traffic/micro/core/Disruptor.hpp>
#include <artis-traffic/micro/utils/End.hpp>

#include <artis-traffic/micro/core/vehicle-dynamics/TimeGippsVehicleDynamics.hpp>
#include <artis-traffic/micro/core/vehicle-dynamics/EventGippsVehicleDynamics.hpp>

#include "one_link_generator.hpp"
#include "MicroGraphManager.hpp"

#define BOOST_TEST_MODULE Link_FourLinksStop_Tests

#include <boost/test/unit_test.hpp>

#include <chrono>

using namespace std::chrono;

/*************************************************
 * Tests
 *************************************************/

struct FourLinksStopParameters {
  GeneratorParameters generator_parameters;
  GeneratorParameters generator_2_parameters;
  GeneratorParameters generator_3_parameters;
  artis::traffic::micro::core::LinkParameters link_1_parameters;
  artis::traffic::micro::core::LinkParameters link_2_parameters;
  artis::traffic::micro::core::LinkParameters link_3_parameters;
  artis::traffic::micro::core::LinkParameters link_4_parameters;
  artis::traffic::micro::core::JunctionParameters junction_parameters;
  artis::traffic::micro::core::StopParameters stop_parameters;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class FourLinksStopGraphManager :
  public MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, FourLinksStopParameters> {
public:
  enum submodels {
    GENERATOR, GENERATOR_2, GENERATOR_3, LINK_1, LINK_2, LINK_3, LINK_4, JUNCTION, STOP, END, END_2
  };

  FourLinksStopGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                            const FourLinksStopParameters &parameters,
                            const artis::common::NoParameters &graph_parameters)
    :
    MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, FourLinksStopParameters>(
      coordinator, parameters, graph_parameters),
    _generator("generator", parameters.generator_parameters),
    _generator_2("generator2", parameters.generator_2_parameters),
    _generator_3("generator3", parameters.generator_3_parameters),
    _link_1("L1", parameters.link_1_parameters),
    _link_2("L2", parameters.link_2_parameters),
    _link_3("L3", parameters.link_3_parameters),
    _link_4("L4", parameters.link_4_parameters),
    _junction("junction", parameters.junction_parameters),
    _stop("stop", parameters.stop_parameters),
    _end("end", artis::common::NoParameters()),
    _end_2("end_2", artis::common::NoParameters()) {
    this->add_child(GENERATOR, &_generator);
    this->add_child(GENERATOR_2, &_generator_2);
    this->add_child(GENERATOR_3, &_generator_3);
    this->add_child(LINK_1, &_link_1);
    this->add_child(LINK_2, &_link_2);
    this->add_child(LINK_3, &_link_3);
    this->add_child(LINK_4, &_link_4);
    this->add_child(JUNCTION, &_junction);
    this->add_child(STOP, &_stop);
    this->add_child(END, &_end);
    this->add_child(END_2, &_end_2);

    this->out({&_generator, Generator::outputs::OUT})
      >> this->in({&_link_1, artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::inputs::IN});
    this->out({&_generator_2, Generator::outputs::OUT})
      >> this->in({&_link_2, artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::inputs::IN});
    this->out({&_generator_3, Generator::outputs::OUT})
      >> this->in({&_link_3, artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::inputs::IN});
    this->connect_link_to_end(_link_4, _end);
    this->connect_upstream_link_to_junction(_link_1, _junction, 0);
    this->connect_upstream_link_to_junction(_link_2, _junction, 1);
    this->connect_upstream_link_to_stop(_link_3, _stop, 0, 1);
    this->connect_stop_to_junction(_stop, _junction, 2);
    this->connect_concurrent_links(_link_1, _link_2, 0, 0);
    this->connect_concurrent_link_to_stop(_link_1, _stop, 0, 1);
    this->connect_concurrent_link_to_stop(_link_2, _stop, 1, 1);
    this->connect_downstream_link_to_junction(_link_4, _junction, 0, 0);
  }

  ~FourLinksStopGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime, Generator, GeneratorParameters> _generator;
  artis::pdevs::Simulator<artis::common::DoubleTime, Generator, GeneratorParameters> _generator_2;
  artis::pdevs::Simulator<artis::common::DoubleTime, Generator, GeneratorParameters> _generator_3;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_3;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_4;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Junction<Vehicle>,
    artis::traffic::micro::core::JunctionParameters> _junction;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Stop<artis::traffic::micro::core::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>, artis::traffic::micro::core::StopParameters>,
    artis::traffic::micro::core::StopParameters> _stop;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::End> _end;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::End> _end_2;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class FourLinkGlobalStopView : public artis::traffic::core::View {
public:
  FourLinkGlobalStopView() {
    selector("Link_1:vehicle_number",
             {FourLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_1:vehicle_positions",
             {FourLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_1:vehicle_speed",
             {FourLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_1:vehicle_indexes",
             {FourLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_2:vehicle_number",
             {FourLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_2:vehicle_positions",
             {FourLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_2:vehicle_speed",
             {FourLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_2:vehicle_indexes",
             {FourLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_3:vehicle_number",
             {FourLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_3:vehicle_positions",
             {FourLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_3:vehicle_speed",
             {FourLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_3:vehicle_indexes",
             {FourLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_4:vehicle_number",
             {FourLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_4:vehicle_positions",
             {FourLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_4:vehicle_speed",
             {FourLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_4:vehicle_indexes",
             {FourLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Stop:vehicle_index",
             {FourLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>::STOP,
              artis::traffic::micro::core::Stop<artis::traffic::micro::core::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>, artis::traffic::micro::core::StopParameters>::vars::VEHICLE_INDEX});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
void run_simulation(FourLinksStopParameters &parameters) {

  artis::common::context::Context<artis::common::DoubleTime> context(0, 40);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      FourLinksStopGraphManager<Vehicle, VehicleEntry, VehicleState>,
      FourLinksStopParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  rc.attachView("Link_global_stop", new FourLinkGlobalStopView<Vehicle, VehicleEntry, VehicleState>);
  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  artis::common::observer::Output<artis::common::DoubleTime,
    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
    output(rc.observer());

  output(context.begin(), context.end(), {context.begin(), 0.1});
}

BOOST_AUTO_TEST_CASE(TestCase_4LinkStop1)
{
  artis::traffic::micro::core::Vehicle vehicle1 = {0, 4.5, 3, 10, 10, 1, 1, 1, {},
                                                   {artis::traffic::micro::core::VehicleData::State::RUNNING}};

  artis::traffic::micro::core::Vehicle vehicle2 = {100, 4.5, 3, 10, 10, 1, 1, 1, {},
                                                   {artis::traffic::micro::core::VehicleData::State::RUNNING}};

  artis::traffic::micro::core::Vehicle vehicle2_2 = {101, 4.5, 3, 10, 10, 1, 1, 1, {},
                                                     {artis::traffic::micro::core::VehicleData::State::RUNNING}};

  artis::traffic::micro::core::Vehicle vehicle3 = {200, 4.5, 3, 10, 10, 1, 1, 1, {},
                                                   {artis::traffic::micro::core::VehicleData::State::RUNNING}};

  std::vector<double> times = {0, 0, 3, 2};
  std::vector<artis::traffic::micro::core::Vehicle> vehicles = {vehicle1, vehicle2, vehicle3};
  FourLinksStopParameters parameters = {{{vehicle1},             {times[0]}},
                                        {{vehicle2, vehicle2_2}, {times[1], times[2]}},
                                        {{vehicle3},             {times[3]}},
                                        {10,                     80, 1, 1, 1, {true}},
                                        {10,                     80, 1, 1, 1, {false}},
                                        {10,                     80, 1, 0, 0, {}},
                                        {10,                     80, 1, 0, 0, {}},
                                        {3,                      1},
                                        {{10},                   2,  0, {}}};
  run_simulation<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::TimeGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::TimeGippsVehicleState>(
    parameters);

  BOOST_CHECK(true);
}