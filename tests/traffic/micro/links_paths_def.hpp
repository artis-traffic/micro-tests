/**
 * @file tests/links_paths_def.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_TESTS_LINKS_PATHS_DEF_HPP
#define ARTIS_TRAFFIC_TESTS_LINKS_PATHS_DEF_HPP

#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

#include <artis-traffic/micro/core/Link.hpp>
#include <artis-traffic/micro/core/Junction.hpp>
#include <artis-traffic/micro/core/Stop.hpp>
#include <artis-traffic/micro/core/Disruptor.hpp>
#include <artis-traffic/micro/utils/End.hpp>
#include <artis-traffic/micro/utils/Generator.hpp>

#include "MicroGraphManager.hpp"
#include <artis-traffic/micro/utils/VehicleFactory.hpp>

/************************************************************
 *  Link (L1) -> Junction (junction) -> Link (L2) -> End (end)
 *                   |
 *                   v
 *                Link (L3)
 ************************************************************/

struct ThreeLinksWithJunctionParameters {
  artis::traffic::micro::utils::GeneratorParameters generator_parameters;
  artis::traffic::micro::core::LinkParameters link_1_parameters;
  artis::traffic::micro::core::LinkParameters link_2_parameters;
  artis::traffic::micro::core::LinkParameters link_3_parameters;
  artis::traffic::micro::core::JunctionParameters junction_parameters;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class ThreeLinksWithJunctionGraphManager :
  public MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, ThreeLinksWithJunctionParameters> {
public:
  enum sub_models {
    GENERATOR, LINK_1, LINK_2, LINK_3, JUNCTION, END, END_2
  };

  ThreeLinksWithJunctionGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                                     const ThreeLinksWithJunctionParameters &parameters,
                                     const artis::common::NoParameters &graph_parameters)
    :
    MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, ThreeLinksWithJunctionParameters>(
      coordinator, parameters, graph_parameters),
    _generator("generator", parameters.generator_parameters),
    _link_1("L1", parameters.link_1_parameters),
    _link_2("L2", parameters.link_2_parameters),
    _link_3("L3", parameters.link_3_parameters),
    _junction("junction", parameters.junction_parameters),
    _end("end", artis::common::NoParameters()),
    _end_2("end_2", artis::common::NoParameters()) {
    this->add_child(GENERATOR, &_generator);
    this->add_child(LINK_1, &_link_1);
    this->add_child(LINK_2, &_link_2);
    this->add_child(LINK_3, &_link_3);
    this->add_child(JUNCTION, &_junction);
    this->add_child(END, &_end);
    this->add_child(END_2, &_end_2);

    this->connect_generator_to_link(_generator, _link_1);
    this->connect_link_to_end(_link_2, _end);
    this->connect_link_to_end(_link_3, _end_2);
    this->connect_upstream_link_to_junction(_link_1, _junction, 0);
    this->connect_downstream_link_to_junction(_link_2, _junction, 0, 0);
    this->connect_downstream_link_to_junction(_link_3, _junction, 1, 1);
  }

  ~ThreeLinksWithJunctionGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::Generator<VehicleFactory>,
    artis::traffic::micro::utils::GeneratorParameters> _generator;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_3;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Junction<Vehicle>,
    artis::traffic::micro::core::JunctionParameters> _junction;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::End> _end;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::End> _end_2;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class ThreeLink1WithJunctionView : public artis::traffic::core::View {
public:
  ThreeLink1WithJunctionView() {
    selector("Link_1:vehicle_number",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_1:vehicle_positions",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_1:vehicle_speed",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_1:vehicle_indexes",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class ThreeLink2WithJunctionView : public artis::traffic::core::View {
public:
  ThreeLink2WithJunctionView() {
    selector("Link_2:vehicle_number",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_2:vehicle_positions",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_2:vehicle_speed",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_2:vehicle_indexes",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class ThreeLink3WithJunctionView : public artis::traffic::core::View {
public:
  ThreeLink3WithJunctionView() {
    selector("Link_3:vehicle_number",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_3:vehicle_positions",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_3:vehicle_speed",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_3:vehicle_indexes",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class ThreeLinkGlobalWithJunctionView : public artis::traffic::core::View {
public:
  ThreeLinkGlobalWithJunctionView() {
    selector("Link_1:vehicle_number",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_1:vehicle_positions",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_1:vehicle_speed",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_1:vehicle_indexes",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_2:vehicle_number",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_2:vehicle_positions",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_2:vehicle_speed",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_2:vehicle_indexes",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_3:vehicle_number",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_3:vehicle_positions",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_3:vehicle_speed",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_3:vehicle_indexes",
             {ThreeLinksWithJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

/************************************************************
 *                 Link (L4)
 *                   |
 *                   v
 *  Link (L1) -> Junction (junction) -> Link (L2) -> End (end)
 *                   |
 *                   v
 *                Link (L3)
 ************************************************************/

struct FourLinksTwoInJunctionParameters {
  artis::traffic::micro::utils::GeneratorParameters generator_parameters;
  artis::traffic::micro::utils::GeneratorParameters generator_2_parameters;
  artis::traffic::micro::core::LinkParameters link_1_parameters;
  artis::traffic::micro::core::LinkParameters link_2_parameters;
  artis::traffic::micro::core::LinkParameters link_3_parameters;
  artis::traffic::micro::core::LinkParameters link_4_parameters;
  artis::traffic::micro::core::JunctionParameters junction_parameters;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class FourLinksTwoInJunctionGraphManager :
  public MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, FourLinksTwoInJunctionParameters> {
public:
  enum sub_models {
    GENERATOR, GENERATOR_2, LINK_1, LINK_2, LINK_3, LINK_4, JUNCTION, END, END_2
  };

  FourLinksTwoInJunctionGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                                     const FourLinksTwoInJunctionParameters &parameters,
                                     const artis::common::NoParameters &graph_parameters)
    :
    MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, FourLinksTwoInJunctionParameters>(
      coordinator, parameters, graph_parameters),
    _generator("generator", parameters.generator_parameters),
    _generator_2("generator2", parameters.generator_2_parameters),
    _link_1("L1", parameters.link_1_parameters),
    _link_2("L2", parameters.link_2_parameters),
    _link_3("L3", parameters.link_3_parameters),
    _link_4("L4", parameters.link_4_parameters),
    _junction("junction", parameters.junction_parameters),
    _end("end", artis::common::NoParameters()),
    _end_2("end_2", artis::common::NoParameters()) {
    this->add_child(GENERATOR, &_generator);
    this->add_child(GENERATOR_2, &_generator_2);
    this->add_child(LINK_1, &_link_1);
    this->add_child(LINK_2, &_link_2);
    this->add_child(LINK_3, &_link_3);
    this->add_child(LINK_4, &_link_4);
    this->add_child(JUNCTION, &_junction);
    this->add_child(END, &_end);
    this->add_child(END_2, &_end_2);


    this->connect_generator_to_link(_generator, _link_1);
    this->connect_generator_to_link(_generator_2, _link_4);
    this->connect_link_to_end(_link_2, _end);
    this->connect_link_to_end(_link_3, _end_2);
    this->connect_concurrent_links(_link_1, _link_4);
    this->connect_upstream_link_to_junction(_link_1, _junction, 0);
    this->connect_upstream_link_to_junction(_link_4, _junction, 1);
    this->connect_downstream_link_to_junction(_link_2, _junction, 0, 0);
    this->connect_downstream_link_to_junction(_link_3, _junction, 1, 1);
  }

  ~FourLinksTwoInJunctionGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::Generator<VehicleFactory>,
    artis::traffic::micro::utils::GeneratorParameters> _generator;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::Generator<VehicleFactory>,
    artis::traffic::micro::utils::GeneratorParameters> _generator_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_3;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_4;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Junction<Vehicle>,
    artis::traffic::micro::core::JunctionParameters> _junction;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::End> _end;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::End> _end_2;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class FourLink1TwoInJunctionView : public artis::traffic::core::View {
public:
  FourLink1TwoInJunctionView() {
    selector("Link_1:vehicle_number",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_1:vehicle_positions",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_1:vehicle_speed",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_1:vehicle_indexes",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class FourLink2TwoInJunctionView : public artis::traffic::core::View {
public:
  FourLink2TwoInJunctionView() {
    selector("Link_2:vehicle_number",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_2:vehicle_positions",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_2:vehicle_speed",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_2:vehicle_indexes",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class FourLink3TwoInJunctionView : public artis::traffic::core::View {
public:
  FourLink3TwoInJunctionView() {
    selector("Link_3:vehicle_number",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_3:vehicle_positions",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_3:vehicle_speed",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_3:vehicle_indexes",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class FourLink4TwoInJunctionView : public artis::traffic::core::View {
public:
  FourLink4TwoInJunctionView() {
    selector("Link_4:vehicle_number",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_4:vehicle_positions",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_4:vehicle_speed",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_4:vehicle_indexes",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class FourLinkGlobalTwoInJunctionView : public artis::traffic::core::View {
public:
  FourLinkGlobalTwoInJunctionView() {
    selector("Link_1:vehicle_number",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_1:vehicle_positions",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_1:vehicle_speed",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_1:vehicle_indexes",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_2:vehicle_number",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_2:vehicle_positions",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_2:vehicle_speed",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_2:vehicle_indexes",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_3:vehicle_number",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_3:vehicle_positions",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_3:vehicle_speed",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_3:vehicle_indexes",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_4:vehicle_number",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_4:vehicle_positions",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_4:vehicle_speed",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_4:vehicle_indexes",
             {FourLinksTwoInJunctionGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

/************************************************************
 *                   End (end)
 *                   ^
 *                   |
 *                 Link (L4)
 *                   |
 *                   ^
 *  Link (L1) -> Junction (junction) <- Link (L2)
 *                   ^
 *                   |
 *                   Stop (stop)
 *                   ^
 *                   |
 *                Link (L3)
 ************************************************************/

struct FourLinksCuttingStopParameters {
  artis::traffic::micro::utils::GeneratorParameters generator_parameters;
  artis::traffic::micro::utils::GeneratorParameters generator_2_parameters;
  artis::traffic::micro::utils::GeneratorParameters generator_3_parameters;
  artis::traffic::micro::core::LinkParameters link_1_parameters;
  artis::traffic::micro::core::LinkParameters link_2_parameters;
  artis::traffic::micro::core::LinkParameters link_3_parameters;
  artis::traffic::micro::core::LinkParameters link_4_parameters;
  artis::traffic::micro::core::JunctionParameters junction_parameters;
  artis::traffic::micro::core::StopParameters stop_parameters;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class FourLinksCuttingStopGraphManager :
  public MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, FourLinksCuttingStopParameters> {
public:
  enum sub_models {
    GENERATOR, GENERATOR_2, GENERATOR_3, LINK_1, LINK_2, LINK_3, LINK_4, JUNCTION, STOP, END, END_2
  };

  FourLinksCuttingStopGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                                   const FourLinksCuttingStopParameters &parameters,
                                   const artis::common::NoParameters &graph_parameters)
    :
    MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, FourLinksCuttingStopParameters>(
      coordinator, parameters, graph_parameters),
    _generator("generator", parameters.generator_parameters),
    _generator_2("generator2", parameters.generator_2_parameters),
    _generator_3("generator3", parameters.generator_3_parameters),
    _link_1("L1", parameters.link_1_parameters),
    _link_2("L2", parameters.link_2_parameters),
    _link_3("L3", parameters.link_3_parameters),
    _link_4("L4", parameters.link_4_parameters),
    _junction("junction", parameters.junction_parameters),
    _stop("stop", parameters.stop_parameters),
    _end("end", artis::common::NoParameters()),
    _end_2("end_2", artis::common::NoParameters()) {
    this->add_child(GENERATOR, &_generator);
    this->add_child(GENERATOR_2, &_generator_2);
    this->add_child(GENERATOR_3, &_generator_3);
    this->add_child(LINK_1, &_link_1);
    this->add_child(LINK_2, &_link_2);
    this->add_child(LINK_3, &_link_3);
    this->add_child(LINK_4, &_link_4);
    this->add_child(JUNCTION, &_junction);
    this->add_child(STOP, &_stop);
    this->add_child(END, &_end);
    this->add_child(END_2, &_end_2);


    this->connect_generator_to_link(_generator, _link_1);
    this->connect_generator_to_link(_generator_2, _link_2);
    this->connect_generator_to_link(_generator_3, _link_3);
    this->connect_link_to_end(_link_4, _end);
    this->connect_upstream_link_to_junction(_link_1, _junction, 0);
    this->connect_upstream_link_to_junction(_link_2, _junction, 1);
    this->connect_upstream_link_to_stop(_link_3, _stop, 0, 1);
    this->connect_stop_to_junction(_stop, _junction, 2);
    this->connect_concurrent_links(_link_1, _link_2, 0, 0);
    this->connect_concurrent_link_to_stop(_link_1, _stop, 0, 1);
    this->connect_concurrent_link_to_stop(_link_2, _stop, 1, 1);
    this->connect_downstream_link_to_junction(_link_4, _junction, 0, 0);
  }

  ~FourLinksCuttingStopGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::Generator<VehicleFactory>,
    artis::traffic::micro::utils::GeneratorParameters> _generator;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::Generator<VehicleFactory>,
    artis::traffic::micro::utils::GeneratorParameters> _generator_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::Generator<VehicleFactory>,
    artis::traffic::micro::utils::GeneratorParameters> _generator_3;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_3;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_4;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Junction<Vehicle>,
    artis::traffic::micro::core::JunctionParameters> _junction;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Stop<artis::traffic::micro::core::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>, artis::traffic::micro::core::StopParameters>,
    artis::traffic::micro::core::StopParameters> _stop;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::End> _end;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::End> _end_2;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class FourLink1CuttingStopView : public artis::traffic::core::View {
public:
  FourLink1CuttingStopView() {
    selector("Link_1:vehicle_number",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_1:vehicle_positions",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_1:vehicle_speed",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_1:vehicle_indexes",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class FourLink2CuttingStopView : public artis::traffic::core::View {
public:
  FourLink2CuttingStopView() {
    selector("Link_2:vehicle_number",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_2:vehicle_positions",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_2:vehicle_speed",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_2:vehicle_indexes",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class FourLink3CuttingStopView : public artis::traffic::core::View {
public:
  FourLink3CuttingStopView() {
    selector("Link_3:vehicle_number",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_3:vehicle_positions",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_3:vehicle_speed",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_3:vehicle_indexes",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class FourLink4CuttingStopView : public artis::traffic::core::View {
public:
  FourLink4CuttingStopView() {
    selector("Link_4:vehicle_number",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_4:vehicle_positions",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_4:vehicle_speed",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_4:vehicle_indexes",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class FourLinkGlobalCuttingStopView : public artis::traffic::core::View {
public:
  FourLinkGlobalCuttingStopView() {
    selector("Link_1:vehicle_number",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_1:vehicle_positions",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_1:vehicle_speed",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_1:vehicle_indexes",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_2:vehicle_number",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_2:vehicle_positions",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_2:vehicle_speed",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_2:vehicle_indexes",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_3:vehicle_number",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_3:vehicle_positions",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_3:vehicle_speed",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_3:vehicle_indexes",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_4:vehicle_number",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_4:vehicle_positions",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_4:vehicle_speed",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_4:vehicle_indexes",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Stop:vehicle_index",
             {FourLinksCuttingStopGraphManager<Vehicle, VehicleEntry, VehicleState>::STOP,
              artis::traffic::micro::core::Stop<artis::traffic::micro::core::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>, artis::traffic::micro::core::StopParameters>::vars::VEHICLE_INDEX});
  }
};

/************************************************************
 *
 *                 Link (L4)
 *                          |
 *                         "
 *  End <- Link(L2)  <- Junction
 *         Link (L1) -> (junction) <- Link (L2)
 *                            ^
 *                            |
 *               Link (3)         Stop (stop)
 *               "            ^
 *               |            |
 *              End     Link (L4)
 *
 ************************************************************/
struct EightLinksWithStopsParameters {
  artis::traffic::micro::utils::GeneratorParameters generator_parameters;
  artis::traffic::micro::utils::GeneratorParameters generator_2_parameters;
  artis::traffic::micro::utils::GeneratorParameters generator_3_parameters;
  artis::traffic::micro::utils::GeneratorParameters generator_4_parameters;
  artis::traffic::micro::core::LinkParameters link_1_parameters;
  artis::traffic::micro::core::LinkParameters link_2_parameters;
  artis::traffic::micro::core::LinkParameters link_3_parameters;
  artis::traffic::micro::core::LinkParameters link_4_parameters;
  artis::traffic::micro::core::LinkParameters link_5_parameters;
  artis::traffic::micro::core::LinkParameters link_6_parameters;
  artis::traffic::micro::core::LinkParameters link_7_parameters;
  artis::traffic::micro::core::LinkParameters link_8_parameters;
  artis::traffic::micro::core::JunctionParameters junction_parameters;
  artis::traffic::micro::core::StopParameters stop_1_parameters;
  artis::traffic::micro::core::StopParameters stop_2_parameters;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class EightLinksWithStopsGraphManager :
  public MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, EightLinksWithStopsParameters> {
public:
  enum sub_models {
    GENERATOR, GENERATOR_2, GENERATOR_3, GENERATOR_4, LINK_1, LINK_2, LINK_3, LINK_4, LINK_5, LINK_6, LINK_7, LINK_8,
    JUNCTION, STOP_1, STOP_2, END, END_2, END_3, END_4
  };

  EightLinksWithStopsGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                                  const EightLinksWithStopsParameters &parameters,
                                  const artis::common::NoParameters &graph_parameters)
    :
    MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, EightLinksWithStopsParameters>(
      coordinator, parameters, graph_parameters),
    _generator("generator", parameters.generator_parameters),
    _generator_2("generator2", parameters.generator_2_parameters),
    _generator_3("generator3", parameters.generator_3_parameters),
    _generator_4("generator4", parameters.generator_4_parameters),
    _link_1("L1", parameters.link_1_parameters),
    _link_2("L2", parameters.link_2_parameters),
    _link_3("L3", parameters.link_3_parameters),
    _link_4("L4", parameters.link_4_parameters),
    _link_5("L5", parameters.link_5_parameters),
    _link_6("L6", parameters.link_6_parameters),
    _link_7("L7", parameters.link_7_parameters),
    _link_8("L8", parameters.link_8_parameters),
    _junction("junction", parameters.junction_parameters),
    _stop_1("stop_1", parameters.stop_1_parameters),
    _stop_2("stop_2", parameters.stop_2_parameters),
    _end("end", artis::common::NoParameters()),
    _end_2("end_2", artis::common::NoParameters()),
    _end_3("end_3", artis::common::NoParameters()),
    _end_4("end_4", artis::common::NoParameters()) {
    this->add_child(GENERATOR, &_generator);
    this->add_child(GENERATOR_2, &_generator_2);
    this->add_child(GENERATOR_3, &_generator_3);
    this->add_child(GENERATOR_4, &_generator_4);
    this->add_child(LINK_1, &_link_1);
    this->add_child(LINK_2, &_link_2);
    this->add_child(LINK_3, &_link_3);
    this->add_child(LINK_4, &_link_4);
    this->add_child(LINK_5, &_link_5);
    this->add_child(LINK_6, &_link_6);
    this->add_child(LINK_7, &_link_7);
    this->add_child(LINK_8, &_link_8);
    this->add_child(JUNCTION, &_junction);
    this->add_child(STOP_1, &_stop_1);
    this->add_child(STOP_2, &_stop_2);
    this->add_child(END, &_end);
    this->add_child(END_2, &_end_2);
    this->add_child(END_3, &_end_3);
    this->add_child(END_4, &_end_4);

    this->connect_generator_to_link(_generator, _link_1);
    this->connect_generator_to_link(_generator_2, _link_4);
    this->connect_generator_to_link(_generator_3, _link_6);
    this->connect_generator_to_link(_generator_4, _link_7);

    this->connect_link_to_end(_link_2, _end);
    this->connect_link_to_end(_link_3, _end_2);
    this->connect_link_to_end(_link_5, _end_3);
    this->connect_link_to_end(_link_8, _end_4);

    this->connect_upstream_link_to_junction(_link_1, _junction, 0);
    this->connect_upstream_link_to_junction(_link_6, _junction, 1);

    this->connect_upstream_link_to_stop(_link_4, _stop_1, 0, 1);
    this->connect_upstream_link_to_stop(_link_7, _stop_2, 0, 1);

    this->connect_stop_to_junction(_stop_1, _junction, 2);
    this->connect_stop_to_junction(_stop_2, _junction, 3);

    this->connect_concurrent_links(_link_1, _link_6, 0, 0);
    this->connect_concurrent_link_to_stop(_link_1, _stop_1, 0, 1);
    this->connect_concurrent_link_to_stop(_link_6, _stop_1, 1, 1);

    this->connect_concurrent_link_to_stop(_link_1, _stop_2, 0, 2);
    this->connect_concurrent_link_to_stop(_link_6, _stop_2, 1, 2);

    this->connect_downstream_link_to_junction(_link_2, _junction, 0, 0);
    this->connect_downstream_link_to_junction(_link_3, _junction, 1, 1);
    this->connect_downstream_link_to_junction(_link_5, _junction, 2, 2);
    this->connect_downstream_link_to_junction(_link_8, _junction, 3, 3);
  }

  ~EightLinksWithStopsGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::Generator<VehicleFactory>,
    artis::traffic::micro::utils::GeneratorParameters> _generator;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::Generator<VehicleFactory>,
    artis::traffic::micro::utils::GeneratorParameters> _generator_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::Generator<VehicleFactory>,
    artis::traffic::micro::utils::GeneratorParameters> _generator_3;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::Generator<VehicleFactory>,
    artis::traffic::micro::utils::GeneratorParameters> _generator_4;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_3;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_4;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_5;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_6;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_7;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>,
    artis::traffic::micro::core::LinkParameters> _link_8;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Junction<Vehicle>,
    artis::traffic::micro::core::JunctionParameters> _junction;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Stop<artis::traffic::micro::core::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>, artis::traffic::micro::core::StopParameters>,
    artis::traffic::micro::core::StopParameters> _stop_1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Stop<artis::traffic::micro::core::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>, artis::traffic::micro::core::StopParameters>,
    artis::traffic::micro::core::StopParameters> _stop_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::End> _end;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::End> _end_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::End> _end_3;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::End> _end_4;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class EightLinkGlobalWithStopsView : public artis::traffic::core::View {
public:
  EightLinkGlobalWithStopsView() {
    selector("Link_1:vehicle_number",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_1:vehicle_positions",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_1:vehicle_speed",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_1:vehicle_indexes",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_2:vehicle_number",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_2:vehicle_positions",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_2:vehicle_speed",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_2:vehicle_indexes",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link_3:vehicle_number",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_3:vehicle_positions",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_3:vehicle_speed",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_3:vehicle_indexes",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_3,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});

    selector("Link_4:vehicle_number",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_4:vehicle_positions",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_4:vehicle_speed",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_4:vehicle_indexes",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_4,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});

    selector("Link_5:vehicle_number",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_5,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_5:vehicle_positions",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_5,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_5:vehicle_speed",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_5,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_5:vehicle_indexes",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_5,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});

    selector("Link_6:vehicle_number",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_6,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_6:vehicle_positions",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_6,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_6:vehicle_speed",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_6,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_6:vehicle_indexes",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_6,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});

    selector("Link_7:vehicle_number",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_7,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_7:vehicle_positions",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_7,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_7:vehicle_speed",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_7,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_7:vehicle_indexes",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_7,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});

    selector("Link_8:vehicle_number",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_8,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link_8:vehicle_positions",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_8,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link_8:vehicle_speed",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_8,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link_8:vehicle_indexes",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_8,
              artis::traffic::micro::core::Link<artis::traffic::micro::core::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::LinkParameters>, artis::traffic::micro::core::LinkParameters>::vars::VEHICLE_INDEXES});

    selector("Stop_1:vehicle_index",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::STOP_1,
              artis::traffic::micro::core::Stop<artis::traffic::micro::core::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>, artis::traffic::micro::core::StopParameters>::vars::VEHICLE_INDEX});
    selector("Stop_2:vehicle_index",
             {EightLinksWithStopsGraphManager<Vehicle, VehicleEntry, VehicleState>::STOP_2,
              artis::traffic::micro::core::Stop<artis::traffic::micro::core::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>, artis::traffic::micro::core::StopParameters>::vars::VEHICLE_INDEX});
  }
};

#endif //ARTIS_TRAFFIC_TESTS_LINKS_PATHS_DEF_HPP
