/**
 * @file web/main.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <system_error>
#include <thread>

#include <nlohmann/json.hpp>

#include <corvusoft/restbed/request.hpp>
#include <corvusoft/restbed/resource.hpp>
#include <corvusoft/restbed/service.hpp>
#include <corvusoft/restbed/session.hpp>
#include <corvusoft/restbed/settings.hpp>
#include <corvusoft/restbed/status_code.hpp>
#include <corvusoft/restbed/web_socket.hpp>
#include <corvusoft/restbed/web_socket_message.hpp>

#include <openssl/sha.h>

#include "animation/simulator.hpp"

using ConcreteSimulator = Simulator<artis::traffic::micro::core::Vehicle, artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::core::Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>;

using json = nlohmann::json;
using namespace restbed;

std::map<std::string, std::shared_ptr<WebSocket>> sockets{};
std::map<std::string, std::shared_ptr<std::thread>> threads{};

json convert_value(const artis::common::event::Value &value) {
  json result;

  if (value.is_type<bool>()) {
    bool v;

    value(v);
    result = v;
  } else if (value.is_type<char>()) {
    char v;

    value(v);
    result = v;
  } else if (value.is_type<double>()) {
    double v;

    value(v);
    result = v;
  } else if (value.is_type<unsigned int>()) {
    unsigned int v;

    value(v);
    result = v;
  } else if (value.is_type<int>()) {
    int v;

    value(v);
    result = v;
  } else if (value.is_type<double *>()) {
    size_t size = value.size();
    double *v;

    value(v);
    result = json::array();
    for (size_t i = 0; i < size; ++i) {
      result[i] = v[i];
    }
  } else if (value.is_type<int *>()) {
    size_t size = value.size();
    int *v;

    value(v);
    result = json::array();
    for (size_t i = 0; i < size; ++i) {
      result[i] = v[i];
    }
  } else if (value.is_type<bool *>()) {
    size_t size = value.size();
    bool *v;

    value(v);
    result = json::array();
    for (size_t i = 0; i < size; ++i) {
      result[i] = v[i];
    }
  } else if (value.is_type<char *>()) {
    size_t size = value.size();
    char *v;

    value(v);
    result = json::array();
    for (size_t i = 0; i < size; ++i) {
      result[i] = v[i];
    }
  } else if (value.is_type<unsigned int *>()) {
    size_t size = value.size();
    unsigned int *v;

    value(v);
    result = json::array();
    for (size_t i = 0; i < size; ++i) {
      result[i] = v[i];
    }
  }
  return result;
}

template<class Time>
json convert(const artis::common::observer::Observer<Time> &observer) {
  const typename artis::common::observer::Observer<Time>::Views &views = observer.views();
  json json_views;

  for (auto itv = views.begin(); itv != views.end(); ++itv) {
    const typename artis::common::observer::View<Time>::SelectorValues &selector_values = itv->second->values();
    json json_view;

    for (auto itp = selector_values.begin(); itp != selector_values.end(); ++itp) {
      const typename artis::common::observer::View<Time>::VariableValues &variable_values = itp->second;
      json json_variable_values;

      for (auto itk = variable_values.begin(); itk != variable_values.end(); ++itk) {
        const typename artis::common::observer::View<Time>::Values &values = itk->second;
        json json_values = json::array();

        for (auto itr = values.begin(); itr != values.end(); ++itr) {
          json_values.push_back({itr->first, convert_value(itr->second)});
        }
        json_variable_values[itk->first.c_str()] = json_values;
      }
      json_view[itp->first.c_str()] = json_variable_values;
    }
    json_views[itv->first.c_str()] = json_view;
  }
  return json_views;
}

void launch_simulation(std::string clientKey) {
  std::ifstream input("../../../scripts/test_audruicq_centre.json");

  if (input) {
    std::string str((std::istreambuf_iterator<char>(input)), std::istreambuf_iterator<char>());
    artis::traffic::micro::utils::JsonReader reader;
    std::vector<std::string> link_names;
    std::map<std::string, std::pair<double, double>> stops;
    std::vector<std::string> stop_names;

    reader.parse_network(str);
    for (const auto &[key, value]: reader.network().links) {
      link_names.push_back(key);
    }
    for (const auto &[key, value]: reader.network().stops) {
      stops[key] = value.data.coordinates;
      stop_names.push_back(key);
    }

    artis::common::context::Context<artis::common::DoubleTime> context(0, 200);
    ConcreteSimulator simulator(context, reader.network(), link_names, {}, stop_names);

    std::clog << "[" << clientKey << "] Simulating ...";
    std::cout.flush();

    simulator.run(context);

    std::clog << "OK" << std::endl;

    json links = json::array();

    for (const auto &e: simulator.link_data()) {
      links.push_back(e.first);
    }

    json views = convert(simulator.observer());
    json ret = {{"message", "finish"},
                {"links",   links},
                {"data",    views}};
    sockets[clientKey]->send(to_string(ret));
  } else {
    json ret = {{"message", "error"}};
    sockets[clientKey]->send(to_string(ret));
  }
}

typedef unsigned char uchar;

static const std::string b = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

static std::string base64_encode(const u_int8_t *in, size_t size) {
  std::string out;

  int val = 0, valb = -6;
  for (size_t i = 0; i < size; ++i) {
    uchar c = in[i];

    val = (val << 8) + c;
    valb += 8;
    while (valb >= 0) {
      out.push_back(b[(val >> valb) & 0x3F]);
      valb -= 6;
    }
  }
  if (valb > -6) out.push_back(b[((val << 8) >> (valb + 8)) & 0x3F]);
  while (out.size() % 4) out.push_back('=');
  return out;
}

std::multimap<std::string, std::string>
build_websocket_handshake_response_headers(const std::shared_ptr<const Request> &request) {
  auto key = request->get_header("Sec-WebSocket-Key");
  Byte hash[SHA_DIGEST_LENGTH];
  std::multimap<std::string, std::string> headers;

  key.append("258EAFA5-E914-47DA-95CA-C5AB0DC85B11");
  SHA1(reinterpret_cast< const unsigned char * >(key.data()), key.length(), hash);
  headers.insert(std::make_pair("Upgrade", "websocket"));
  headers.insert(std::make_pair("Connection", "Upgrade"));
  headers.insert(make_pair("Sec-WebSocket-Accept", base64_encode(hash, SHA_DIGEST_LENGTH)));
  return headers;
}

void close_handler(const std::shared_ptr<WebSocket> &socket) {
  if (socket->is_open()) {
    socket->send(std::make_shared<WebSocketMessage>(WebSocketMessage::CONNECTION_CLOSE_FRAME, Bytes({10, 00})));
  }

  const auto key = socket->get_key();

  sockets.erase(key);

  std::clog << "Closed connection to " << key.data() << std::endl;
}

void error_handler(const std::shared_ptr<WebSocket> &socket, const std::error_code error) {
  std::cerr << "WebSocket Errored '" << error.message().data() << "' for " << socket->get_key().data() << std::endl;
}

void message_handler(const std::shared_ptr<WebSocket> &source, const std::shared_ptr<WebSocketMessage> &message) {
  const auto opcode = message->get_opcode();

  if (opcode == WebSocketMessage::CONNECTION_CLOSE_FRAME) {
    source->close();
  } else if (opcode == WebSocketMessage::TEXT_FRAME) {
    std::string action(reinterpret_cast< const char * >(message->get_data().data()), message->get_data().size());
    const auto &key = source->get_key();

    if (action == "run") {
      if (threads.find(key.data()) == threads.end()) {
        json ret = {{"message", "ok"}};
        threads[key.data()] = std::make_shared<std::thread>(launch_simulation, std::string(key.data()));
        threads[key.data()]->detach();
        sockets[key.data()]->send(to_string(ret));
      } else {
        json ret = {{"message", "yet running"}};
        sockets[key.data()]->send(to_string(ret));
      }
    } else {
      json ret = {{"message", "unknown action"}};
      sockets[key.data()]->send(to_string(ret));
    }
  }
}

void get_method_handler(const std::shared_ptr<Session> &session) {
  const auto request = session->get_request();
  const auto connection_header = request->get_header("connection", String::lowercase);

  if (connection_header.find("upgrade") != std::string::npos) {
    if (request->get_header("upgrade", String::lowercase) == "websocket") {
      const auto headers = build_websocket_handshake_response_headers(request);

      session->upgrade(SWITCHING_PROTOCOLS, headers, [](const std::shared_ptr<WebSocket> &socket) {
        if (socket->is_open()) {
          json ret = {{"message", "connected"}};
          socket->set_close_handler(close_handler);
          socket->set_error_handler(error_handler);
          socket->set_message_handler(message_handler);
          socket->send(to_string(ret), [](const std::shared_ptr<WebSocket> &socket) {
            sockets.insert(make_pair(socket->get_key(), socket));
          });
        } else {
          std::cerr << "WebSocket Negotiation Failed: Client closed connection" << std::endl;
        }
      });
      return;
    }
  }
  session->close(BAD_REQUEST);
}

void service_ready_handler(Service &) {
  std::clog << "The service is up and running." << std::endl;
}

int main() {
  auto resource = std::make_shared<Resource>();

  resource->set_path("/simulation");
  resource->set_method_handler("GET", get_method_handler);

  auto settings = std::make_shared<Settings>();

  settings->set_port(3000);
  settings->set_default_header("Connection", "close");

  auto service = std::make_shared<Service>();

  service->publish(resource);
  service->set_ready_handler(service_ready_handler);
  service->start(settings);

  return EXIT_SUCCESS;
}