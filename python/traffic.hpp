/**
 * @file python/traffic.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_PYTHON_TRAFFIC_HPP
#define ARTIS_TRAFFIC_PYTHON_TRAFFIC_HPP

#include <python/utils.hpp>

#include <tests/traffic/micro/collector_def.hpp>
#include <tests/traffic/micro/links_def.hpp>

//MAKE_PARAMETERS(artis::traffic::micro::core::CollectorParameters,
//                CollectorParameters, occupied_duration);
MAKE_PARAMETERS(artis::traffic::micro::utils::GeneratorParameters,
                GeneratorParameters, start_index, min, mean, stddev, seed);
//MAKE_PARAMETERS(artis::traffic::micro::core::InserterParameters,
//                InserterParameters, threshold);
MAKE_PARAMETERS(artis::traffic::micro::core::LinkParameters,
                LinkParameters, max_speed, length, length_threshold);

MAKE_PARAMETERS(artis::traffic::micro::core::NodeParameters, NodeParameters, open_duration, close_duration,
                occupied_duration, delay_start);

MAKE_PARAMETERS(artis::traffic::micro::core::StopParameters, StopParameters, threshold);

//MAKE_PARAMETERS(artis::traffic::micro::core::Disturbance, Disturbance, time, type, durations, vehicle_index);
//MAKE_PARAMETERS(artis::traffic::micro::core::DisturbanceParameters, DisturbanceParameters, disturbances);
//MAKE_PARAMETERS(OnlyOneLinkDisturbanceParameters, GlobalParameters, generator_parameters, link_parameters, node_parameters, disturbances_parameters);
//MAKE_PARAMETERS(OnlyOneLinkParameters, GlobalParameters, generator_parameters, link_parameters, node_parameters);
MAKE_PARAMETERS(ThreeLinksWithStopParameters,
                GlobalParameters,
                generator1_parameters,
                generator2_parameters,
                link_1_parameters,
                link_2_parameters,
                link_3_parameters,
                stop_parameters
);
//MAKE_PARAMETERS(InserterParameters,
//                InserterGlobalParameters,
//                generator_1_parameters,
//                generator_2_parameters,
//                generator_3_parameters,
//                link_1_parameters,
//                link_2_parameters,
//                link_3_parameters,
//                link_4_parameters,
//                link_5_parameters,
//                link_6_parameters,
//                link_7_parameters,
//                collector_1_parameters,
//                inserter_1_parameters,
//                collector_2_parameters,
//                inserter_2_parameters);

typedef boost::mpl::vector<//artis::traffic::micro::core::CollectorParameters,
  artis::traffic::micro::utils::GeneratorParameters,
  //artis::traffic::micro::core::InserterParameters,
  artis::traffic::micro::core::LinkParameters,
  artis::traffic::micro::core::NodeParameters,
  artis::traffic::micro::core::StopParameters,
//  artis::traffic::micro::core::Disturbance,
//  artis::traffic::micro::core::DisturbanceParameters,
//                           OnlyOneLinkParameters
//                             OnlyOneLinkDisturbanceParameters
  ThreeLinksWithStopParameters
  //InserterParameters
> StructTypes;

typedef boost::mpl::vector<//py_CollectorParameters,
  py_GeneratorParameters,
  //py_InserterParameters,
  py_LinkParameters,
  py_NodeParameters,
  py_StopParameters,
  py_GlobalParameters//,
  //py_InserterGlobalParameters
> Structs;

BUILDER(Structs, StructTypes)

#endif