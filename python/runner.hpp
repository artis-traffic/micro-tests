/**
 * @file python/runner.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_PYTHON_RUNNER_HPP
#define ARTIS_TRAFFIC_PYTHON_RUNNER_HPP

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/kernel/pdevs/Coordinator.hpp>

#include <artis-traffic/core/Base.hpp>

#include <boost/python.hpp>

template<class Parameters, class PyParameters, class GraphManager>
class Runner {
public:
  Runner(PyObject *parameters,
         const std::initializer_list<std::pair<std::string,
           artis::traffic::core::View *>> &views)
    : _parameters(PyParameters()(parameters)), _views(views), _root_coordinator(nullptr) {}

  PyObject *run(double begin, double end, double step) {
    artis::common::context::Context<artis::common::DoubleTime> context(begin, end);

    _root_coordinator.reset(new artis::common::RootCoordinator<
      artis::common::DoubleTime, artis::pdevs::Coordinator<
        artis::common::DoubleTime, GraphManager, Parameters>
    >(context, "root", _parameters, artis::common::NoParameters()));
    for (auto view: _views) {
      _root_coordinator->attachView(view.first, view.second);
    }
    if (step > 0) {
      _root_coordinator->switch_to_timed_observer(step);
    }
    _root_coordinator->run(context);
    return convert<artis::common::DoubleTime>(_root_coordinator->observer());
  }

private:
  PyObject *convert_value(const artis::common::event::Value &value) {
    PyObject * result = Py_None;

    if (value.is_type<double>()) {
      double v;

      value(v);
      result = PyFloat_FromDouble(v);
    } else if (value.is_type<unsigned int>()) {
      unsigned int v;

      value(v);
      result = PyLong_FromLong(v);
    } else if (value.is_type<int>()) {
      int v;

      value(v);
      result = PyLong_FromLong(v);
    } else if (value.is_type<double *>()) {
      size_t size = value.size<double>();
      double *v;

      value(v);
      result = PyList_New(size);
      for (size_t i = 0; i < size; ++i) {
        PyList_SetItem(result, i, PyFloat_FromDouble(v[i]));
      }
    } else if (value.is_type<unsigned int *>()) {
      size_t size = value.size<unsigned int>();
      unsigned int *v;

      value(v);
      result = PyList_New(size);
      for (size_t i = 0; i < size; ++i) {
        PyList_SetItem(result, i, PyLong_FromLong(v[i]));
      }
    }
    return result;
  }

  template<class Time>
  PyObject *convert(const artis::common::observer::Observer<Time> &observer) {
    const typename artis::common::observer::Observer<Time>::Views &views = observer.views();
    PyObject * py_views = PyDict_New();

    for (typename artis::common::observer::Observer<Time>::Views::const_iterator itv = views.begin();
         itv != views.end(); ++itv) {
      const typename artis::common::observer::View<Time>::SelectorValues
        &selector_values = itv->second->values();
      PyObject * py_view = PyDict_New();

      for (typename artis::common::observer::View<Time>::SelectorValues::const_iterator
             itp = selector_values.begin(); itp != selector_values.end(); ++itp) {
        const typename artis::common::observer::View<Time>::VariableValues &variable_values = itp->second;
        PyObject * py_variable_values = PyDict_New();

        for (typename artis::common::observer::View<Time>::VariableValues::const_iterator
               itk = variable_values.begin();
             itk != variable_values.end(); ++itk) {
          const typename artis::common::observer::View<Time>::Values &values = itk->second;
          PyObject * py_values = PyList_New(0);

          for (typename artis::common::observer::View<Time>::Values::const_iterator itr = values.begin();
               itr != values.end(); ++itr) {
            PyObject * value = PyTuple_New(2);

            PyTuple_SetItem(value, 0, PyFloat_FromDouble(itr->first));
            PyTuple_SetItem(value, 1, convert_value(itr->second));
            PyList_Append(py_values, value);
          }
          PyDict_SetItemString(py_variable_values, itk->first.c_str(), py_values);
        }
        PyDict_SetItemString(py_view, itp->first.c_str(), py_variable_values);
      }
      PyDict_SetItemString(py_views, itv->first.c_str(), py_view);
    }
    return py_views;
  }

  Parameters _parameters;
  std::vector<std::pair<std::string, artis::traffic::core::View *>> _views;
  std::unique_ptr<artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime, GraphManager, Parameters>
  >> _root_coordinator;
};

#endif