/**
 * @file python/traffic.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "runner.hpp"
#include "traffic.hpp"

#include <boost/python.hpp>

PyObject *run(double begin, double end, PyObject *obj) {
//  Runner<InserterParameters, py_InserterGlobalParameters, InserterGraphManager>
//      runner(obj, {{"counter", new InserterView()}});

  Runner<ThreeLinksWithStopParameters, py_GlobalParameters, ThreeLinksWithStopGraphManager>
    runner(obj, {{"Link", new ThreeLink1WithStopView()}});

//  Runner<OnlyOneLinkParameters, py_GlobalParameters, OnlyOneLinkGraphManager>
//    runner(obj, {{"Link", new OnlyOneView()}});

//    Runner<OnlyOneLinkDisturbanceParameters, py_GlobalParameters, OnlyOneLinkDisturbanceGraphManager>
//            runner(obj, {{"Link", new OnlyOneDisturbanceView()}});

  return runner.run(begin, end, 0.5);
}

BOOST_PYTHON_MODULE (libpyartis_traffic) {
  namespace bp = boost::python;

  bp::def("run", run);
}