import numpy as np
import matplotlib.pyplot as plt
import pygame
import sys
import os
import imageio
import glob
import pandas as pd
from PIL import Image

import pyartis_traffic

def plot_vehicle_number(parameters):
    for v in range(1, 5):
        parameters['link_parameters']['max_speed'] = v
        links = pyartis_traffic.Traffic(parameters)
        result = links.run(0, 3600)
        vehicle_number = result['link']['Link:vehicle_number'][':root:link:vehicle_number']
        time = np.array([v[0] for v in vehicle_number])
        vn = np.array([v[1] for v in vehicle_number])
        plt.plot(time, vn, label='max speed = ' + str(v))
    plt.legend()
    plt.xlabel('time (s)')
    plt.ylabel('vehicle number')
    plt.show()


def plot_vehicle_positions(parameters):
    links = pyartis_traffic.Traffic(parameters)
    result = links.run(0, 3600)
    vehicle_positions = result['link']['Link:vehicle_positions'][':root:link:vehicle_positions']
    time = np.array([v[0] for v in vehicle_positions])
    vp = [v[1] for v in vehicle_positions]
    for k in range(0, 100):
        p = []
        for i in range(0, len(vp)):
            if vp[i] is not None and len(vp[i]) > k:
                p.append(vp[i][k])
            else:
                p.append(0)
        pp = np.array(p)
        plt.plot(time, pp)
    plt.show()


def clean_data(data):
    new_data = []
    for t in data:
        if len(new_data) > 0:
            if new_data[-1][0] == t[0]:
                new_data.pop()
            if new_data[-1][0] >= t[0]:
                print(new_data[-1], t)
        new_data.append(t)
    return new_data


def draw_vehicle(screen, index, link, position, offset_x, offset_y, links_length, horizontal_link_height, vertical_link_width):
    reverse = True
    reverse_l2 = True
    reverse_l3 = False
    reverse_l4 = True
    vehicle_width = 5
    vehicle_height = 5
    nb_links = len(links_length)
    green = 0, 255, 0
    blue = 0, 0, 255
    red = 255, 0, 9
    purple = 255, 0, 255
    if index < 1000:
        color = blue
    elif index < 2000:
        color = red
    elif index < 3000:
        color = green
    else:
        color = purple
    if link == -1:  # inserter
        pygame.draw.rect(screen, color, [513, 37, 5, 5])
    if link == -2:  # inserter
        pygame.draw.rect(screen, color, [1023, 37, 5, 5])
    if link == 1:
        if nb_links < 4:
            pygame.draw.rect(screen, color, [position + offset_x, 18, vehicle_width, vehicle_height])
        elif nb_links == 4:
            pygame.draw.rect(screen, color, [position + offset_x, 18 + links_length[3], vehicle_width, vehicle_height])
        else:
            pygame.draw.rect(screen, color, [position + offset_x, 18 + links_length[3] + horizontal_link_height + offset_y, vehicle_width, vehicle_height])
    if link == 2:
        if nb_links < 4:
            pygame.draw.rect(screen, color, [links_length[0] + offset_x + vehicle_width + position,
                                             18, vehicle_width, vehicle_height])
        else:
            if reverse_l2:
                if nb_links == 4:
                    pygame.draw.rect(screen, color, [links_length[0] + links_length[1] + offset_x + vehicle_width - position,
                                                 18 + links_length[3], vehicle_width, vehicle_height])
                else:
                    pygame.draw.rect(screen, color, [position + offset_x, 18 + links_length[3], vehicle_width, vehicle_height])
            else:
                pygame.draw.rect(screen, color, [links_length[0] + offset_x + vehicle_width + position,
                                         18 + links_length[3], vehicle_width, vehicle_height])
    if link == 3:
        # pygame.draw.rect(screen, color, [520, 540 - position, 5, 5])
        if nb_links < 4:
            if reverse:
                pygame.draw.rect(screen, color, [520, 540 - position, 5, 5])
            else:
                pygame.draw.rect(screen, color, [520, 40 + position, 5, 5])
        else:
            if reverse_l3:
                pygame.draw.rect(screen, color, [520, links_length[3] + links_length[2] + 40 - position, 5, 5])
            else:
                pygame.draw.rect(screen, color, [520, links_length[3] + 40 + position, 5, 5])
    if link == 4:
        if reverse_l4:
            if nb_links == 4:
                pygame.draw.rect(screen, color, [520, links_length[3] + offset_y - position, 5, 5])
            else:
                pygame.draw.rect(screen, color, [520 + vertical_link_width + offset_x, links_length[2] + links_length[3] + 40 - position, 5, 5])
        else:
            if nb_links == 4:
                pygame.draw.rect(screen, color, [520, offset_y + position, 5, 5])
            else:
                pygame.draw.rect(screen, color, [520 + vertical_link_width + offset_x, links_length[2] + 40 + position, 5, 5])
    if link == 5:
        pygame.draw.rect(screen, color, [links_length[0] + links_length[1] + offset_x + vehicle_width - position,
                                         18 + links_length[3] + horizontal_link_height + offset_y, vehicle_width, vehicle_height])
    if link == 6:
        pygame.draw.rect(screen, color, [links_length[0] + links_length[1] + offset_x + vehicle_width - position,
                                         18 + links_length[3], vehicle_width, vehicle_height])
    if link == 7:
        pygame.draw.rect(screen, color, [520, offset_y + position, 5, 5])
    if link == 8:
        pygame.draw.rect(screen, color, [520 + vertical_link_width + offset_x, links_length[3] + offset_y - position, 5, 5])

def split_columns(column, max_col):
    pos = column.str.split(" ")
    pos = pos.dropna()
    VN = []
    for num_col in range(0, max_col):
        V_i = pos.str[num_col]
        V_i.replace('', np.nan, inplace=True)
        V_i = V_i.dropna().astype('float')
        V_i.replace(-1, np.nan, inplace=True)
        VN.append(V_i)

    return VN


def main():
    # run_one_link()
    # run_three_links_with_stop()

    duration = 1500
    links_length = [500, 500, 500, 500, 500, 500, 500, 500]
    threshold = 200
    nb_links = 1
    timestep = 0.1
    parameters = {
        'generator_parameters':
            {'start_index': 0, 'min': 5, 'mean': 20., 'stddev': 5., 'seed': 200},
        'link_parameters':
            {'max_speed': 10, 'length': 500, 'length_threshold': 51},
        'node_parameters':
            {'open_duration': 200., 'close_duration': 200., 'occupied_duration': 0.5,
             'delay_start': 200.}
    }
    # 'disturbances_parameters':
    # {
    #     {'time': 10, 'type': 'DECELERATION', 'durations': {5, 30, 5}, 'vehicle_index': 1}
    # }
    filename = 'threelinkswithstop'
    run_simulation(parameters, duration, links_length, timestep, filename)


def run_simulation(parameters, duration, links_length, timestep, filename):
    nb_links = len(links_length)
    has_stop = True
    has_roundabout = False
    nb_stop = 2
    nb_roundabout = 1
    start_time = 60
    #links = pyartis_traffic.Traffic(parameters)
    #result = links.run(0, duration)
    result = pd.read_csv('../cmake-build-debug/tests/traffic/micro/Link_junction_global.csv', sep=";")
    links = ['L1', 'L2', 'L3', 'L4', 'L5', 'L6', 'L7', 'L8']
    indexes = []
    vp = []
    vs = []
    indexes_stop = []
    roundabout_vehicle_number = []
    # print(result['Link'].keys())
    # for k in range(1, nb_links + 1):
    #     indexes.append([v[1] for v in result['Link']['Link' + ':vehicle_number'][
    #         ':root:link' + ':vehicle_number']])
    #     vp.append([v[1] for v in result['Link']['Link' + ':vehicle_positions'][
    #         ':root:link' + ':vehicle_positions']])
    #     vs.append([v[1] for v in result['Link']['Link' + ':vehicle_speed'][
    #         ':root:link' + ':vehicle_speed']])

    for i in range(0, len(links)):
        #vp.append([])
        #vs.append([])
        if result[":root:" + links[i] + ":vehicle_indexes"].dtype == 'object':
            indexes.append(result[":root:" + links[i] + ":vehicle_indexes"].str.split(" "))
            max_col = len(indexes[i][indexes[i].last_valid_index()]) - 1
            # print(max_col)
        else:
            r = [[x] for x in result[":root:" + links[i] + ":vehicle_indexes"]]
            # indexes.append(result[":root:" + links[i] + ":vehicle_indexes"])
            indexes.append(r)
            max_col = 1

        if max_col > 1:
            vp.append(split_columns(result[':root:' + links[i] + ':vehicle_positions'], max_col))
            vs.append(split_columns(result[':root:' + links[i] + ':vehicle_speed'], max_col))
        else:
            vp.append([result[':root:' + links[i] + ':vehicle_positions']])
            vs.append([result[':root:' + links[i] + ':vehicle_speed']])

        if nb_stop == 1:
            indexes_stop.append(result[":root:" + "stop" + ":vehicle_index"])

        if nb_stop == 2:
            indexes_stop.append(result[":root:" + "stop_1" + ":vehicle_index"])
            indexes_stop.append(result[":root:" + "stop_2" + ":vehicle_index"])

        if has_roundabout:
            roundabout_vehicle_number.append(result[":root:" + "roundabout" + ":vehicle_number"].dropna())

    #indexes.append(result[':root:link:vehicle_indexes'])
    #vp.append(result[':root:link:vehicle_positions'])
    #vs.append(result[':root:link:vehicle_speed'])

    # print(indexes)
    # print(vp)
    # print(vs)

    offset_x = 10
    offset_y = 10
    horizontal_link_height = 20
    vertical_link_width = 20
    node_width = 5
    stop_height = 30

    pygame.init()
    if nb_links == 1:
        size = links_length[0] + offset_x + horizontal_link_height, 80
    if nb_links == 2:
        size = links_length[0] + offset_x + horizontal_link_height\
               + links_length[1] + offset_x + horizontal_link_height, 80
    if nb_links == 3:
        size = links_length[0] + offset_x + horizontal_link_height \
               + links_length[1] + offset_x + horizontal_link_height, links_length[2] + 50
    if nb_links == 4:
        size = links_length[0] + offset_x + horizontal_link_height \
               + links_length[1] + offset_x + horizontal_link_height, links_length[2] + links_length[3] + 50
    if nb_links == 8:
        size = links_length[0] + offset_x + horizontal_link_height \
               + links_length[4] + offset_x + horizontal_link_height, links_length[2] + links_length[6] + 100
    black = 0, 0, 0
    white = 255, 255, 255
    green = 0, 255, 0
    blue = 0, 0, 255
    red = 255, 0, 0
    screen = pygame.display.set_mode(size)
    myfont = pygame.font.SysFont('Comic Sans MS', 30)
    time = 0
    while time < start_time / timestep:
        time += 1
    while time < duration / timestep:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
        screen.fill(black)

        # link 1
        if nb_links < 4:
            pygame.draw.rect(screen, white, [offset_x, offset_y, links_length[0], horizontal_link_height])
            pygame.draw.line(screen, red, [links_length[0] + offset_x, offset_y],
                             [links_length[0] + offset_x, offset_x + horizontal_link_height], node_width)
        elif nb_links == 8:
            pygame.draw.rect(screen, white, [offset_x, offset_y + links_length[6] + horizontal_link_height + offset_y, links_length[0], horizontal_link_height])
        else:
            pygame.draw.rect(screen, white, [offset_x, offset_y + links_length[3], links_length[0], horizontal_link_height])
            #pygame.draw.line(screen, red, [links_length[0] + offset_x, offset_y + links_length[3]],
            #                 [links_length[0] + offset_x, offset_x + horizontal_link_height], node_width)

        # link 2
        if nb_links >= 2:
            if nb_links < 4:
                pygame.draw.rect(screen, white, [links_length[0] + offset_x, offset_y,
                                                 links_length[1], horizontal_link_height])
                # pygame.draw.line(screen, red, [1110 - threshold, 10], [1110 - threshold, 30], 5)
            elif nb_links == 8:
                pygame.draw.rect(screen, white, [offset_x, offset_y + links_length[6], links_length[0], horizontal_link_height])
            else:
                pygame.draw.rect(screen, white, [links_length[0] + offset_x, offset_y + links_length[3],
                                                 links_length[1], horizontal_link_height])

        # link 3
        if nb_links >= 3:
            if nb_links < 4:
                pygame.draw.rect(screen, white, [links_length[0] + offset_x, stop_height + offset_y,
                                                 vertical_link_width, links_length[2]])
                pygame.draw.line(screen, red, [links_length[0] + offset_x, stop_height + 5],
                                 [links_length[0] + stop_height, stop_height + 5], 10)
            else:
                pygame.draw.rect(screen, white, [links_length[0] + offset_x, stop_height + offset_y + links_length[3],
                                                 vertical_link_width, links_length[2]])
                #pygame.draw.line(screen, red, [links_length[0] + offset_x, stop_height + 5 + links_length[3]],
                #                 [links_length[0] + stop_height, stop_height + 5], 10)

        # link 4
        if nb_links >= 4:
            if nb_links == 4:
                pygame.draw.rect(screen, white, [links_length[0] + offset_x, offset_y,
                                             vertical_link_width, links_length[3]])
            elif nb_links == 8:
                pygame.draw.rect(screen, white, [links_length[0] + offset_x + vertical_link_width + offset_x, stop_height + offset_y + links_length[3],
                                             vertical_link_width, links_length[2]])
            #pygame.draw.line(screen, red, [links_length[0] + offset_x, links_length[3] + stop_height + 5],
            #                 [links_length[0] + stop_height, stop_height + 5], 10)

        # link 5
        if nb_links >= 5:
            pygame.draw.rect(screen, white, [links_length[0] + offset_x, offset_y + links_length[3] + horizontal_link_height + offset_y,
                                             links_length[4], horizontal_link_height])

        # link 6
        if nb_links >= 6:
            pygame.draw.rect(screen, white, [links_length[0] + offset_x, offset_y + links_length[3],
                                             links_length[4], horizontal_link_height])

        # link 7
        if nb_links >= 7:
            pygame.draw.rect(screen, white, [links_length[0] + offset_x, offset_y,
                                             vertical_link_width, links_length[3]])

        # link 8
        if nb_links >= 8:
            pygame.draw.rect(screen, white, [links_length[0] + offset_x + vertical_link_width + offset_x, offset_y,
                                             vertical_link_width, links_length[3]])

        if has_roundabout:
            if nb_links >= 4:
                pygame.draw.circle(screen, red, (links_length[0] + offset_x + 15, links_length[3] + offset_y + 8), 15)
            else:
                pygame.draw.circle(screen, red, (links_length[0] + offset_x + 15, offset_y + 8), 15)

            # pygame.draw.rect(screen, white, [510, 40, 20, 500])
            # pygame.draw.line(screen, red, [510, 35], [530, 35], 10)

        for k in range(0, nb_links):
            # if indexes[k][time] is not None and len(vp[k][time]) > 0:
            if len(indexes[k]) > time and len(vp[k]) > 0:
                # for i in range(0, len(vp[k][time])):
                for i in range(0, len(vp[k])):
                    # if int(vp[k][time][i]) <= links_length[k] and int(vp[k][time][i]) != -1:
                    # print(vp[k][i].first_valid_index(), time, vp[k][i].last_valid_index())
                    if vp[k][i].first_valid_index() is not None:
                        if vp[k][i].first_valid_index() <= time < vp[k][i].last_valid_index():
                            if not np.isnan(vp[k][i][time]):
                                if int(vp[k][i][time]) <= links_length[k] and int(vp[k][i][time]) != -1:
                            # draw_vehicle(screen, i + 1, k + 1, float(vp[k][time][i]), offset_x, offset_y, links_length)
                                    draw_vehicle(screen, int(indexes[k][time][i]), k + 1, float(vp[k][i][time]), offset_x, offset_y, links_length,
                                                 horizontal_link_height, vertical_link_width)

                if has_stop:
                    for j in range(0, nb_stop):
                        if indexes_stop[j][time] != -1:
                            if nb_links < 4:
                                pygame.draw.rect(screen, blue, [520, 30, 5, 5])
                            elif nb_links == 4:
                                pygame.draw.rect(screen, green, [520, links_length[3] + 30, 5, 5])
                            else:
                                if j == 0:
                                    pygame.draw.rect(screen, green, [520 + vertical_link_width + offset_x, links_length[3] + 30 + 40, 5, 5])
                                if j == 1:
                                    pygame.draw.rect(screen, green, [520, links_length[3] + 30, 5, 5])
                if has_roundabout:
                    for j in range(0, nb_roundabout):
                        for l in range(0, int(roundabout_vehicle_number[j][time])):
                            if nb_links >= 4:
                                pygame.draw.rect(screen, blue, [520 + l * 5, links_length[3] + offset_y + 5, 5, 5])
                            else:
                                pygame.draw.rect(screen, blue, [520 + l * 5, offset_y + 5, 5, 5])

        textsurface = myfont.render(str(round(time * timestep, 1)), False, white)
        screen.blit(textsurface, (size[0] / 2 + 40, 40))

        pygame.display.flip()
        # pygame.time.delay(1)
        pygame.image.save(screen, 'images/image' + str(time) + '.png')
        time += 1
    pygame.time.delay(1)
    create_gif(filename)
    clean_repertory()


def run_one_link():
    duration = 1400
    links_length = [500]
    threshold = 200
    nb_links = 1
    timestep = 0.5
    parameters = {
        'generator_parameters':
            {'start_index': 0, 'min': 5, 'mean': 20., 'stddev': 5., 'seed': 200},
        'link_parameters':
            {'max_speed': 10, 'length': 500, 'length_threshold': 51},
        'node_parameters':
            {'open_duration': 2000., 'close_duration': 200., 'occupied_duration': 0.5,
             'delay_start': 0.},
    }
    links = pyartis_traffic.Traffic(parameters)
    result = links.run(0, duration)
    indexes = []
    vp = []
    vs = []
    indexes_stop = []
    print(result['Link'].keys())
    for k in range(1, nb_links + 1):
        indexes.append([v[1] for v in result['Link']['Link' + ':vehicle_number'][
            ':root:link' + ':vehicle_number']])
        vp.append([v[1] for v in result['Link']['Link' + ':vehicle_positions'][
            ':root:link' + ':vehicle_positions']])
        vs.append([v[1] for v in result['Link']['Link' + ':vehicle_speed'][
            ':root:link' + ':vehicle_speed']])

    pygame.init()

    offset_x = 10
    offset_y = 10
    horizontal_link_height = 20
    vertical_link_width = 20
    node_width = 5
    stop_height = 30

    size = 530, 80
    black = 0, 0, 0
    white = 255, 255, 255
    red = 255, 0, 0
    blue = 0, 0, 255
    screen = pygame.display.set_mode(size)
    myfont = pygame.font.SysFont('Comic Sans MS', 30)
    time = 0
    while time < duration / timestep:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
        screen.fill(black)

        # link 1
        pygame.draw.rect(screen, white, [10, 10, 500, 20])
        pygame.draw.line(screen, red, [510, 10], [510, 30], 5)

        for k in range(0, nb_links):
            if indexes[k][time] is not None and len(vp[k][time]) > 0:
                for i in range(0, len(vp[k][time])):
                    if vp[k][time][i] <= links_length[k] and vp[k][time][i] != -1:
                        draw_vehicle(screen, i + 1, k + 1, float(vp[k][time][i]), offset_x, offset_y, links_length)

        textsurface = myfont.render(str(time * timestep), False, white)
        screen.blit(textsurface, (size[0] / 2 + 20, 40))

        pygame.display.flip()
        # pygame.time.delay(1)
        pygame.image.save(screen, 'images/image' + str(time) + '.png')
        time += 1
    pygame.time.delay(1)
    create_gif('onelinknode')
    # clean_repertory()


def run_three_links_with_stop():
    duration = 300
    links_length = [500, 500, 500]
    threshold = 200
    nb_links = 3
    nb_stop = 1
    timestep = 0.5
    parameters = {
        'generator1_parameters':
            {'start_index': 0, 'min': 5, 'mean': 50., 'stddev': 8., 'seed': 432453},
        'generator2_parameters':
            {'start_index': 0, 'min': 5, 'mean': 30., 'stddev': 8., 'seed': 432453},
        'link_1_parameters':
            {'max_speed': 10, 'length': 500, 'length_threshold': 30},
        'link_2_parameters':
            {'max_speed': 10, 'length': 500, 'length_threshold': 30},
        'link_3_parameters':
            {'max_speed': 10, 'length': 500, 'length_threshold': 30},
        'stop_parameters':
            {'threshold': 300}
        # 'node_parameters':
        #    {'open_duration': 200000000., 'close_duration': 200., 'occupied_duration': 0.5,
        #     'delay_start': 0.}
    }
    links = pyartis_traffic.Traffic(parameters)
    result = links.run(0, duration)
    indexes = []
    vp = []
    vs = []
    indexes_stop = []
    print(result['Link'].keys())
    for k in range(1, nb_links + 1):
        indexes.append([v[1] for v in result['Link']['Link_' + str(k) + ':vehicle_number'][
            ':root:L' + str(k) + ':vehicle_number']])
        vp.append([v[1] for v in result['Link']['Link_' + str(k) + ':vehicle_positions'][
            ':root:L' + str(k) + ':vehicle_positions']])
        vs.append([v[1] for v in result['Link']['Link_' + str(k) + ':vehicle_speed'][
            ':root:L' + str(k) + ':vehicle_speed']])
    indexes_stop.append([v[1] for v in result['Link']['Stop' + ':vehicle_index'][
        ':root:stop' + ':vehicle_index']])
    pygame.init()

    offset_x = 10
    offset_y = 10
    horizontal_link_height = 20
    vertical_link_width = 20
    node_width = 5
    stop_height = 30

    size = 1050, 550
    black = 0, 0, 0
    white = 255, 255, 255
    red = 255, 0, 0
    blue = 0, 0, 255
    screen = pygame.display.set_mode(size)
    myfont = pygame.font.SysFont('Comic Sans MS', 30)
    time = 0
    while time < duration / timestep:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
        screen.fill(black)

        # link 1
        pygame.draw.rect(screen, white, [10, 10, 500, 20])
        pygame.draw.line(screen, red, [510, 10], [510, 30], 5)

        # link 2
        pygame.draw.rect(screen, white, [510, 10, 500, 20])
        # pygame.draw.line(screen, red, [1110 - threshold, 10], [1110 - threshold, 30], 5)

        # link 3
        pygame.draw.rect(screen, white, [510, 40, 20, 500])
        pygame.draw.line(screen, red, [510, 35], [530, 35], 10)

        for k in range(0, nb_links):
            if indexes[k][time] is not None and len(vp[k][time]) > 0:
                for i in range(0, len(vp[k][time])):
                    if vp[k][time][i] <= links_length[k] and vp[k][time][i] != -1:
                        draw_vehicle(screen, i + 1, k + 1, float(vp[k][time][i]), offset_x, offset_y, links_length)

        for j in range(0, nb_stop):
            if indexes_stop[j][time] != -1:
                pygame.draw.rect(screen, blue, [520, 30, 5, 5])

        textsurface = myfont.render(str(time * timestep), False, white)
        screen.blit(textsurface, (size[0] / 2 + 20, 40))

        pygame.display.flip()
        # pygame.time.delay(1)
        pygame.image.save(screen, 'images/image' + str(time) + '.png')
        time += 1
    pygame.time.delay(1)
    create_gif('threelinkswithstop')
    # clean_repertory()


def create_gif(name):
    png_dir = 'images'
    images = []
    for file_name in sorted(os.listdir(png_dir)):
        if file_name.endswith('.png'):
            file_path = os.path.join(png_dir, file_name)
            images.append(imageio.imread(file_path))
    imageio.mimsave(name + '.gif', images, format='GIF', duration=0.001)
    print("Gif saved.")


def clean_repertory():
    png_dir = 'images/*'
    files = glob.glob(png_dir)
    for f in files:
        os.remove(f)


def complex_simulation():
    duration = 1000
    threshold = 200
    parameters = {
        'generator_1_parameters':
            {'start_index': 0, 'min': 5, 'mean': 20., 'stddev': 8., 'seed': 78364},
        'generator_2_parameters':
            {'start_index': 100000, 'min': 5, 'mean': 30., 'stddev': 8., 'seed': 76352},
        'generator_3_parameters':
            {'start_index': 200000, 'min': 5, 'mean': 30., 'stddev': 8., 'seed': 76652},
        'link_1_parameters':
            {'max_speed': 10, 'length': 500, 'length_threshold': 30, 'stop': False},
        'link_2_parameters':
            {'max_speed': 10, 'length': 500, 'length_threshold': 30, 'stop': True},
        'link_3_parameters':
            {'max_speed': 10, 'length': 500, 'length_threshold': 30, 'stop': False},
        'link_4_parameters':
            {'max_speed': 10, 'length': 500, 'length_threshold': 30, 'stop': True},
        'link_5_parameters':
            {'max_speed': 10, 'length': 500, 'length_threshold': 30, 'stop': False},
        'link_6_parameters':
            {'max_speed': 10, 'length': 500, 'length_threshold': 30, 'stop': False},
        'link_7_parameters':
            {'max_speed': 10, 'length': 500, 'length_threshold': 30, 'stop': False},
        'collector_1_parameters':
            {'occupied_duration': 0.1},
        'inserter_1_parameters':
            {'threshold': threshold},
        'collector_2_parameters':
            {'occupied_duration': 0.1},
        'inserter_2_parameters':
            {'threshold': threshold}
    }
    links = pyartis_traffic.Traffic(parameters)
    result = links.run(0, duration)

    # vehicle_number = clean_data(result['counter']['Link_1:counter'][':root:link_1:vehicle_number'])
    # time = np.array([v[0] for v in vehicle_number])
    # vn = np.array([v[1] for v in vehicle_number])
    # plt.plot(time, vn)
    #
    # vehicle_number = clean_data(result['counter']['Link_2:counter'][':root:link_2:vehicle_number'])
    # time = np.array([v[0] for v in vehicle_number])
    # vn = np.array([v[1] for v in vehicle_number])
    # plt.plot(time, vn)
    #
    # vehicle_number = clean_data(result['counter']['Link_3:counter'][':root:link_3:vehicle_number'])
    # time = np.array([v[0] for v in vehicle_number])
    # vn = np.array([v[1] for v in vehicle_number])
    # plt.plot(time, vn)
    #
    # plt.xlabel('time (s)')
    # plt.ylabel('vehicle number')
    # plt.show()
    # vehicle_indexes_1 = result['counter']['Link_1:vehicle_indexes'][':root:link_1:vehicle_indexes']
    # indexes_1 = [v[1] for v in vehicle_indexes_1]
    # vehicle_indexes_3 = result['counter']['Link_3:vehicle_indexes'][':root:link_3:vehicle_indexes']
    # indexes_3 = [v[1] for v in vehicle_indexes_3]
    # vehicle_positions_1 = result['counter']['Link_1:vehicle_positions'][
    #     ':root:link_1:vehicle_positions']
    # vehicle_positions_3 = result['counter']['Link_3:vehicle_positions'][
    #     ':root:link_3:vehicle_positions']
    # time = np.array([v[0] for v in vehicle_positions_1])
    # vp_1 = [v[1] for v in vehicle_positions_1]
    # vp_3 = [v[1] for v in vehicle_positions_3]
    # for k in range(0, 11):
    #     p = []
    #     for i in range(0, len(time)):
    #         found = False
    #         if indexes_1[i] is not None:
    #             try:
    #                 index_1 = indexes_1[i].index(k)
    #                 if vp_1[i][index_1] < 500:
    #                     p.append(vp_1[i][index_1])
    #                     found = True
    #             except ValueError:
    #                 pass
    #         if indexes_3[i] is not None:
    #             try:
    #                 index_3 = indexes_3[i].index(k)
    #                 if vp_3[i][index_3] < 500:
    #                     p.append(vp_3[i][index_3])
    #                     found = True
    #             except ValueError:
    #                 pass
    #         if not found:
    #             p.append(0)
    #     pp = np.array(p)
    #     plt.plot(time[:350], pp[:350])
    # plt.show()
    indexes = []
    vp = []
    for k in range(1, 8):
        indexes.append([v[1] for v in result['counter']['Link_' + str(k) + ':vehicle_indexes'][
            ':root:link_' + str(k) + ':vehicle_indexes']])
        vp.append([v[1] for v in result['counter']['Link_' + str(k) + ':vehicle_positions'][
            ':root:link_' + str(k) + ':vehicle_positions']])
    inserter_1_indexes = [v[1] for v in
                          result['counter']['Inserter_1:index'][':root:inserter_1:vehicle_index']]
    inserter_2_indexes = [v[1] for v in
                          result['counter']['Inserter_2:index'][':root:inserter_2:vehicle_index']]
    pygame.init()
    size = 1550, 768
    black = 0, 0, 0
    white = 255, 255, 255
    red = 255, 0, 0
    screen = pygame.display.set_mode(size)
    myfont = pygame.font.SysFont('Comic Sans MS', 30)
    time = 0
    while time < duration / 0.5:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
        screen.fill(black)

        # link 1
        pygame.draw.rect(screen, white, [10, 10, 500, 20])
        pygame.draw.line(screen, red, [510 - threshold, 10], [510 - threshold, 30], 5)
        # link 2
        pygame.draw.rect(screen, white, [505, 50, 20, 500])
        # link 6
        pygame.draw.rect(screen, white, [480, 50, 20, 500])
        # inserter 1
        pygame.draw.rect(screen, white, [505, 35, 20, 10])
        # link 3
        pygame.draw.rect(screen, white, [520, 10, 500, 20])
        pygame.draw.line(screen, red, [1020 - threshold, 10], [1020 - threshold, 30], 5)
        # link 4
        pygame.draw.rect(screen, white, [1015, 50, 20, 500])
        # link 7
        pygame.draw.rect(screen, white, [990, 50, 20, 500])
        # inserter 2
        pygame.draw.rect(screen, white, [1015, 35, 20, 10])
        # link 5
        pygame.draw.rect(screen, white, [1030, 10, 500, 20])

        for k in range(0, 7):
            if indexes[k][time] is not None and len(indexes[k][time]) > 0:
                for index in indexes[k][time]:
                    i = indexes[k][time].index(index)
                    if vp[k][time][i] < 500:
                        draw_vehicle(screen, index, k + 1, int(vp[k][time][i]))

        if inserter_1_indexes[time] >= 0:
            draw_vehicle(screen, inserter_1_indexes[time], -1, 0)
        if inserter_2_indexes[time] >= 0:
            draw_vehicle(screen, inserter_2_indexes[time], -2, 0)

        textsurface = myfont.render(str(time * 0.5), False, white)
        screen.blit(textsurface, (size[0] / 2 - 10, 700))

        pygame.display.flip()
        pygame.time.delay(100)
        time += 1
    pygame.time.delay(10000)


if __name__ == "__main__":
    main()
