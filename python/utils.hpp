/**
 * @file python/utils.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_PYTHON_UTILS_HPP
#define ARTIS_TRAFFIC_PYTHON_UTILS_HPP

#include <artis-star/common/observer/Observer.hpp>
#include <artis-star/common/observer/View.hpp>

#include <boost/mpl/vector.hpp>
#include <boost/mpl/for_each.hpp>

#include <Python.h>

#include <functional>
#include <tuple>
#include <utility>
#include <vector>

typedef boost::mpl::vector<double, unsigned int, unsigned long, int, long, bool> Types;

template<typename S>
std::vector<std::function<void(PyObject * , S & )>> funcs = {
  [](PyObject *v, S &s) { s = PyFloat_AsDouble(v); },
  [](PyObject *v, S &s) { s = PyLong_AsLong(v); },
  [](PyObject *v, S &s) { s = PyLong_AsLong(v); },
  [](PyObject *v, S &s) { s = PyLong_AsLong(v); },
  [](PyObject *v, S &s) { s = PyLong_AsLong(v); },
  [](PyObject *v, S &s) { s = PyObject_IsTrue(v); }
};

template<typename S, std::size_t...Is, class T>
S to_struct_aux(std::index_sequence<Is...>, T &&tuple) {
  return {std::get<Is>(std::forward<T>(tuple))...};
}

template<typename S, class T>
S to_struct(T &&tuple) {
  return to_struct_aux<S>(
    std::make_index_sequence<std::tuple_size<std::remove_reference_t<T >>{}>{},
    std::forward<T>(tuple)
  );
}

template<typename S>
S func(PyObject * obj,
const std::string &key
);

template<typename S, typename Tuple, size_t ...Idx>
S to_struct(std::string vars, std::index_sequence<Idx...>, PyObject *obj) {
  std::vector<std::string> members;

  vars.erase(std::remove(vars.begin(), vars.end(), ' '), vars.end());
  while (not vars.empty()) {
    size_t end = vars.find(',');
    members.push_back(vars.substr(0, end));
    vars = end + 1 == 0 ? "" : vars.substr(end + 1);
  }

  Tuple tuple{func<typename std::tuple_element<Idx, Tuple>::type>(obj, members[Idx])...};

  return to_struct<S>(tuple);
}

#define MAKE_PARAMETERS(S, T, ...) struct py_##T : S { \
    using Tuple = decltype(std::make_tuple(__VA_ARGS__)); \
    S operator()(PyObject* obj) { \
        return to_struct<S, Tuple>(#__VA_ARGS__, \
            std::make_index_sequence<std::tuple_size<Tuple>::value>{}, obj); \
    }; \
}

#define BUILDER(Structs, StructTypes) template<typename S, size_t K> \
S func_aux(PyObject * obj) \
{ \
  typename boost::mpl::at_c<Structs, K>::type t; \
  return t(obj); \
} \
template<typename S> \
S func(PyObject * obj, const std::string &key) \
{ \
  typedef typename boost::mpl::find<Types, S>::type iter; \
  if constexpr (iter::pos::value<boost::mpl::size<Types>::value) { \
    S s; \
    funcs<S>[iter::pos::value](PyDict_GetItemString(obj, key.c_str()), s); \
    return s; \
  } \
  if constexpr (iter::pos::value == boost::mpl::size<Types>::value) { \
    typedef typename boost::mpl::find<StructTypes, S>::type iter2; \
    return func_aux<S, iter2::pos::value>(PyDict_GetItemString(obj, key.c_str())); \
  } \
}

#endif