import libartispython
import libpyartis_traffic


class Traffic:
    def __init__(self, parameters):
        self.parameters = parameters

    def run(self, begin, end):
        return libpyartis_traffic.run(begin, end, self.parameters)
